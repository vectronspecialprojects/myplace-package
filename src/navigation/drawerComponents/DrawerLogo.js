import React from 'react'
import {View, SafeAreaView, Image, ScrollView, StyleSheet, Text, TouchableOpacity} from 'react-native'
import Images from '../../Themes/Images'
import {useSelector} from 'react-redux'
import {isTrue} from '../../utilities/utils'
import Avatar from '../../components/Avatar'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Colors from '../../Themes/Colors'
import Fonts from '../../Themes/Fonts'
import {navigate} from '../NavigationService'
import RouteKey from '../RouteKey'
import {logoImage} from "../../constants/constants";

export const DrawerLogo = props => {
  const sideMenuShowProfile = useSelector(state => state.app.sideMenuShowProfile)
  const useLegacyDesign = useSelector(state => state.app.useLegacyDesign)
  const profile = useSelector(state => state.infoServices.profile)
  return (
    <View style={{flex: 1}}>
      <SafeAreaView forceInset={{top: 'always', horizontal: 'never', flex: 1}}>
        {isTrue(sideMenuShowProfile) && !!profile?.member ? (
          <View style={{alignItems: 'center'}}>
            <Avatar
              uri={profile?.member?.profile_img}
              size={responsiveWidth(64)}
              style={styles.avatar}
              editable={false}
            />
            <Text style={styles.name}>
              {profile?.member?.first_name} {profile?.member?.last_name}
            </Text>
            <Text
              style={styles.seeProfile}
              onPress={() => {
                navigate(RouteKey.ProfileScreen)
              }}>
              See my profile >
            </Text>
          </View>
        ) : (
          <View style={styles.drawerLogoContainer}>
            <Image source={{uri: logoImage}} style={styles.drawerLogo} />
          </View>
        )}
        <ScrollView bounces={false}>{props.children}</ScrollView>
      </SafeAreaView>
      {!isTrue(useLegacyDesign) && (
        <View style={{position: 'absolute', bottom: responsiveHeight(25), right: 20}}>
          <Image
            source={{uri: logoImage}}
            style={{width: responsiveWidth(100), height: responsiveHeight(40)}}
            resizeMode={'contain'}
          />
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  drawerLogoContainer: {
    height: 100,
    marginTop: 20,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  drawerLogo: {
    width: '50%',
    resizeMode: 'contain',
    height: 50
  },
  avatar: {},
  name: {
    color: Colors().drawer.title,
    fontSize: responsiveFont(18),
    fontFamily: Fonts.openSansBold,
    marginTop: responsiveHeight(4),
  },
  seeProfile: {
    fontSize: responsiveFont(15),
    fontFamily: Fonts.openSans,
    color: Colors().drawer.title,
    marginVertical: responsiveHeight(10),
  },
  logoBottom: {
    bottom: responsiveHeight(50),
    position: 'absolute',
    right: responsiveWidth(10),
  },
})
