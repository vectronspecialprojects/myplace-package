/**
 * Created by Hong HP on 11/17/19.
 */

import {Animated, StyleSheet, Text, TouchableOpacity, View, Image, useWindowDimensions} from 'react-native'
import React, {useEffect, useRef, useState} from 'react'
import Colors from '../Themes/Colors'
import {isIOS, responsiveFont, responsiveHeight, shadow} from '../Themes/Metrics'
import {SafeAreaView} from 'react-native-safe-area-context'
import {useDispatch, useSelector} from 'react-redux'
import CustomIcon from '../components/CustomIcon'
import {setShowBarCode} from '../store/actions/appServices'

function CustomTabBar({routeName, ...props}) {
  const {navigation, state} = props
  const selectedTabIndex = state.index
  const {width} = useWindowDimensions()
  const tabMenus = useSelector((state) => state.app.tabMenus)
  const dispatch = useDispatch()

  function renderItem({
    route,
    title,
    selected,
    icon,
    activeColor,
    color,
    index,
    image_icon,
    special_page,
    icon_selector,
  }) {
    return (
      <TouchableOpacity
        activeOpacity={1}
        key={route}
        style={styles.itemContainer}
        onPress={() => {
          if (special_page === 'barcode') {
            dispatch(setShowBarCode(true))
            return
          }
          navigation.navigate(route)
        }}>
        <CustomIcon
          image_icon={image_icon}
          name={icon}
          size={responsiveHeight(22)}
          color={selected ? activeColor : color}
          tintColor={selected ? activeColor : color}
          icon_selector={icon_selector}
        />
        <Text style={[styles.title, {color: Colors().tabs.tabLabel}, selected && {color: activeColor}]}>
          {title}
        </Text>
      </TouchableOpacity>
    )
  }

  return (
    <SafeAreaView
      edges={['bottom']}
      style={[styles.container, {backgroundColor: Colors().tabs.tabBackground}]}>
      <View style={styles.wrapper}>
        {tabMenus?.map((tabMenu, index) => {
          return renderItem({
            route: routeName + tabMenu.id,
            selected: selectedTabIndex === index,
            color: Colors().tabs.tabLabel,
            title: tabMenu?.page_name,
            activeColor: Colors().tabs.activeTabLabel,
            icon: tabMenu.icon,
            index: 0,
            image_icon: tabMenu.image_icon,
            special_page: tabMenu.special_page,
            icon_selector: tabMenu.icon_selector,
          })
        })}
      </View>
    </SafeAreaView>
  )
}

export default CustomTabBar

const styles = StyleSheet.create({
  container: {
    maxHeight: 80,
    ...shadow,
  },
  wrapper: {
    flexDirection: 'row',
    height: responsiveHeight(55),
    width: '100%',
  },
  itemContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  title: {
    fontSize: responsiveFont(10),
    marginTop: responsiveHeight(2),
  },
})
