import React from 'react'
import {View, StyleSheet, SafeAreaView, Text, Image, ScrollView, TouchableOpacity} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {isTrue} from '../utilities/utils'
import Avatar from '../components/Avatar'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import {navigate} from './NavigationService'
import RouteKey from './RouteKey'
import Images from '../Themes/Images'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {useSafeAreaInsets} from 'react-native-safe-area-context'
import CustomIcon from '../components/CustomIcon'
import {DrawerItem} from '@react-navigation/drawer'
import {DrawerCtnOptions} from './DefaultOptions'
import Alert from '../components/Alert'
import * as authServicesActions from '../store/actions/authServices'
import {logoImage, version} from '../constants/constants'

function CustomSideMenu({navigation, data, routeName}) {
  const sideMenuShowProfile = useSelector((state) => state.app.sideMenuShowProfile)
  const useLegacyDesign = useSelector((state) => state.app.useLegacyDesign)
  const profile = useSelector((state) => state.infoServices.profile)
  const safeView = useSafeAreaInsets()
  const dispatch = useDispatch()

  function renderHeader() {
    return (
      <View>
        {isTrue(sideMenuShowProfile) && !!profile?.member ? (
          <View style={{alignItems: 'center'}}>
            <Avatar
              uri={profile?.member?.profile_img}
              size={responsiveWidth(64)}
              style={styles.avatar}
              editable={false}
            />
            <Text style={styles.name}>
              {profile?.member?.first_name} {profile?.member?.last_name}
            </Text>
            <Text
              style={styles.seeProfile}
              onPress={() => {
                navigate(RouteKey.ProfileScreen)
              }}>
              See my profile >
            </Text>
          </View>
        ) : (
          <View style={styles.drawerLogoContainer}>
            <Image source={{uri: logoImage}} style={styles.drawerLogo} />
          </View>
        )}
      </View>
    )
  }

  function renderFooter() {
    return (
      <View>
        {!isTrue(useLegacyDesign) && (
          <View style={{position: 'absolute', bottom: safeView.bottom + 10, right: 20}}>
            <Image
              source={{uri: logoImage}}
              style={{width: responsiveWidth(100), height: responsiveHeight(40)}}
              resizeMode={'contain'}
            />
          </View>
        )}
        <Text
          style={{
            color: Colors().white,
            fontSize: responsiveFont(8),
            textAlign: 'center',
            marginBottom: responsiveHeight(15)
          }}>
          {version}
        </Text>
      </View>
    )
  }

  function renderItem(item, index) {
    return (
      <DrawerItem
        key={index.toString()}
        label={item.page_name}
        onPress={() => {
          if (item.state === 'logout') {
            navigation.toggleDrawer()
            Alert.alert('Logout', 'Are you sure you want to logout?', [
              {
                text: 'No'
              },
              {
                text: 'Yes',
                onPress: () => dispatch(authServicesActions.logout()),
                style: {backgroundColor: Colors().white, borderWidth: 1, borderColor: Colors().second},
                textStyle: {color: Colors().second}
              }
            ])
          } else {
            navigation.navigate(routeName + item.id)
          }
        }}
        labelStyle={DrawerCtnOptions.drawerLabelStyle}
        icon={({focus, size, color}) => (
          <CustomIcon
            image_icon={item.image_icon}
            icon_selector={item.icon_selector}
            name={item.icon}
            size={22}
            style={{width: 30}}
            color={DrawerCtnOptions.drawerActiveTintColor}
            tintColor={DrawerCtnOptions.drawerActiveTintColor}
          />
        )}
      />
    )
  }

  return (
    <View style={{flex: 1, paddingTop: safeView.top}}>
      {renderHeader()}
      <ScrollView bounces={false}>{data.map(renderItem)}</ScrollView>
      {renderFooter()}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  drawerLogoContainer: {
    height: 100,
    marginTop: 20,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  drawerLogo: {
    width: '50%',
    resizeMode: 'contain',
    height: 50
  },
  avatar: {},
  name: {
    color: Colors().drawer.title,
    fontSize: responsiveFont(18),
    fontFamily: Fonts.openSansBold,
    marginTop: responsiveHeight(4)
  },
  seeProfile: {
    fontSize: responsiveFont(15),
    fontFamily: Fonts.openSans,
    color: Colors().drawer.title,
    marginVertical: responsiveHeight(10)
  },
  logoBottom: {
    bottom: responsiveHeight(50),
    position: 'absolute',
    right: responsiveWidth(10)
  },
  item: {
    height: responsiveHeight(50)
  },
  version: {}
})
export default CustomSideMenu
