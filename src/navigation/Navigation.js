import React from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {createDrawerNavigator, DrawerItemList} from '@react-navigation/drawer'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {componentMatch} from './StackNavigation'
import {DrawerCtnOptions, tabOptions} from './DefaultOptions'
import RouteKey from './RouteKey'
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'
import {isIOS} from '../Themes/Metrics'
import CustomIcon from '../components/CustomIcon'
import CustomTabBar from './CustomTabBar'
import CustomSideMenu from './CustomSideMenu'

//tab
const Tab = createBottomTabNavigator()
export const TabNavigator = props => {
  const tabMenus = useSelector(state => state.app.tabMenus)
  return (
    <Tab.Navigator
      backBehavior="initialRoute"
      screenOptions={tabOptions}
      tabBar={props => <CustomTabBar {...props} routeName={RouteKey.tabMenu}/>}>
      {tabMenus.map(tabMenu => {
        if (!tabMenu.state) return null
        return (
          <Tab.Screen
            key={tabMenu.id}
            name={RouteKey.tabMenu + tabMenu.id}
            component={componentMatch(tabMenu.state)}
            initialParams={{params: tabMenu}}
            options={{
              headerShown: false,
              tabBarLabel: tabMenu.page_name,
              unmountOnBlur: true,
              tabBarIcon: props => (
                <CustomIcon
                  image_icon={tabMenu.image_icon}
                  name={tabMenu.icon}
                  style={{marginTop: isIOS() ? 10 : 0}}
                  size={22}
                  color={props.color}
                  tintColor={props.color}
                  icon_selector={tabMenu.icon_selector}
                />
              ),
            }}
          />
        )
      })}
    </Tab.Navigator>
  )
}

//drawer after signin
const DrawerStackAfterAuth = createDrawerNavigator()
export const DrawerAfterAuth = () => {
  const dispatch = useDispatch()
  const drawerMenus = useSelector(state => state.app.drawerMenus)
  const sideMenuPosition = useSelector(state => state.app.sideMenuPosition)

  return (
    <DrawerStackAfterAuth.Navigator
      drawerContent={props => <CustomSideMenu {...props} data={drawerMenus} routeName={RouteKey.sideMenu} />}
      screenOptions={{...DrawerCtnOptions, drawerPosition: sideMenuPosition}}>
      {drawerMenus.map(drawerMenu => {
        if (!drawerMenu.icon && !drawerMenu.image_icon) return null
        return (
          <DrawerStackAfterAuth.Screen
            key={drawerMenu.id}
            name={RouteKey.sideMenu + drawerMenu.id}
            component={componentMatch(drawerMenu.state)}
            initialParams={{params: drawerMenu}}
            options={{
              unmountOnBlur: true,
              headerShown: false,
            }}
          />
        )
      })}
    </DrawerStackAfterAuth.Navigator>
  )
}

//drawer before signin
const DrawerStackBeforeAuth = createDrawerNavigator()
export const DrawerBeforeAuth = () => {
  const drawerFrontMenus = useSelector(state => state.app.drawerFrontMenus)
  const sideMenuPosition = useSelector(state => state.app.sideMenuPosition)

  return (
    <DrawerStackBeforeAuth.Navigator
      drawerContent={props => (
        <CustomSideMenu {...props} data={drawerFrontMenus} routeName={RouteKey.sideFrontMenu} />
      )}
      screenOptions={{...DrawerCtnOptions, drawerPosition: sideMenuPosition}}>
      <DrawerStackBeforeAuth.Screen
        name={RouteKey.FrontpageNavigator}
        component={componentMatch(RouteKey.FrontpageNavigator)}
        options={{
          headerShown: false,
          drawerLabel: 'Home',
          drawerIcon: props => (
            <FontAwesome5Pro name="home" size={22} style={{width: 30}} color={props.color} />
          ),
        }}
      />

      {drawerFrontMenus.map(drawerFrontMenu => {
        return (
          <DrawerStackBeforeAuth.Screen
            key={drawerFrontMenu.id}
            name={RouteKey.sideFrontMenu + drawerFrontMenu.id}
            component={componentMatch(drawerFrontMenu.state)}
            initialParams={{params: drawerFrontMenu}}
            options={{
              unmountOnBlur: true,
              headerShown: false,
              drawerLabel: drawerFrontMenu.page_name,
              drawerIcon: props => (
                <CustomIcon
                  image_icon={drawerFrontMenu.image_icon}
                  icon_selector={drawerFrontMenu.icon_selector}
                  name={drawerFrontMenu.icon}
                  size={22}
                  style={{width: 30}}
                  color={props.color}
                  tintColor={props.color}
                />
              ),
            }}
          />
        )
      })}
    </DrawerStackBeforeAuth.Navigator>
  )
}
