import React from 'react'
import {View, Image, StyleSheet, TouchableOpacity} from 'react-native'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {navigate, checkRouteOrigin} from './NavigationService'
import RouteKey from './RouteKey'
import Images from '../Themes/Images'
import {logoImage} from "../constants/constants";

export const tabOptions = {
  tabBarLabelStyle: {
    fontFamily: Fonts.openSans,
    fontSize: 10,
  },
  tabBarHideOnKeyboard: true,
  tabBarInactiveTintColor: Colors().tabs.tabLabel,
  tabBarActiveTintColor: Colors().tabs.activeTabLabel,
  tabBarShowLabel: true,
  tabBarStyle: {
    backgroundColor: Colors().tabs.tabBackground,
    paddingTop: 0,
  },
}

export const defaultStackOptions = {
  headerStyle: {
    backgroundColor: Colors().first,
  },
  headerTitleStyle: {
    fontFamily: Fonts.openSans,
  },
  headerBackTitleStyle: {
    fontFamily: Fonts.openSans,
  },
  headerBackTitleVisible: false,
  headerTintColor: Colors().defaultTextColor,
  headerBackground: () => (
    <View style={styles.logoContainer}>
      <TouchableOpacity
        onPress={() => {
          if (checkRouteOrigin() === 'Home') {
            navigate(RouteKey.HomeNavigator)
            navigate(RouteKey.Home)
          }
          if (checkRouteOrigin() === 'FrontpageNavigator') {
            navigate(RouteKey.FrontpageNavigator)
            navigate(RouteKey.StartupScreen)
          }
        }}>
        <Image style={styles.logo} source={{uri: logoImage}} />
      </TouchableOpacity>
    </View>
  ),
  headerTitle: () => null,
}

export const DrawerCtnOptions = {
  drawerActiveTintColor: Colors().drawer.title,
  drawerActiveBackgroundColor: null,
  drawerInactiveTintColor: Colors().drawer.title,
  drawerLabelStyle: {
    fontFamily: Fonts.openSans,
    fontSize: 16,
    color: Colors().drawer.title,
  },
  drawerStyle: {
    flex: 1,
    width: '80%',
    backgroundColor: Colors().drawer.background,
  },
}

const styles = StyleSheet.create({
  logoContainer: {
    flex: 1,
    backgroundColor: Colors().headerBackground,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  logo: {
    width: 100,
    height: 50,
    resizeMode: 'contain',
  },
})
