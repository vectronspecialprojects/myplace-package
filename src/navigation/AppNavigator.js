import {NavigationContainer} from '@react-navigation/native'
import React from 'react'
import {connect} from 'react-redux'
import SplashScreen from '../screens/SplashScreen'
import MaintenanceScreen from '../screens/MaintenanceScreen'
import {DrawerAfterAuth, DrawerBeforeAuth} from './Navigation'
import {navigationRef} from './NavigationService'
import OnboardingScreen from '../screens/OnboardingScreen'

function AppNavigation(props) {
  const {appState} = props
  function renderStack() {
    switch (appState) {
      case 'SplashScreen':
        return <SplashScreen />
      case 'OnBoarding':
        return <OnboardingScreen />
      case 'Maintenance':
        return <MaintenanceScreen />
      case 'Auth':
        return <DrawerBeforeAuth />
      case 'Main':
        return <DrawerAfterAuth />
      default:
        return <SplashScreen />
    }
  }

  return <NavigationContainer ref={navigationRef}>{renderStack()}</NavigationContainer>
}

export default connect((state) => ({
  appState: state.app.appState,
}))(AppNavigation)
