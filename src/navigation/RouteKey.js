import PondHoppersScreen from '../screens/HomeComponent/PondHoppersScreen'
import {TabNavigator} from './Navigation'

export default {
  AboutNavigator: 'AboutNavigator',
  EnquiriesNavigator: 'EnquiriesNavigator',
  FaqNavigator: 'FaqNavigator',
  FeedbackNavigator: 'FeedbackNavigator',
  FrontpageNavigator: 'FrontpageNavigator',
  GiftCertificateNavigator: 'GiftCertificateNavigator',
  HomeNavigator: 'HomeNavigator',
  LegalsNavigator: 'LegalsNavigator',
  LocationsNavigator: 'LocationsNavigator',
  OffersNavigator: 'OffersNavigator',
  OurTeamNavigator: 'OurTeamNavigator',
  ReferNavigator: 'ReferNavigator',
  StampcardNavigator: 'StampcardNavigator',
  SurveysNavigator: 'SurveysNavigator',
  TicketsNavigator: 'TicketsNavigator',
  VouchersNavigator: 'VouchersNavigator',
  WebviewNavigator: 'WebviewNavigator',
  YourorderNavigator: 'YourorderNavigator',
  WhatsonNavigator: 'WhatsonNavigator',
  GamingNavigator: 'GamingNavigator',
  MembershipNavigator: 'MembershipNavigator',
  ShopNavigator: 'ShopNavigator',
  CameraNavigator: 'CameraNavigator',
  ConnectWithUsNavigator: 'ConnectWithUsNavigator',
  tabMenu: 'tabMenu',
  sideMenu: 'sideMenu',
  sideFrontMenu: 'sideFrontMenu',
  homeMenu: 'homeMenu',
  profileMenu: 'profileMenu',
  Home: 'Home',
  ProfileNavigator: 'ProfileNavigator',
  CommunityPointNavigator: 'CommunityPointNavigator',
  PondHoppersNavigator: 'PondHoppersNavigator',
  TabNavigator: 'TabNavigator',

  //Screen
  LoginScreen: 'LoginScreen',
  SignupScreen: 'SignupScreen',
  ForgotPasswordScreen: 'ForgotPasswordScreen',
  PreferredVenueScreen: 'PreferredVenueScreen',
  MapScreen: 'MapScreen',
  ProfileScreen: 'ProfileScreen',
  HistoryScreen: 'HistoryScreen',
  FavoriteScreen: 'FavoriteScreen',
  StartupScreen: 'StartupScreen',
  AboutScreen: 'AboutScreen',
  WhatsonScreen: 'WhatsonScreen',
  WhatsonDetailsScreen: 'WhatsonDetailsScreen',
  ReferScreen: 'ReferScreen',
  TicketsScreen: 'TicketsScreen',
  GiftCertificateScreen: 'GiftCertificateScreen',
  SurveysScreen: 'SurveysScreen',
  LocationsScreen: 'LocationsScreen',
  StampcardScreen: 'StampcardScreen',
  FaqScreen: 'FaqScreen',
  FeedbackScreen: 'FeedbackScreen',
  VouchersScreen: 'VouchersScreen',
  VoucherDetailScreen: 'VoucherDetailScreen',
  OffersScreen: 'OffersScreen',
  EnquiriesScreen: 'EnquiriesScreen',
  LegalsScreen: 'LegalsScreen',
  HomeScreen: 'HomeScreen',
  WebviewScreen: 'WebviewScreen',
  YourorderScreen: 'YourorderScreen',
  ResetPasswordScreen: 'ResetPasswordScreen',
  ChangeEmailScreen: 'ChangeEmailScreen',
  SurveyDetailsScreen: 'SurveyDetailsScreen',
  OurTeamScreen: 'OurTeamScreen',
  TicketDetailScreen: 'TicketDetailScreen',
  FavoriteDetailScreen: 'FavoriteDetailScreen',
  ProductListScreen: 'ProductListScreen',
  BuyGiftCertificateScreen: 'BuyGiftCertificateScreen',
  MatchAccountScreen: 'MatchAccountScreen',
  MatchAccountInfo: 'MatchAccountInfo',
  GamingScreen: 'GamingScreen',
  WheelGameScreen: 'WheelGameScreen',
  MembershipScreen: 'MembershipScreen',
  MembershipDetailsScreen: 'MembershipDetailsScreen',
  ShopScreen: 'ShopScreen',
  ScratchGameScreen: 'ScratchGameScreen',
  CameraScreen: 'CameraScreen',
  VerifyPhoneAndEmail: 'VerifyPhoneAndEmail',
  CommunityPointScreen: 'CommunityPointScreen',
  ConnectWithUsScreen: 'ConnectWithUsScreen',
  ChangePhoneScreen: 'ChangePhoneScreen',
  UpdateAccountScreen: 'UpdateAccountScreen',
  PondHoppersScreen: 'PondHoppersScreen',
}
