/*eslint-disable*/
import {logout} from '../store/actions/authServices'
import {ERROR_STATUS, MaintainStatus} from '../constants/constants'
import {setAppState} from '../store/actions/appServices'
import store from '../store/store'
import Alert from '../components/Alert'

let objRequest = {}

function timeout(request, duration, api) {
  return new Promise((resolve, reject) => {
    let timeout = setTimeout(() => {
      resolve({})
    }, duration)

    request.then((res) => {
      clearTimeout(timeout)
      timeout = null
      resolve(res)
    }, (err) => {
      clearTimeout(timeout)
      timeout = null
      resolve({})
    },)
  })
}

function apiChecking(api, additionKey, callback) {
  let key = api
  if (additionKey) {
    key += additionKey
  }
  if (objRequest[key]) {
    return {ok: true}
  } else {
    objRequest[key] = setTimeout(() => {
      delete objRequest[key]
    }, 5000)
  }
  return callback()
}

export function getWithTimeout(api, headers) {
  return apiChecking(api, '', () => {
    return timeout(get(api, headers), 60000, api)
  })
}

export function get(api, headers) {
  return fetch(api, {
    method: 'get', headers: {
      Accept: 'application/json', 'Content-Type': 'application/json', ...headers,
    },
  })
    .then((response) => {
      return response.json().then((data) => {
        return {
          ok: response.ok, ...data,
        }
      })
    })
    .catch((err) => {
      return err
    })
}

export function patchWithTimeout(api, headers, body) {
  return apiChecking(api, '', () => {
    return timeout(patch(api, headers, body), 60000, api)
  })

}

export function patch(api, headers, body) {
  if (typeof body === 'object' && body.constructor !== FormData) body = JSON.stringify(body)
  return fetch(api, {
    method: 'patch', headers: {
      Accept: 'application/json', 'Content-Type': 'application/json', ...headers,
    }, body: body,
  })
    .then((response) => {
      return response.json().then((data) => {
        return {
          ok: response.ok, ...data,
        }
      })
    })
    .catch((err) => {
      return err
    })
}

export function postWithTimeout(api, headers, body) {
  return apiChecking(api, body?.display_id, () => {
    return timeout(post(api, headers, body), 60000, api)
  })

}

export function post(api, headers, body) {
  if (typeof body === 'object' && body.constructor !== FormData) body = JSON.stringify(body)
  let heads = {}
  if (headers['Content-Type']) {
    heads = {
      ...headers, Accept: 'application/json', 'Content-Type': 'application/json',
    }
  } else {
    heads = {
      ...headers, Accept: 'application/json', 'Content-Type': 'application/json',
    }
  }
  return fetch(api, {
    method: 'post', headers: heads, body: body,
  })
    .then((response) => {
      console.log(response)
      if (ERROR_STATUS.includes(response.status)) {
        store.dispatch(logout())
        response.json().then((data) => {
          Alert.alert('Error', data?.message)
        })
        return
      }
      if (MaintainStatus.includes(response.status)) {
        store.dispatch(setAppState('Maintenance'))
        return
      }
      return response.json().then((data) => {
        return {
          ok: response.ok, ...data,
        }
      })
    })
    .catch((err) => {
      return err
    })
}

export function deleteWithTimeout(api, headers, body) {
  return apiChecking(api, '', () => {
    return timeout(_delete(api, headers, body), 30000, api)
  })
}

export function _delete(api, headers, body) {
  if (typeof body === 'object' && body.constructor !== FormData) body = JSON.stringify(body)

  let heads = {}
  if (headers['Content-Type'])
    heads = {
    ...headers, Accept: 'application/json',
  }
  else
    heads = {
    ...headers, Accept: 'application/json', 'Content-Type': 'application/json',
  }

  return fetch(api, {
    method: 'delete', headers: heads, body: body,
  })
    .then((response) => {
      return response.json().then((data) => {
        return {
          ok: response.ok, ...data,
        }
      })
    })
    .catch((err) => {
      return err
    })
}

export function putWithTimeout(api, headers, body) {
  return apiChecking(api, '', () => {
    return timeout(put(api, headers, body), 30000, api)
  })
}

export function put(api, headers, body) {
  if (typeof body === 'object' && body.constructor !== FormData) body = JSON.stringify(body)

  let heads = {}
  if (headers['Content-Type'])
    heads = {
    ...headers, Accept: 'application/json',
  }
  else
    heads = {
    ...headers, Accept: 'application/json', 'Content-Type': 'application/json',
  }
  return fetch(api, {
    method: 'put', headers: heads, body: body,
  })
    .then((response) => {
      if (ERROR_STATUS.includes(response.status)) {
        response.json().then((data) => {
          Alert.alert('Error', data?.message)
        })
        store.dispatch(logout())
        return
      }
      if (MaintainStatus.includes(response.status)) {
        store.dispatch(setAppState('Maintenance'))
        return
      }
      return response.json().then((data) => {
        return {
          ok: response.ok, ...data,
        }
      })
    })
    .catch((err) => {
      return err
    })
}
