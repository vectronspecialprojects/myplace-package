import stripe from 'tipsi-stripe'
import {apiGetStripeKey} from './ApiManage'
export function initStripe() {
  apiGetStripeKey().then((res) => {
    if (res.ok) {
      stripe.setOptions({
        publishableKey: res.data?.key,
        androidPayMode: 'test', // Android only
      })
    }
  })
}

export async function createPaymentMethod(name, number, cvc, expMonth, expYear) {
  try {
    const paymentMethod = await stripe.createTokenWithCard({
      number,
      cvc,
      expMonth,
      expYear,
      name,
    })
    return paymentMethod
  } catch (e) {
    throw e
  }
}
