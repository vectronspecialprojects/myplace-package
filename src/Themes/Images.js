export default {
  defaultImage: require('../assets/img/default_image.jpg'),
  maintenance: require('../assets/img/maintenance.png'),
  marker: require('../assets/img/marker.png'),
  barcode: require('../assets/img/barcode-pl.png'),
  wheelGame: require('../assets/img/wheel_icon.png'),
  scratchGame: require('../assets/img/scratch_icon.png'),
  grayImage: require('../assets/img/gray_image.jpeg'),
  knob: require('../assets/img/knob.png'),
}
