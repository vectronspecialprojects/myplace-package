let colors = {
  defaultRefreshSpinner: '#65c8c6',
  defaultBackground: '#202224',
  headerBackground: '#25282a',
  subHeaderBackground: '#25282a',
  defaultFilterIcon: '#65c8c6',
  defaultFilterBackground: '#fff',
  defaultFilterText: '#444',
  headerLeftIcon: '#fff',
  pageTitle: '#65c8c6',
  defaultTextColor: '#ffffff',
  defaultTextInputBackgound: '#25282a',
  defaultTextInputLabelColor: '#ffffff',
  defaultTextInputColor: '#ffffff',
  defaultPlaceholderTextColor: '#fff',
  termAndConditionTextColor: '#65c8c6',
  dufaultButtonBackground: '#65c8c6',
  defaultButtonText: '#fff',
  defaultAlertTextTitle: '#000',
  defaultAlertTextmessage: '#000',
  defaultCartBarBackground: '#25282a',
  btnDone: '#0405cf',
  first: '#25282a',
  second: '#65c8c6',
  cartBarItems: '#ffffff',
  cartBarItemsActive: '#FF3838',
  flatlistNoItems: '#444',
  red: 'red',
  white: '#fff',
  black: '#000',
  opacity: 'rgba(0,0,0,0.45)',
  gray: 'gray',
  notification: {
    borderColor: '#000',
    backgroundColor: '#f0f0f0',
    textColor: '#000',
    iconColor: '#000'
  },
  firstPage: {
    firstButtonBackground: '#65c8c6',
    firstButtonBorder: '#65c8c6',
    firstButtonText: '#fff',
    secondButtonBackground: '#25282a',
    secondButtonBorder: '#25282a',
    secondButtonText: '#fff',
    borderMatchButton: '#65c8c6',
    backgroundMatchButton: 'transparent',
    textMatchButton: '#65c8c6'
  },
  home: {
    name: '#fff',
    description: '#fff',
    menuBackground: '#25282a',
    menuIcon: '#fff',
    menuDesc: '#65c8c6',
    menuText: '#fff',
    subTitle: '#65c8c6',
    preferredVenueButton: '#25282a',
    preferredVenueButtonIcon: '#fff',
    tierBackground: '#65c8c6',
    iconStar: '#65c8c6',
    iconBackground: '#fff',
    venueTitle: '#65c8c6',
    venueDesc: '#fff',
    tierBarText: '#fff',
    barcodeGeneratingText: '#ffffff',
    barcodeTopBorder: '#25282a',
    barcodeAreaBackground: '#202224'
  },
  whaton: {
    tabBackgroundActive: '#65c8c6',
    tabBackgroundInactive: 'transparent',
    tabTitleActive: '#fff',
    tabTitleInactive: '#65c8c6',
    titleColor: '#65c8c6',
    description: '#fff',
    dateColor: '#fff',
    dateBorderColor: '#fff'
  },
  whatonDetails: {
    title: '#65c8c6',
    description: '#fff',
    dateColor: '#fff'
  },
  stampcard: {
    cardTitle: '#65c8c6',
    starIcon: '#65c8c6'
  },
  giftCertificate: {
    cardTitle: '#fff',
    cardTitleExpanded: '#65c8c6'
  },
  buyGiftCertificate: {
    descriptionText: '#fff',
    inputText: '#444'
  },
  about: {
    description: '#fff'
  },
  legal: {
    iconArrow: '#fff',
    title: '#fff',
    description: '#fff'
  },
  voucher: {
    title: '#fff',
    date: '#fff',
    description: '#fff'
  },
  location: {
    tagsBackgroundActive: '#65c8c6',
    tagsBackgroundInactive: '#202224',
    tagsTitleActive: '#fff',
    tagsTitleInactive: '#65c8c6',
    title: '#fff',
    address: '#fff',
    description: '#fff',
    openHours: '#65c8c6',
    cardBackground: '#202224',
    tagsBarBackground: '#25282a'
  },
  faq: {
    searchBox: '#fff',
    title: '#65c8c6',
    description: '#fff',
    background: '#25282a'
  },
  survey: {
    title: '#fff',
    description: '#fff',
    background: '#25282a',
    icon: '#65c8c6',
    questionTitle: '#65c8c6',
    answer: '#fff'
  },
  referFriends: {
    placeHolder: '#444',
    textInput: '#444',
    textInputBackground: '#fff'
  },
  ticket: {
    title: '#fff',
    date: '#fff',
    description: '#fff'
  },
  ourTeam: {
    title: '#fff',
    description: '#fff',
    name: '#65c8c6',
    background: '#25282a',
    buttonBackground: '#25282a',
    buttonTitle: '#65c8c6'
  },
  offer: {
    title: '#fff',
    description: '#fff',
    point: '#65c8c6',
    redeemButtonBackground: '#202224',
    redeemButtonBorder: '#fff',
    redeemButtonText: '#fff',
    cancelButtonBackground: '#202224',
    cancelButtonBorder: '#fff',
    cancelButtonText: '#fff',
    popupBackground: '#202224'
  },
  favorite: {
    title: '#65c8c6',
    description: '#fff'
  },
  history: {
    background: '#25282a',
    text: '#fff',
    filterText: '#65c8c6'
  },
  profile: {
    borderButtonColor: '#65c8c6',
    backgroundButton: '#202224',
    buttonText: '#444',
    textInput: '#444',
    icon: '#fff'
  },
  resetPassword: {
    secondButtonBackground: '#fff',
    secondButtonBorder: '#000',
    secondButtonText: '#000'
  },
  changeEmail: {
    secondButtonBackground: '#fff',
    secondButtonBorder: '#000',
    secondButtonText: '#000'
  },
  signUp: {
    fieldBackground: '#25282a',
    fieldText: '#fff',
    backgroundSignupButton: '#65c8c6',
    textSignupButton: '#ffffff',
    placeholderTextColor: '#eee'
  },
  signIn: {
    textForgotPassword: '#65c8c6',
    fieldText: '#fff',
    backgroundSigninButton: '#65c8c6',
    borderSigninButton: '#65c8c6',
    textSigninButton: '#ffffff',
    placeholderTextColor: '#eee',
    fieldBackground: '#25282a'
  },
  preferredVenue: {
    icon: '#fff',
    title: '#fff',
    background: '#202224',
    venueActive: '#65c8c6',
    venueActiveText: '#000'
  },
  feedback: {
    description: '#fff',
    star: '#65c8c6',
    checkBoxOuter: '#65c8c6',
    checkBoxInner: '#65c8c6',
    background: '#25282a',
    textInput: '#fff'
  },
  drawer: {
    background: '#25282a',
    title: '#fff'
  },
  tabs: {
    tabBackground: '#202224',
    tabLabel: '#ffffff',
    activeTabLabel: '#65c8c6'
  },
  paymentPopup: {
    totalAmount: '#fff',
    buttonBackground: '#000',
    buttonText: '#fff'
  },
  forgotPassword: {
    buttonBackground: '#000',
    buttonBorder: '#000',
    buttonText: '#fff',
    placeholderTextColor: '#fff'
  },
  matchAccountScreen: {
    placeholderTextColor: '#eee',
    fieldBackground: '#25282a',
    fieldText: '#fff',
    backgroundFirstButton: '#000',
    boderColorFirstButton: '#000',
    textFirstButton: '#fff',
    backgroundSecondButton: '#fff',
    boderColorSecondButton: '#000',
    textSecondButton: '#fff'
  },
  productItemScreen: {
    cardBackground: '#25282a',
    productName: '#fff',
    price: '#fff',
    buttonBackground: '#65c8c6',
    buttonBorder: '#fff',
    buttonIcon: '#fff',
    quantityText: '#fff'
  },
  buyGiftCertificateScreen: {
    destColor: '#fff'
  },
  gamingScreen: {
    gameItemBackground: '#25282a'
  },
  membershipScreen: {
    cardTitle: '#65c8c6'
  },
  changePhone: {
    secondButtonBackground: '#fff',
    secondButtonBorder: '#000',
    secondButtonText: '#000'
  },
  onBoarding: {
    background: '#25282a',
    backgroundButton: '#fff',
    titleButton: '#25282a',
    contentBackground: '#fff'
  },
  startUpScreen: {
    background: '#25282a',
    title: '#fff'
  },
  venueTags: {
    tagsBackgroundActive: '#25282a',
    tagsBackgroundInactive: '#fff',
    tagsTitleActive: '#fff',
    tagsTitleInactive: '#000',
    tagsBarBackground: '#fff',
    borderColor: '#25282a'
  },
  cameraScreen: {
    title: '#fff',
    description: '#fff'
  }
}

export function setColors(obj) {
  colors = obj
}

export default function Colors() {
  return colors
}
