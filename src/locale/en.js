let engData = {
  okay: 'Okay',
  somethingWentWrong: 'Something Went Wrong',
  pullToRefresh: 'Pull to refresh',
  canNotMatch: 'Can not match account at the moment',
  sorry: 'Sorry',
  startUpScreen: {
    welcomeBack: 'Welcome to Publinc! Log in',
    alreadyAMember: 'Already have a Publinc member card?',
    matchMyAccount: 'Link it to your account.',
    imNew: 'Sign up for free',
    joinNow: 'Join Now',
    version: 'Version',
    appNotSetup: 'App not setup',
    message: "The app navigation hasn't been setup. please setup the back panel and reset the app",
    signIn: 'Sign In'
  },
  signUp: {
    signUp: 'Sign Up',
    preferredVenue: 'My Local',
    preferredVenuePlaceholder: 'Select your local',
    tier: 'Tier',
    tierPlaceholder: 'Select your preferred tier',
    firstName: 'First Name',
    lastName: 'Last Name',
    field_1: 'Custom Field 1',
    field_1Placeholder: 'Select your custom field 1',
    field_2: 'Custom Field 2',
    field_2Placeholder: 'Select your custom field 2',
    email: 'E-mail',
    dob: 'DOB',
    dobErr: 'DOB must be over 18 years old',
    password: 'Password',
    confirmPassword: 'Re-enter Password',
    confirmPassErr: 'Please enter the same password',
    mobile: 'Mobile',
    continue: 'Continue',
    titleGetTier: 'Cannot get tiers signup',
    titleGetGaming: 'Cannot get gaming signup',
    titleErrSignUp: 'Sorry, we cannot sign you up at the moment',
    plsEnter: 'Please enter a valid',
    errPassword: 'Password must be not less than 6 characters with at least 1 letter & 1 number.',
    receiveOffer:
      "Sorry, I don't want to receive marketing, nice offers and promotional material from Publinc Communities via SMS and/or email. I understand that I can change my mind and opt in at any time.",
    title: "Let's get you set up"
  },
  signIn: {
    signIn: 'Log In',
    email: 'E-mail',
    password: 'Password',
    forgotPassword: 'Forgot Password',
    titleErrSignIn: 'Sorry, we cannot sign you in at the moment',
    plsEnter: 'Please enter a valid',
    errPassword: 'Password must be not less than 6 characters with at least 1 letter & 1 number.'
  },
  forgotPassword: {
    title: 'Forgot Password',
    email: 'E-mail',
    plsEnter: 'Please enter a valid email address',
    submit: '',
    success: 'A reset password request has been sent successfully.'
  },
  matchAccount: {
    accountId: 'Account Id',
    accountNumber: 'Account Number',
    plsEnter: 'Please enter a valid',
    continue: 'Continue',
    cancel: 'Cancel'
  },
  profile: {
    messUpdateSuccess: 'Profile info updated successfully!',
    firstName: 'First Name',
    lastName: 'Last Name',
    dob: 'DOB',
    mobile: 'Mobile',
    email: 'Email',
    tierName: 'Tier Name',
    resetPassword: 'Reset Password',
    changeEmail: 'Change Email',
    saveChange: 'Save changes',
    changePhone: 'Change Phone',
    updateTitle: 'Edit my details',
    support: 'About Publinc',
    signOut: 'Log out of Publinc Communities',
    preferredVenue: 'My Local'
  },
  home: {
    titleConfirmEmail: 'Please confirm your email',
    messageConfirmEmail: 'Please confirm your email to complete the sign up process.'
  },
  memberNumber: 'Member Number:',
  plsSelectPreferred: 'Please select a preferred venue.',
  saveChanges: 'Save Changes',
  product: {
    messageSuccess: 'Your order submitted successfully!',
    buyNow: 'Buy Now',
    proceed: 'Proceed',
    messageError: 'Please contact the venue for product setting.'
  },
  changeEmail: {
    title: 'Change Email',
    messageSuccess: 'Email changed successfully!',
    invalidEmail: 'Your email is not validated!',
    confirmEmailNotMatch: 'Confirm email do not match!',
    newEmail: 'New Email',
    confirmNewEmail: 'Confirm New Email',
    password: 'Password',
    continue: 'Continue',
    cancel: 'Cancel'
  },
  resetPassword: {
    title: 'Reset Password',
    messageSuccess: 'Password reset successfully!',
    confirmNotMatch: 'Confirm password do not match!',
    oldPassword: 'Old Password',
    newPassword: 'New Password',
    confirmPassword: 'Confirm New Password',
    continue: 'Continue',
    cancel: 'Cancel'
  },
  survey: {
    messageSuccess: 'Survey submitted successfully!',
    submit: 'Submit'
  },
  ticket: {
    title: 'Ticket Detail',
    lookupNumber: 'Lookup Number:'
  },
  buyGiftCertificate: {
    title: 'Buy Now',
    invalidEmail: 'Your email is not validated!',
    messageSuccess: 'Gift Certificate bought successfully!',
    submitSuccess: 'Your order submitted successfully!',
    fullName: 'Full Name',
    email: 'Email',
    message: 'Message',
    continue: 'Continue'
  },
  enquiry: {
    title: 'Enquiries'
  },
  favorite: {
    title: 'Favourite'
  },
  feedback: {
    description: 'We would love to hear from you!\nPlease enter your thoughts below.',
    plsContact: 'Please contact me regarding this feedback.',
    submit: 'Submit'
  },
  history: 'History',
  location: {
    titlePer: 'Insufficient permissions!',
    titleMess: 'You need to grant location permissions to use this app.',
    titleFetch: 'Could not fetch location',
    plsTryAgain: 'Please try again later.'
  },
  refer: {
    title: 'Refer a Friend',
    invitationSuccess: 'Your invitation has been sent!',
    fullName: 'Full Name',
    email: 'Email',
    continue: 'Continue',
    invalidEmail: 'Your email is not validated!'
  },
  voucher: {
    title: 'Voucher Detail',
    lookupNumber: 'Lookup Number:'
  },
  webView: {
    titleAlert: "Website is n't available",
    message: 'Please contact the venue.'
  },
  yourOrder: {
    notAvailable: "Online ordering is n't available",
    message: 'Please contact the venue.'
  },
  preferredVenueButton: {
    onlineOrderingAvailable: 'Online ordering is available at this venue',
    onlineOrderingNotAvailable: "Online ordering isn't available at this venue"
  },
  camera: {
    errTitle: 'Not Your Order QR Code',
    errMessage: 'Our app only read Your Order QR code'
  },
  verifyPhoneAndEmail: {
    title: "Let's link your account",
    email: 'Email',
    mobile: 'Mobile',
    connect: 'Connect'
  },
  changePhone: {
    title: 'Change Phone',
    messageSuccess: 'Phone changed successfully!',
    invalidPhone: 'Your Phone is not validated!',
    confirmPhoneNotMatch: 'Confirm Phone do not match!',
    newPhone: 'New Phone',
    confirmNewPhone: 'Confirm New Phone',
    password: 'Password',
    continue: 'Continue',
    cancel: 'Cancel'
  },
  cameraScreen: {
    title: 'Scan QR code to start Ordering',
    description: 'Hold your camera over the QR code located on the table.'
  }
}

export function setLanguage(obj) {
  console.log('setLanguage', obj)
  engData = obj
}

export function getLanguage() {
  return engData
}
