import I18n from 'i18n-js'
import {getLanguage} from './en'

export function configuration() {
  I18n.locale = 'en'
  I18n.fallbacks = true
  I18n.translations = {
    en: getLanguage(),
  }
}

export function setLocale(locale) {
  I18n.locale = locale
}

export function getLocale() {
  return I18n.locale
}

export function localize(text) {
  return I18n.t(text)
}

export default I18n
