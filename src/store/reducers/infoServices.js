import * as act from '../actions/actionCreator'
import {isTrue, sortVenueByDistance, updateObject} from '../../utilities/utils'
import {SET_USER_LOCATION} from '../actions/user'
import {GET_APPSETTINGS} from '../actions/actionCreator'

const initialState = {
  legals: [],
  about: '',
  referMsg: '',
  profile: {},
  venues: [],
  venueTags: [],
  preferredVenueId: 0,
  venueTagsSetting: {},
  specialEvents: undefined,
  regularEvents: undefined,
  stampCards: undefined,
  stampCardsWon: undefined,
  giftCertificate: undefined,
  vouchers: undefined,
  offers: undefined,
  outTeam: undefined,
  faq: undefined,
  notifications: undefined,
  tickets: undefined,
  surveys: [],
  membershiplist: undefined,
  preferredBepozVenueId: null,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_APPSETTINGS:
      return updateObject(state, {isMultipleVenue: action.appFlags?.isMultipleVenue})
    case act.GET_LEGALS:
      return updateObject(state, {legals: action.legals})
    case act.GET_ABOUT:
      return updateObject(state, {about: action.about})
    case act.GET_PROFILE:
      let listAllow = action.profile?.member.member_tier?.tier?.allowed_venues_tag?.venue_pivot_tags
      let venues = [...state.venues]
      if (listAllow?.length > 0) {
        venues = venues.filter(item => !!listAllow.find(i => i.venue_id === item.id))
      }
      let venueTagWithVenue = state?.venueTags.map(item => {
        return {
          ...item,
          listVenue: venues.filter(
            venue => !!venue.venue_pivot_tags?.find(tag => tag.venue_tag_id === item.id),
          ),
        }
      })

      return updateObject(state, {
        profile: action.profile,
        preferredVenueId: isTrue(state.isMultipleVenue)
          ? action?.profile?.member?.current_preferred_venue
          : 0,
        venues: venues,
        venueTagWithVenue: venueTagWithVenue,
        preferredBepozVenueId: isTrue(state.isMultipleVenue)
          ? action.profile?.member?.current_preferred_venue_full?.bepoz_venue_id
          : null,
      })
    case act.UPDATE_PROFILE_SUCCESS:
      return updateObject(state, {profile: action.payload})
    case act.GET_SPECIALEVENTS:
      return updateObject(state, {specialEvents: action.events})
    case act.GET_REGULAREVENTS:
      return updateObject(state, {regularEvents: action.events})
    case act.GET_STAMPCARDS:
      return updateObject(state, {stampCards: action.stampcards})
    case act.GET_STAMPCARDSWON:
      return updateObject(state, {stampCardsWon: action.stampcardswon})
    case act.GET_GIFTCERTIFICATE:
      return updateObject(state, {giftCertificate: action.giftcertificate})
    case act.GET_VOUCHERS:
      return updateObject(state, {vouchers: action.vouchers})
    case act.GET_OFFERS:
      return updateObject(state, {offers: action.offers})
    case act.GET_TICKETS:
      return updateObject(state, {tickets: action.tickets})
    case act.GET_FAQ:
      return updateObject(state, {faq: action.faq})
    case act.GET_TRANSACTION_SUCCESS:
      return updateObject(state, {transactions: action.payload})
    case act.GET_FAVORITE_SUCCESS:
      console.log('favorite', action.payload)
      return updateObject(state, {favorites: action.payload})
    case act.GET_OUTTEAM:
      return updateObject(state, {ourTeam: action.ourteam})
    case act.UPDATE_NOTIFICATIONS:
      return updateObject(state, {notifications: action.notifications})
    case act.GET_REFERMSG:
      return updateObject(state, {referMsg: action.referMsg})
    case act.GET_SURVEYS:
      return updateObject(state, {surveys: action.surveys})
    case act.CHANGE_EMAIL_SUCCESS:
      return updateObject(state, {profile: updateObject(state.profile, {email: action.payload})})
    case act.GET_VENUES:
      return updateObject(state, {
        venues: action.venues,
        venueTags: action.venueTags,
        venueTagsSetting: action.venueTagsSetting,
        venueOrder: action.venueOrder,
      })
    case SET_USER_LOCATION: {
      let venues = [...state.venues]
      if (isTrue(state?.venueOrder?.distanceOrder)) {
        venues = sortVenueByDistance(venues, action.payload)
      } else {
        venues = venues?.[0]?.display_order
          ? venues.sort((a, b) => a.display_order - b.display_order)
          : venues.sort((a, b) => a.id - b.id)
      }
      return updateObject(state, {
        venues: venues,
      })
    }
    case act.GET_MEMBERSHIPLIST:
      return updateObject(state, {membershiplist: action.membershiplist})
    case act.LOGOUT:
      const savedLegals = [...(state.legals || [])]
      const savedAbout = state.about
      const savedVenues = [...(state.venues || [])]
      const savedVenuesList = [...(state.venuesList || [])]
      const savedVenueTags = [...(state.venueTags || [])]
      const savedVenueTagsSetting = {...(state.venueTagsSetting || {})}
      return updateObject(initialState, {
        legals: savedLegals,
        about: savedAbout,
        venues: savedVenues,
        venuesList: savedVenuesList,
        venueTags: savedVenueTags,
        venueTagsSetting: savedVenueTagsSetting,
      })
    default:
      return state
  }
}
