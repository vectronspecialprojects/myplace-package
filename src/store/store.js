import {createStore, applyMiddleware, compose} from 'redux'
import ReduxThunk from 'redux-thunk'
import {persistReducer, persistStore} from 'redux-persist'
import reducers from './reducers'
import AsyncStorage from '@react-native-async-storage/async-storage'
/**
 *  Redux Store configuration
 */

const persistConfig = {
  key: 'primaryData',
  storage: AsyncStorage,
  blacklist: [],
}

const persistedReducer = persistReducer(persistConfig, reducers)

export const store = createStore(persistedReducer, applyMiddleware(ReduxThunk))
export const persistor = persistStore(store)

export default store
