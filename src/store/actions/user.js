export const USER_REGISTER = 'USER_REGISTER'

export function userRegister(email, password) {
  return {
    type: USER_REGISTER,
    email,
    password,
  }
}

export const USER_LOGIN = 'USER_LOGIN'

export function userLogin(username, password) {
  return {
    type: USER_LOGIN,
    username,
    password,
  }
}

export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS'

export function userLoginSuccess(payload) {
  return {
    type: USER_LOGIN_SUCCESS,
    payload,
  }
}

export const UPDATE_USER_INFO = 'UPDATE_USER_INFO'

export function updateUserInfo(userInfo) {
  return {
    type: UPDATE_USER_INFO,
    userInfo: userInfo,
  }
}

export const GET_ACCOUNT_DETAILS = 'GET_ACCOUNT_DETAILS'

export function getAccountDetails(session) {
  return {
    type: GET_ACCOUNT_DETAILS,
    session,
  }
}

export const GET_ACCOUNT_DETAILS_SUCCESS = 'GET_ACCOUNT_DETAILS_SUCCESS'

export function getAccountDetailsSuccess(accountDetails) {
  return {
    type: GET_ACCOUNT_DETAILS_SUCCESS,
    accountDetails,
  }
}

export const FORGOT_PASSWORD = 'FORGOT_PASSWORD'

export function forgotPassword(email, session) {
  return {
    type: FORGOT_PASSWORD,
    email,
    session,
  }
}

export const USER_LOGOUT = 'USER_LOGOUT'

export function userLogout() {
  return {
    type: USER_LOGOUT,
  }
}

export const SET_USER_LOCATION = 'SET_USER_LOCATION'

export function setUserLocation(payload) {
  return {
    type: SET_USER_LOCATION,
    payload,
  }
}
