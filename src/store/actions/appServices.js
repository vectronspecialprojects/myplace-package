import {apiGetAppSettings, getPledgeInfo} from '../../utilities/ApiManage'
import * as act from './actionCreator'
import {convertArrayToObject, isTrue} from '../../utilities/utils'
import {setLanguage} from '../../locale/en'
import {configuration} from '../../locale/I18nConfig'
import {setColors} from '../../Themes/Colors'
import {setData} from '../../utilities/storage'
import {APP_COLORS, APP_LANGUAGE} from '../../constants/constants'

export function setGlobalIndicatorVisibility(visible) {
  return {
    type: act.SET_GLOBAL_INDICATOR_VISIBILITY,
    visible,
  }
}

export const getAppSettings = () => {
  return async (dispatch) => {
    const response = await apiGetAppSettings()
    console.log(response)
    if (!response.ok) {
      throw new Error(response.message)
    }
    if (response.ok) {
      const galleries = []
      let isAppSetup = true
      const homeMenus = response?.data?.home_menus
      const drawerMenus = response?.data?.side_menus.filter(
        (sideMenu) => sideMenu.icon || sideMenu.image_icon,
      )
      const drawerFrontMenus = response?.data?.side_menus_before_login.filter(
        (sideMenu) => sideMenu.icon || sideMenu.image_icon,
      )
      const tabMenus = response?.data?.tabs.filter((tabMenu) => tabMenu.icon || tabMenu.image_icon)

      for (const key in response?.data?.galleries) {
        galleries.push(response?.data?.galleries[key].url)
      }

      for (const key in homeMenus) {
        if (!homeMenus[key]?.state) isAppSetup = false
      }

      for (const key in drawerMenus) {
        if (!drawerMenus[key]?.state) isAppSetup = false
      }

      for (const key in drawerFrontMenus) {
        if (!drawerFrontMenus[key]?.state) isAppSetup = false
      }

      for (const key in tabMenus) {
        if (!tabMenus[key]?.state) isAppSetup = false
      }
      /** start - config language and color */
      setLanguage(response?.data?.default_message)
      setColors(response?.data?.default_color)
      setData(APP_LANGUAGE, response?.data?.default_message)
      setData(APP_COLORS, response?.data?.default_color)
      configuration()
      /** end - config language and color */

      dispatch({
        type: act.GET_APPSETTINGS,
        galleries: galleries,
        homeMenus: homeMenus,
        drawerMenus: drawerMenus,
        drawerFrontMenus: drawerFrontMenus,
        tabMenus: tabMenus,
        isAppSetup: isAppSetup,
        welcomeInstruction: response?.data?.welcome_instruction,
        communityImpact: response?.data?.community_impact,
        emailNotAllowedDomains: !!response?.data?.email_not_allowed_domains?.length
          ? new RegExp('[^,]*' + response?.data?.email_not_allowed_domains.join('|') + '[^,]*', 'i')
          : '',
        memberDefaultTierDetail: convertArrayToObject(
          response?.data?.member_default_tier_detail,
          response?.data?.member_default_tier_option === 'by_venue_tag' ? 'name' : 'id',
        ),
        memberDefaultTierOption: response?.data?.member_default_tier_option,
        sideMenuPosition: isTrue(response.data?.use_legacy_design) ? 'left' : 'right',
        sideMenuShowProfile: response.data?.side_menu_show_profile,
        useLegacyDesign: isTrue(response.data?.use_legacy_design),
        welcomeInstructionEnable: isTrue(response.data?.welcome_instruction_enable),
        formInputStyle: response.data?.form_input_style,
        profileMenu: response.data?.profile_menu,
        pondHopper: {
          uniqueKey: response.data?.pond_hoppers_unique_key,
          url: response.data?.pond_hoppers_url,
        },
        enableShowTier: isTrue(response.data?.enable_tier_icon_image_colour),
        pondHoppersEnable: response.data?.pond_hoppers_enable,
        appFlags: response.data?.flags
      })
    }
  }
}

export function setAppMaintenance(payload) {
  return {
    type: act.SET_APP_MAINTENANCE,
    payload,
  }
}

export function setAppState(payload) {
  return {
    type: act.SET_APP_STATE,
    payload,
  }
}

export function setInternetState(payload) {
  return {
    type: act.SET_INTERNET_STATE,
    payload,
  }
}

export function setShowBarCode(visible) {
  return {
    type: act.SET_SHOW_BAR_CODE,
    visible,
  }
}
