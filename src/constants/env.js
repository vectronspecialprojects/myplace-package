import {Platform} from 'react-native'
import {
  GOOGLE_API_KEY,
  ONE_SIGNAL_API_KEY,
  BUILD_ENV,
  CODEPUSH_IOS_STG,
  CODEPUSH_IOS_PRO,
  CODEPUSH_ANDROID_STG,
  CODEPUSH_ANDROID_PRO,
} from 'react-native-dotenv'

const vars = {
  googleApiKey: GOOGLE_API_KEY,
  oneSignalApiKey: ONE_SIGNAL_API_KEY,
  buildEvn: BUILD_ENV,
}

export const codePushKey = Platform.select({
  ios: {
    staging: CODEPUSH_IOS_STG,
    production: CODEPUSH_IOS_PRO,
  },
  android: {
    staging: CODEPUSH_ANDROID_STG,
    production: CODEPUSH_ANDROID_PRO,
  },
})

export default vars
