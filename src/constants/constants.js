import {API_URI as baseUrl, APP_SECRET, APP_TOKEN} from 'react-native-dotenv';
export const API_URI = baseUrl
export const app_secret = APP_SECRET
export const app_token = APP_TOKEN
export const ERROR_STATUS = [401, 403, 410]
export const MaintainStatus = [503]
export const defaultDistance = 9999999999
export const backgroundImage = 'https://api.vecport.net/devreact/api/background'
export const logoImage = 'https://api.vecport.net/devreact/api/logo'
export const APP_LANGUAGE = 'APP_LANGUAGE'
export const APP_COLORS = 'APP_COLORS'
export const version = '5.2.7.0.0'
