import React, {useState, useEffect, useCallback} from 'react'
import {View, Text, ScrollView, StyleSheet, RefreshControl, LayoutAnimation} from 'react-native'
import {onLinkPress} from './../components/UtilityFunctions'
import {useSelector, useDispatch} from 'react-redux'
import {TouchableCmp} from './../components/UtilityFunctions'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import * as infoServicesActions from '../store/actions/infoServices'
import Html from '../components/Html'
import SubHeaderBar from './../components/SubHeaderBar'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Fonts from '../Themes/Fonts'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import {isTrue} from '../utilities/utils'
import {navigate} from '../navigation/NavigationService'
import RouteKey from '../navigation/RouteKey'

const LegalsScreen = (props) => {
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isGroupShown, setIsGroupShown] = useState('')
  const legals = useSelector((state) => state.infoServices.legals)
  console.log(legals)
  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchLegals)
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    setIsRefreshing(false)
    dispatch(setGlobalIndicatorVisibility(false))
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  function onItemPress(item) {
    if (isTrue(item.webview_enable)) {
      navigate(RouteKey.WebviewScreen, {
        params: {
          appUri: item.webview_url,
        },
      })
    } else {
      if (isGroupShown === item.title) {
        setIsGroupShown('')
      } else {
        setIsGroupShown(item.title)
      }
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    }
  }

  function renderItem(item, index) {
    const isSelected = isGroupShown === item?.title
    return (
      <View style={{paddingHorizontal: 5}} key={index.toString()}>
        <TouchableCmp activeOpacity={0.6} onPress={() => onItemPress(item)}>
          <View style={styles.titleContainer}>
            <Text style={[styles.title, {color: Colors().legal.title}]}>{item?.title}</Text>
            <AntDesign name={isSelected ? 'down' : 'right'} size={22} color={Colors().legal.iconArrow} />
          </View>
        </TouchableCmp>
        {isSelected ? (
          <View style={{marginBottom: 0}}>
            <Html
              html={item?.content || ''}
              textAlign={'left'}
              color={Colors().legal.description}
              onLinkPress={onLinkPress}
            />
          </View>
        ) : null}
      </View>
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      <View style={{flex: 1, padding: 20}}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                setIsRefreshing(true)
                loadContent()
              }}
              tintColor={Colors().defaultRefreshSpinner}
              titleColor={Colors().defaultRefreshSpinner}
              title={localize('pullToRefresh')}
            />
          }>
          {legals.map(renderItem)}
        </ScrollView>
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  titleContainer: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontFamily: Fonts.openSansBold,
    fontSize: 20,
    textAlign: 'left',
  },
})

export default LegalsScreen
