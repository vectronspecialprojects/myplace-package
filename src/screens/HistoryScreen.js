import React, {useCallback, useEffect, useMemo, useState} from 'react'
import {View, RefreshControl, FlatList} from 'react-native'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import HistoryItem from './../components/HistoryItem'
import DateFilterComponent from './../components/DateFilterComponent'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const HistoryScreen = props => {
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()
  const transactions = useSelector(state => state.infoServices.transactions)
  const [filter, setFilter] = useState({})

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.getTransaction())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  function renderItem({item, index}) {
    return <HistoryItem data={item} />
  }

  const dataShow = useMemo(() => {
    if (!filter?.from && !filter?.to) return transactions
    return transactions
      .filter(item => item.ordered_at >= filter?.from)
      .filter(item => (!!filter?.to ? item.ordered_at <= filter?.to : true))
  }, [filter, transactions])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('history')} />
      <DateFilterComponent
        onChange={data => {
          setFilter(data)
        }}
      />
      <FlatList
        data={dataShow}
        renderItem={renderItem}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
            tintColor={Colors().defaultRefreshSpinner}
            titleColor={Colors().defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  )
}

export const screenOptions = navData => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default HistoryScreen
