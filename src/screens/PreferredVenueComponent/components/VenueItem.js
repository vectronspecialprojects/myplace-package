import React, {useMemo} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import {useSelector} from 'react-redux'
import {TouchableCmp} from '../../../components/UtilityFunctions'
import {isIOS, responsiveHeight, responsiveWidth, shadow} from '../../../Themes/Metrics'
import FastImage from 'react-native-fast-image'
import Images from '../../../Themes/Images'
import Styles from '../../../Themes/Styles'
import Colors from '../../../Themes/Colors'
import {defaultDistance} from '../../../constants/constants'

function VenueItem({venueAddress, onPress, imgSource, venueName, distance}) {
  const useLegacyDesign = useSelector((state) => state.app.useLegacyDesign)
  const address = useMemo(() => {
    const after = venueAddress?.slice(venueAddress.indexOf(',') + 1)?.trim()
    const before = venueAddress?.slice(0, venueAddress.indexOf(','))?.trim()
    return {
      after,
      before,
    }
  }, [])
  return (
    <TouchableCmp onPress={onPress}>
      <View style={!useLegacyDesign && shadow}>
        <View
          style={[
            styles.container,
            {backgroundColor: Colors().location.cardBackground},
            !useLegacyDesign && {
              ...styles.newDesign,
              backgroundColor: Colors().defaultBackground,
            },
            !isIOS() && shadow,
          ]}>
          <FastImage
            source={imgSource ? {uri: imgSource} : Images.defaultImage}
            style={styles.image}
            resizeMode={'contain'}
          />
          <View style={styles.textWrapper}>
            <Text style={{...Styles.mediumCapText, alignSelf: 'flex-start', color: Colors().location.title}}>
              {venueName}
            </Text>
            <Text style={[Styles.xSmallNormalText, styles.address, {color: Colors().location.address}]}>
              {address?.before}
            </Text>
            <Text
              style={[
                Styles.xSmallNormalText,
                styles.address,
                {
                  marginTop: 0,
                  color: Colors().location.address,
                },
              ]}>
              {address?.after}
            </Text>
            {distance !== defaultDistance && (
              <Text style={{...Styles.xSmallNormalText, ...styles.address, marginTop: 0}}>
                {distance?.toFixed(2)} Km
              </Text>
            )}
          </View>
        </View>
      </View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: responsiveHeight(12),
    alignItems: 'center',
  },
  image: {
    width: responsiveWidth(100),
    aspectRatio: 1,
  },
  textWrapper: {
    flex: 1,
    height: '100%',
    paddingVertical: responsiveHeight(5),
    paddingHorizontal: responsiveWidth(15),
  },
  address: {
    textAlign: 'left',
    marginTop: responsiveHeight(10),
    lineHeight: responsiveHeight(20),
  },
  newDesign: {
    marginHorizontal: responsiveWidth(10),
    borderRadius: responsiveWidth(15),
    overflow: 'hidden',
  },
})

export default VenueItem
