import React, {useState} from 'react'
import {View, StyleSheet, FlatList, Text, RefreshControl} from 'react-native'
import {responsiveHeight} from '../../../Themes/Metrics'
import Styles from '../../../Themes/Styles'
import Colors from '../../../Themes/Colors'
import {localize} from '../../../locale/I18nConfig'
import VenueItem from './VenueItem'
import {isTrue} from '../../../utilities/utils'
import VenueTags from '../../../components/VenueTags'
import {useSelector} from 'react-redux'

function PreferredVenueVersion2({
  profile,
  navigation,
  venueTagsSetting,
  venueTagShow,
  isRefreshing,
  loadContent,
  savePreferredVenue,
}) {
  const [selectedTag, setSelectedTag] = useState(venueTagShow[0])
  const appFlags = useSelector((state) => state.app.appFlags)

  function renderItem({item, index}) {
    return (
      <VenueItem
        imgSource={item?.image}
        venueName={item?.name}
        venueAddress={item?.address}
        onPress={() => savePreferredVenue(item?.id)}
        distance={item?.distance}
      />
    )
  }

  return (
    <View style={styles.container}>
      {isTrue(venueTagsSetting?.isTagEnabled) &&
        isTrue(appFlags?.isMultipleVenue) &&
        !!venueTagShow?.length && (
          <VenueTags
            data={venueTagShow}
            selectedTag={selectedTag?.id}
            onPress={(venueTag) => setSelectedTag(venueTag)}
          />
        )}
      <View style={{flex: 1, marginTop: responsiveHeight(12)}}>
        <FlatList
          data={selectedTag?.listVenue}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
          ListEmptyComponent={
            <Text style={Styles.flatlistNoItems}>No venues found, please check again later.</Text>
          }
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={loadContent}
              tintColor={Colors().defaultRefreshSpinner}
              titleColor={Colors().defaultRefreshSpinner}
              title={localize('pullToRefresh')}
            />
          }
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})
export default PreferredVenueVersion2
