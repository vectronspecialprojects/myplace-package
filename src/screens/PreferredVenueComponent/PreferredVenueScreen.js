import React, {useState, useCallback, useMemo} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import PreferredVenueVersion2 from './components/PreferredVenueVersion2'
import * as infoServicesActions from '../../store/actions/infoServices'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import Toast from '../../components/Toast'

const PreferredVenueScreen = (props) => {
  const venues = useSelector((state) => state.infoServices.venues)
  const venueTagWithVenue = useSelector((state) => state.infoServices.venueTagWithVenue)
  const venueTagsSetting = useSelector((state) => state.infoServices.venueTagsSetting)
  const profile = useSelector((state) => state.infoServices.profile)
  const [selectedVenue, setSelectedVenue] = useState()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()

  const loadContent = useCallback(async () => {
    try {
      setIsRefreshing(true)
      await dispatch(infoServicesActions.fetchVenues())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
    }
  }, [dispatch])

  const venueTagShow = useMemo(() => {
    let newArr = venueTagWithVenue.filter((item) => !!item.listVenue?.length)
    newArr = [{id: 0, display_order: 0, name: 'All Venues', listVenue: venues}, ...newArr]
    return newArr
  }, [venueTagWithVenue])

  const savePreferredVenue = async (venue) => {
    try {
      setGlobalIndicatorVisibility(true)
      if (venue) {
        const rest = await dispatch(infoServicesActions.changePreferredVenue(venue))
        Toast.success(rest)
        props.navigation.pop()
      } else Alert.alert(localize('sorry'), localize('plsSelectPreferred'), [{text: localize('okay')}])
    } catch (e) {
      Alert.alert(localize('somethingWentWrong'), e.message, [{text: localize('okay')}])
    } finally {
      setGlobalIndicatorVisibility(false)
    }
  }

  return (
    <PreferredVenueVersion2
      navigation={props.navigation}
      profile={profile}
      venueTagShow={venueTagShow}
      venueTagsSetting={venueTagsSetting}
      loadContent={loadContent}
      isRefreshing={isRefreshing}
      savePreferredVenue={savePreferredVenue}
      setSelectedVenue={setSelectedVenue}
      selectedVenue={selectedVenue}
    />
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default PreferredVenueScreen
