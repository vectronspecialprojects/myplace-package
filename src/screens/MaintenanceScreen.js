import React from 'react'
import {View, StyleSheet, Text, Image} from 'react-native'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import Images from '../Themes/Images'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import {SafeAreaView} from 'react-native-safe-area-context'
import {logoImage} from "../constants/constants";

function MaintenanceScreen() {
  return (
    <View style={[styles.container, {backgroundColor: Colors().headerBackground}]}>
      <SafeAreaView style={styles.logoContainer}>
        <Image style={styles.logo} source={{uri: logoImage}} />
      </SafeAreaView>
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <Image source={Images.maintenance} style={[styles.image, {tintColor: Colors().white}]} />
        <Text style={[styles.content, {color: Colors().white}]}>We are currently undergoing maintenance.</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    fontFamily: Fonts.openSansBold,
    marginTop: responsiveHeight(50),
  },
  image: {
    width: responsiveWidth(200),
    height: responsiveHeight(200),
  },
  logo: {
    width: responsiveWidth(100),
    height: responsiveHeight(50),
    resizeMode: 'contain',
  },
  logoContainer: {
    alignItems: 'center',
  },
})

export default MaintenanceScreen
