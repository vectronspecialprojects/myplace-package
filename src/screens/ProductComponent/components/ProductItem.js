import React, {useMemo, useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Colors from '../../../Themes/Colors'
import QuantityComponent from './QuantityComponent'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'

function ProductItem({data, onChangeQuantity, isRedeem}) {
  const {product} = data

  const [quantity, setQuantity] = useState(data.qty || (data?.listingTypeId === 10 ? 1 : 0))

  const total = useMemo(() => {
    if (isRedeem) return +product?.point_price * quantity || 0
    return +product?.unit_price * quantity || 0
  }, [quantity])

  return (
    <View style={[styles.container, {backgroundColor: Colors().productItemScreen.cardBackground}]}>
      <View style={{flex: 2}}>
        <Text style={styles.name}>{product?.name}</Text>
        {product?.product_price_option !== 'free' && (
          <Text style={[styles.price, {color: Colors().productItemScreen.price}]}>
            {!isRedeem && '$'}
            {isRedeem ? +product?.point_price : product?.unit_price}
            {isRedeem && ' Points'}
          </Text>
        )}
        {product?.product_price_option === 'free' && (
          <Text style={[styles.price, {color: Colors().productItemScreen.price}]}>free</Text>
        )}
      </View>
      <QuantityComponent
        disabled={data?.listingTypeId === 10 ? true : false}
        setValue={(value) => {
          onChangeQuantity(value)
          setQuantity(value)
        }}
        value={quantity}
      />
      {product?.product_price_option !== 'free' && (
        <Text
          style={[styles.name, {flex: 1, textAlign: 'right', color: Colors().productItemScreen.productName}]}>
          {!isRedeem && '$'}
          {total.toFixed(isRedeem ? 0 : 2)}
          {isRedeem && ' Points'}
        </Text>
      )}
      {product?.product_price_option === 'free' && (
        <Text
          style={[styles.name, {flex: 1, textAlign: 'right', color: Colors().productItemScreen.productName}]}>
          free
        </Text>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: responsiveWidth(14),
    marginTop: responsiveHeight(16),
    marginHorizontal: responsiveWidth(10),
    borderRadius: responsiveWidth(10),
    alignItems: 'flex-end',
  },
  name: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSansBold,
  },
  price: {
    fontSize: responsiveFont(14),
  },
})

export default ProductItem
