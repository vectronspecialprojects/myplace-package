import React, {useEffect, useImperativeHandle, useState} from 'react'
import {View, StyleSheet, Modal, TouchableOpacity, Text, Switch, ScrollView, FlatList} from 'react-native'
import Colors from '../../../Themes/Colors'
import {deviceWidth, responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import {apiGetListCard} from '../../../utilities/ApiManage'
import TextInputView from '../../../components/TextInputView'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import ButtonView from '../../../components/ButtonView'
import BottomSheet from '../../../components/BottomSheet'
import {formatNumber} from '../../../utilities/utils'

function PaymentPopup({visible, onClose, amount, onSubmit}, ref) {
  const [listCard, setListCard] = useState('')
  const [name, setName] = useState('')
  const [number, setNumber] = useState('')
  const [expiryDate, setExpiryDate] = useState('')
  const [cvc, setCvc] = useState('')
  const [isSave, setSave] = useState(false)
  const [isUseSaveCard, setUseSaveCard] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [isAddNewCard, setAddNewCard] = useState(false)
  const [token, setToken] = useState('')
  const [brand, setBrand] = useState('')
  const [showSelectCard, setShowSelectCard] = useState(false)

  useEffect(() => {
    getListCard()
  }, [])

  useImperativeHandle(ref, () => ({
    setError: (message) => {
      setErrorMessage(message)
    },
  }))

  async function getListCard() {
    try {
      const res = await apiGetListCard()
      console.log('apiGetListCard', res)
      if (!res.ok) throw new Error(res.message)
      setListCard(res.data?.cards?.data)
      if (res.data?.cards?.data?.length > 0) {
        handleSelectCard(res.data?.cards?.data[0])
      }
    } catch (e) {}
  }

  function handleSelectCard(card) {
    const {last4, id, brand, exp_month, exp_year, name} = card || {}
    setNumber(last4)
    setExpiryDate(`${exp_month}/${exp_year}`)
    setToken(id)
    setBrand(brand)
    setName(name)
    setUseSaveCard(true)
  }

  function handleClearCard() {
    setNumber('')
    setExpiryDate('')
    setToken('')
    setBrand('')
    setName('')
    setCvc('')
  }

  function handleFormatDate(value) {
    let array = value.split('/')
    if (array[1]?.length > 4) return
    if (array.length === 2 && array[1]?.length <= 4) {
      setExpiryDate(value)
    } else if (array[0].length === 1) {
      setExpiryDate(value)
    } else {
      setExpiryDate((current) => {
        if (current.length > value.length) {
          return value
        } else {
          return value + '/'
        }
      })
    }
  }

  return (
    <Modal
      visible={visible}
      transparent={true}
      onRequestClose={() => {}}
      animationType={'fade'}
      animated={true}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: Colors().opacity,
        }}>
        <View style={[styles.container, {backgroundColor: Colors().first}]}>
          <ScrollView>
            <View style={{padding: responsiveWidth(15)}}>
              <Text style={[styles.title, {color: Colors().white}]}>Credit card details</Text>
              <Text style={[styles.title, {fontSize: responsiveFont(15), color: Colors().white}]}>
                Please enter the card details
              </Text>
              {!!listCard && !isAddNewCard && (
                <TextInputView
                  value={`${brand} xxxx xxxx xxxx ${number}`}
                  inputStyle={{backgroundColor: Colors().defaultBackground}}
                  textInputStyle={{color: Colors().second}}
                  rightIcon={<MaterialIcons name={'keyboard-arrow-down'} size={25} color={Colors().second} />}
                  onPress={() => setShowSelectCard(true)}
                  editable={false}
                />
              )}
              <TextInputView
                placeholder={'Name on the card'}
                inputStyle={{backgroundColor: Colors().defaultBackground}}
                value={name}
                editable={!isUseSaveCard}
                onChangeText={(text) => setName(text)}
              />
              <TextInputView
                placeholder={'Card Number'}
                inputStyle={{backgroundColor: Colors().defaultBackground}}
                value={formatNumber(+number)}
                editable={!isUseSaveCard}
                maxLength={19}
                keyboardType={'numeric'}
                onChangeText={(text) => setNumber(text.replace(/-/g, ''))}
              />
              <View style={{flexDirection: 'row'}}>
                <TextInputView
                  placeholder={'Expiry Date'}
                  style={{flex: 1, marginRight: responsiveWidth(10)}}
                  inputStyle={{backgroundColor: Colors().defaultBackground}}
                  value={expiryDate}
                  editable={!isUseSaveCard}
                  onChangeText={(text) => handleFormatDate(text)}
                  maxLength={7}
                />
                <TextInputView
                  placeholder={'CVC'}
                  style={{flex: 1}}
                  inputStyle={{backgroundColor: Colors().defaultBackground}}
                  value={cvc}
                  editable={!isUseSaveCard}
                  onChangeText={(text) => setCvc(text)}
                />
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={[styles.textSaveCard, {color: Colors().white}]}>
                  Would you like to save this card?
                </Text>
                <Switch value={isSave} onValueChange={(value) => setSave(value)} tintColor={Colors().second} />
              </View>
              {!!errorMessage && (
                <Text style={{color: Colors().red, marginTop: responsiveHeight(10)}}>{errorMessage}</Text>
              )}
              {!!listCard && (
                <ButtonView
                  title={isAddNewCard ? 'Selected a Saved Card' : 'Add New Card'}
                  titleStyle={{color: Colors().second}}
                  style={{
                    backgroundColor: Colors().defaultBackground,
                    marginTop: responsiveHeight(30),
                  }}
                  onPress={() => {
                    setAddNewCard(!isAddNewCard)
                    if (isAddNewCard) {
                      handleSelectCard(listCard[0])
                      setUseSaveCard(true)
                    } else {
                      handleClearCard()
                      setUseSaveCard(false)
                    }
                  }}
                />
              )}
              <Text style={[styles.total, {color: Colors().paymentPopup.totalAmount}]}>
                Total Amount: ${amount.toFixed(2)}
              </Text>
            </View>
          </ScrollView>
          <View style={{flexDirection: 'row'}}>
            <ButtonView
              titleStyle={{color: Colors().paymentPopup.buttonText}}
              title={'Cancel'}
              style={[styles.button, {marginRight: 3, backgroundColor: Colors().paymentPopup.buttonBackground}]}
              onPress={onClose}
            />
            <ButtonView
              title={'Continue'}
              titleStyle={{color: Colors().paymentPopup.buttonText}}
              style={[styles.button, {backgroundColor: Colors().paymentPopup.buttonBackground}]}
              onPress={() =>
                onSubmit({
                  name,
                  number,
                  cvc,
                  save_card: isSave,
                  use_saved_card: isUseSaveCard,
                  expiryDate,
                  token,
                })
              }
            />
          </View>
        </View>
      </View>
      <BottomSheet
        visible={showSelectCard}
        headerTitle={'Select A Card'}
        showConfirmButton={false}
        onClose={() => setShowSelectCard(false)}>
        <FlatList
          data={listCard}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                style={[styles.itemCard, {borderColor: Colors().gray}]}
                onPress={() => {
                  handleSelectCard(item)
                  setShowSelectCard(false)
                }}>
                <Text
                  style={{
                    fontFamily: Fonts.openSansBold,
                    fontSize: responsiveFont(15),
                  }}>{`${item.brand} xxxx xxxx xxxx ${item.last4}`}</Text>
                <Text>Exp: {`${item.exp_month}/${item.exp_year}`}</Text>
              </TouchableOpacity>
            )
          }}
        />
      </BottomSheet>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    width: deviceWidth() * 0.9,
    borderRadius: responsiveWidth(14),
    overflow: 'hidden',
  },
  button: {
    flex: 1,
    borderRadius: 0,
  },
  total: {
    fontSize: responsiveFont(18),
    fontFamily: Fonts.openSansBold,
    textAlign: 'center',
    marginVertical: responsiveHeight(30),
  },
  title: {
    fontSize: responsiveFont(16),
    fontFamily: Fonts.openSansBold,
    textAlign: 'center',
    marginBottom: responsiveHeight(18),
  },
  textSaveCard: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans,
    flex: 1,
  },
  itemCard: {
    height: responsiveHeight(50),
    justifyContent: 'center',
    borderBottomWidth: 0.5,
  },
})

export default React.forwardRef(PaymentPopup)
