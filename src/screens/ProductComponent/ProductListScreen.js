import React, {useEffect, useMemo, useRef, useState} from 'react'
import {View, StyleSheet, FlatList, Text} from 'react-native'
import Styles from '../../Themes/Styles'
import SubHeaderBar from '../../components/SubHeaderBar'
import ProductItem from './components/ProductItem'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import ButtonView from '../../components/ButtonView'
import PaymentPopup from './components/PaymentPopup'
import Colors from '../../Themes/Colors'
import fonts from '../../Themes/Fonts'
import {createPaymentMethod, initStripe} from '../../utilities/Stripe'
import {apiConfirmOrder, apiCreateOrder, sendToken} from '../../utilities/ApiManage'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {fetchProfile} from '../../store/actions/infoServices'
import Toast from '../../components/Toast'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'

function ProductListScreen({route, navigation}) {
  const {listingId, type} = route?.params
  const [products, setProducts] = useState(route?.params?.products)
  const [isShowPayment, setShowPayment] = useState(false)
  const dispatch = useDispatch()
  const orderToken = useRef()
  const user_credentials = useSelector((state) => state.authServices.user_credentials)
  const popupPaymentRef = useRef()

  useEffect(() => {
    initStripe()
  }, [])

  const isRedeem = useMemo(() => {
    if (!products?.length) return false
    let redeem = true
    products.forEach((product) => {
      if (
        !(
          (!!+product?.product?.point_price && product?.product?.product_price_option === 'point_price') ||
          product?.product?.product_price_option === 'free'
        )
      )
        redeem = false
    })
    return redeem
  }, [products])

  async function handleCreateOrder() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await apiCreateOrder({
        type: isRedeem ? 'point' : 'cash',
        order: JSON.stringify(products),
        total: JSON.stringify({
          point: isRedeem ? total : 0,
          cash: isRedeem ? 0 : total,
        }),
        listing_id: listingId,
      })
      if (!res.ok) throw new Error(res?.message)
      orderToken.current = res?.token
      if (isRedeem) {
        const resConfirm = await apiConfirmOrder(orderToken.current, 'confirmed', '')
        if (!resConfirm.ok) throw new Error(res?.message)
        dispatch(fetchProfile())
        navigation.pop()
        Toast.success(localize('product.messageSuccess'))
      } else {
        setShowPayment(true)
      }
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err?.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  async function handleCancelOrder() {
    try {
      setShowPayment(false)
      const res = await apiConfirmOrder(orderToken.current, 'cancelled', '')
    } catch (e) {}
  }

  function handleChangeValue(value, index) {
    let newList = [...products]
    newList[index] = {
      ...newList[index],
      qty: value,
    }
    setProducts(newList)
  }

  const errorShow = () => {
    Alert.alert(localize('somethingWentWrong'), localize('product.messageError'), [{text: localize('okay')}])
    navigation.pop()
  }

  useEffect(() => {
    if (type === 10) {
      if (products?.length === 1 && products[0]?.product?.product_price_option) handleChangeValue(1, 0)
      else errorShow()
    } else {
      let isProductsValid = true
      for (let i = 1; i < products?.length; i++) {
        if (!products[i - 1]?.product?.product_price_option) isProductsValid = false
        if (products[i]?.product?.product_price_option !== products[i - 1]?.product?.product_price_option)
          isProductsValid = false
      }
      if (!isProductsValid) errorShow()
    }
  }, [])

  function renderItem({item, index}) {
    let data = {...item, listingTypeId: type}
    return (
      <ProductItem
        data={data}
        isRedeem={isRedeem}
        onChangeQuantity={(value) => {
          handleChangeValue(value, index)
        }}
      />
    )
  }

  async function checkCardWithStripe(card) {
    try {
      const {name, number, cvc, expiryDate} = card
      let date = expiryDate.split('/')
      const res = await createPaymentMethod(name, number, cvc, +date[0], +date[1])
      return res.tokenId
    } catch (err) {
      popupPaymentRef.current?.setError(err?.message)
      return null
    }
  }

  async function handleSubmitOrder(card) {
    try {
      let token_stripe = card?.token
      if (!card?.use_saved_card) {
        token_stripe = await checkCardWithStripe(card)
      }
      if (token_stripe) {
        setShowPayment(false)
        dispatch(setGlobalIndicatorVisibility(true))
        // const confirmRes = await apiConfirmOrder(orderToken.current, 'confirmed', '')
        const resToken = await sendToken(orderToken.current, token_stripe, card, user_credentials)
        if (!resToken.ok) throw new Error(resToken.message)
        navigation.pop()
        Toast.success(localize('product.messageSuccess'))
      }
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err?.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const total = useMemo(() => {
    if (type === 10) {
      if (products[0]?.product?.product_price_option === 'point_price')
        return +products[0].product?.point_price || 0
      if (products[0]?.product?.product_price_option === 'dollar_price')
        return +products[0].product?.unit_price || 0
      if (products[0]?.product?.product_price_option === 'free') return 0
    } else {
      let value = 0
      products.map((item) => {
        if (item?.product?.product_price_option === 'point_price')
          value += item?.qty * item?.product?.point_price || 0
        if (item?.product?.product_price_option === 'dollar_price')
          value += item?.qty * item?.product?.unit_price || 0
        if (item?.product?.product_price_option === 'free') value += 0
      })
      return value
    }
  }, [products])

  const allowProceed = useMemo(() => {
    let quantity = 0
    products.map((item) => {
      quantity += item?.qty || 0
    })
    if (!!quantity) return true
    return false
  }, [products, type])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('product.buyNow')} />
      <FlatList
        data={products}
        extraData={total}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        ListFooterComponent={
          products[0]?.product?.product_price_option !== 'free' ? (
            <Text style={[styles.total, {color: Colors().second}]}>
              Total: {!isRedeem && '$'}
              {total.toFixed(isRedeem ? 0 : 2)}
              {isRedeem && ' Points'}
            </Text>
          ) : (
            <Text style={[styles.total, {color: Colors().second}]}>Free</Text>
          )
        }
      />

      <ButtonView
        style={{marginBottom: responsiveHeight(25), marginHorizontal: responsiveWidth(24)}}
        title={localize('product.proceed')}
        disabled={!allowProceed}
        onPress={() => handleCreateOrder()}
      />
      <PaymentPopup
        visible={isShowPayment}
        onClose={() => handleCancelOrder()}
        onSubmit={(card) => {
          handleSubmitOrder(card)
        }}
        amount={total}
        ref={popupPaymentRef}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  total: {
    textAlign: 'right',
    marginTop: responsiveHeight(15),
    marginRight: responsiveWidth(20),
    fontSize: responsiveFont(14),
    fontFamily: fonts.openSansBold,
  },
})

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default ProductListScreen
