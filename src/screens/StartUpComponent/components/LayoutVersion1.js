import React from 'react'
import {View, StyleSheet, Image, Text} from 'react-native'
import ButtonView from '../../../components/ButtonView'
import {localize} from '../../../locale/I18nConfig'
import Colors from '../../../Themes/Colors'
import RouteKey from '../../../navigation/RouteKey'
import {responsiveFont, responsiveHeight} from '../../../Themes/Metrics'
import {navigate} from '../../../navigation/NavigationService'
import {logoImage, version} from '../../../constants/constants'
import {useSelector} from 'react-redux'
import {isTrue} from '../../../utilities/utils'

function LayoutVersion1({isAppSetup}) {
  const appFlags = useSelector((state) => state.app.appFlags)
  return (
    <>
      <View style={styles.logoContainer}>
        {isTrue(appFlags?.app_is_show_logo) && (
          <Image source={{uri: logoImage, cache: 'force-cache'}} style={styles.logo} resizeMode={'contain'} />
        )}
      </View>
      <View style={styles.buttonsContainer}>
        <ButtonView
          disabled={!isAppSetup}
          title={localize('startUpScreen.welcomeBack')}
          style={[
            styles.buttonBorder,
            {
              backgroundColor: Colors().firstPage.firstButtonBackground,
              borderColor: Colors().firstPage.firstButtonBorder,
            },
          ]}
          titleStyle={{color: Colors().firstPage.firstButtonText}}
          descStyle={{color: Colors().firstPage.firstButtonText}}
          desc={localize('startUpScreen.signIn')}
          onPress={() => {
            navigate(RouteKey.LoginScreen)
          }}
        />
        <ButtonView
          disabled={!isAppSetup}
          title={localize('startUpScreen.imNew')}
          desc={localize('startUpScreen.joinNow')}
          style={[
            styles.buttonBorder,
            {
              backgroundColor: Colors().firstPage.secondButtonBackground,
              borderColor: Colors().firstPage.secondButtonBorder,
            },
          ]}
          titleStyle={{color: Colors().firstPage.secondButtonText}}
          descStyle={{color: Colors().firstPage.secondButtonText}}
          onPress={() => {
            navigate(RouteKey.SignupScreen)
          }}
        />

        {isTrue(appFlags?.app_account_match) && (
          <ButtonView
            title={localize('startUpScreen.alreadyAMember')}
            desc={localize('startUpScreen.matchMyAccount')}
            style={[
              styles.buttonBorder,
              {
                borderColor: Colors().firstPage.borderMatchButton,
                backgroundColor: Colors().firstPage.backgroundMatchButton,
              },
            ]}
            titleStyle={{color: Colors().firstPage.textMatchButton}}
            descStyle={{color: Colors().firstPage.textMatchButton}}
            onPress={() => {
              navigate(RouteKey.VerifyPhoneAndEmail)
            }}
          />
        )}

        <Text
          style={{
            color: Colors().white,
            fontSize: responsiveFont(12),
            textAlign: 'center',
          }}>
          {localize('startUpScreen.version')}: {version}
        </Text>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: '100%',
  },
  logoContainer: {
    width: '100%',
    height: '70%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: '80%',
    height: responsiveHeight(150),
    resizeMode: 'contain',
  },
  buttonsContainer: {
    paddingHorizontal: responsiveHeight(20),
    paddingVertical: responsiveHeight(20),
  },
  buttonBorder: {
    borderWidth: 2,
    marginBottom: responsiveHeight(8),
  },
})
export default LayoutVersion1
