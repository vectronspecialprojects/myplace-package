import React from 'react'
import {View, StyleSheet, Image, Text} from 'react-native'
import Images from '../../../Themes/Images'
import {deviceWidth, isIOS, responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import PagingScrollView from '../../../components/PagingScrollView'
import {useSelector} from 'react-redux'
import CustomIcon from '../../../components/CustomIcon'
import ButtonView from '../../../components/ButtonView'
import {localize} from '../../../locale/I18nConfig'
import Colors from '../../../Themes/Colors'
import Fonts from '../../../Themes/Fonts'
import RouteKey from '../../../navigation/RouteKey'
import {navigate} from '../../../navigation/NavigationService'
import {logoImage, version} from '../../../constants/constants'
import {isTrue} from '../../../utilities/utils'

function LayoutVersion2({isAppSetup}) {
  const welcomeInstruction = useSelector((state) => state.app.welcomeInstruction)
  const welcomeInstructionEnable = useSelector((state) => state.app.welcomeInstructionEnable)
  const appFlags = useSelector((state) => state.app.appFlags)

  function renderItem(item, index) {
    return (
      <View style={styles.itemView} key={index.toString()}>
        <View style={[styles.imageContainer, {backgroundColor: Colors().white}]}>
          <CustomIcon
            size={responsiveHeight(200)}
            image_icon={item.image_icon}
            name={item.icon}
            icon_selector={item.icon_selector || 'image'}
          />
        </View>
        <Text style={[styles.title, {color: Colors().startUpScreen.title}]}>{item.title}</Text>
        <View style={styles.subTitlePadding}>
          <Text style={[styles.subTitle, {color: Colors().startUpScreen.title}]}>{item.content}</Text>
        </View>
      </View>
    )
  }

  return (
    <>
      <View style={styles.logoContainer}>
        {isTrue(appFlags?.app_is_show_logo) && <Image source={{uri: logoImage}} style={styles.logo} />}
      </View>
      {welcomeInstructionEnable ? (
        <PagingScrollView
          style={{width: deviceWidth(), marginTop: responsiveHeight(50)}}
          total={welcomeInstruction?.length}
          showDot={true}
          dotContainerStyle={{bottom: isIOS() ? 0 : -5}}>
          {welcomeInstruction?.map(renderItem)}
        </PagingScrollView>
      ) : (
        <View style={{height: responsiveHeight(200)}} />
      )}
      <View style={styles.buttonsContainer}>
        <ButtonView
          disabled={!isAppSetup}
          title={localize('startUpScreen.welcomeBack')}
          style={[
            styles.buttonBorder,
            {
              backgroundColor: Colors().firstPage.firstButtonBackground,
              borderColor: Colors().firstPage.firstButtonBorder,
            },
          ]}
          titleStyle={{color: Colors().firstPage.firstButtonText, fontFamily: Fonts.openSans}}
          descStyle={{color: Colors().firstPage.firstButtonText}}
          onPress={() => {
            navigate(RouteKey.LoginScreen)
          }}
        />
        <ButtonView
          disabled={!isAppSetup}
          title={localize('startUpScreen.imNew')}
          style={[
            styles.buttonBorder,
            {
              backgroundColor: Colors().firstPage.backgroundMatchButton,
              borderColor: Colors().firstPage.firstButtonBorder,
            },
          ]}
          titleStyle={{color: Colors().firstPage.secondButtonText, fontFamily: Fonts.openSans}}
          descStyle={{color: Colors().firstPage.secondButtonText}}
          onPress={() => {
            navigate(RouteKey.SignupScreen)
          }}
        />

        {isTrue(appFlags?.app_account_match) && (
          <View style={{alignItems: 'center', marginVertical: responsiveHeight(20)}}>
            <Text
              style={{
                color: Colors().firstPage.textMatchButton,
              }}>
              {localize('startUpScreen.alreadyAMember')}
            </Text>
            <Text
              style={{color: Colors().firstPage.textMatchButton, textDecorationLine: 'underline'}}
              onPress={() => {
                navigate(RouteKey.VerifyPhoneAndEmail)
              }}>
              {localize('startUpScreen.matchMyAccount')}
            </Text>
          </View>
        )}
        <Text
          style={{
            color: Colors().white,
            fontSize: responsiveFont(12),
            textAlign: 'center',
          }}>
          {localize('startUpScreen.version')}: {version}
        </Text>
      </View>
    </>
  )
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    width: '100%',
  },
  logoContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: responsiveWidth(230),
    height: responsiveHeight(80),
    resizeMode: 'contain',
    marginTop: responsiveHeight(50),
  },
  buttonsContainer: {
    paddingHorizontal: responsiveHeight(20),
    paddingVertical: responsiveHeight(25),
  },
  buttonBorder: {
    borderWidth: 2,
    marginBottom: responsiveHeight(8),
    height: responsiveHeight(40),
  },
  itemView: {
    alignItems: 'center',
    width: deviceWidth(),
    height: responsiveHeight(310),
    marginBottom: responsiveHeight(15),
  },
  title: {
    fontSize: responsiveFont(22),
    fontFamily: Fonts.openSansBold,
    marginTop: responsiveHeight(10),
  },
  subTitle: {
    fontSize: responsiveFont(14),
    marginTop: responsiveHeight(10),
    textAlign: 'center',
    fontFamily: Fonts.openSans,
  },
  imageContainer: {
    width: responsiveHeight(200),
    height: responsiveHeight(200),
    borderRadius: responsiveHeight(100),
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  subTitlePadding: {
    paddingHorizontal: responsiveWidth(30),
  },
})
export default LayoutVersion2
