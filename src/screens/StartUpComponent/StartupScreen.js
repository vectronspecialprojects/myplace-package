import React, {useEffect} from 'react'
import {View, StyleSheet, Image, ImageBackground, Text, Platform} from 'react-native'
import Colors from '../../Themes/Colors'
import Images from '../../Themes/Images'
import {useSelector} from 'react-redux'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import LayoutVersion1 from './components/LayoutVersion1'
import LayoutVersion2 from './components/LayoutVersion2'
import HamburgerButton from '../../navigation/drawerComponents/HamburgerButton'
import {backgroundImage} from '../../constants/constants'
import FastImage from 'react-native-fast-image'
import {deviceHeight, deviceWidth} from '../../Themes/Metrics'

const StartupScreen = (props) => {
  const isAppSetup = useSelector((state) => state.app.isAppSetup)
  const useLegacyDesign = useSelector((state) => state.app.useLegacyDesign)
  useEffect(() => {
    if (!isAppSetup) {
      Alert.alert(localize('startUpScreen.appNotSetup'), localize('startUpScreen.message'), [
        {text: localize('okay')},
      ])
    }
  }, [isAppSetup])

  return (
    <View style={[styles.screen, {backgroundColor: Colors().startUpScreen.background}]}>
      <ImageBackground source={{uri: backgroundImage, cache: 'force-cache'}} style={styles.backgroundImage}>
        {useLegacyDesign ? (
          <LayoutVersion1 isAppSetup={isAppSetup} />
        ) : (
          <LayoutVersion2 isAppSetup={isAppSetup} />
        )}
      </ImageBackground>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerTransparent: true,
    headerTitle: () => null,
    headerBackground: () => null,
    headerLeft: () => <HamburgerButton navData={navData} position={'left'} />,
    headerRight: () => <HamburgerButton navData={navData} position={'right'} />,
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
  },
  backgroundImage: {
    flex: 1,
    alignSelf: 'center',
    width: '100%',
    justifyContent: 'center',
  },
})

export default StartupScreen
