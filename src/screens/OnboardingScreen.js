/**
 * Created by Hong HP on 3/12/20.
 */
import React from 'react'
import {Dimensions, StatusBar, StyleSheet, Text, View} from 'react-native'
import Colors from '../Themes/Colors'
import FastImage from 'react-native-fast-image'
import Images from '../Themes/Images'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import {connect, useDispatch, useSelector} from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import PagingScrollView from '../components/PagingScrollView'
import {setAppState} from '../store/actions/appServices'
import CustomIcon from '../components/CustomIcon'
import {logoImage} from '../constants/constants'

const {width, height} = Dimensions.get('window')

function OnboardingScreen(props) {
  const dispatch = useDispatch()
  const welcomeInstruction = useSelector((state) => state.app.welcomeInstruction)

  function handleFinish(route) {
    dispatch(setAppState('Main'))
    AsyncStorage.setItem('isSecondTime', 'true')
  }

  return (
    <View style={[styles.container, {backgroundColor: Colors().onBoarding.background}]}>
      <View style={{flex: 1}}>
        <PagingScrollView
          style={{width: width}}
          total={welcomeInstruction?.length}
          finishAction={(route) => {
            handleFinish(route)
          }}
          showDot={true}
          hasAction={true}>
          {welcomeInstruction?.map((item, index) => {
            return (
              <View style={styles.itemView} key={index.toString()}>
                <FastImage source={{uri: logoImage}} style={styles.image} resizeMode={'contain'} />
                <View
                  style={{
                    backgroundColor: Colors().onBoarding.contentBackground,
                    alignItems: 'center',
                    paddingVertical: responsiveHeight(20),
                    width: '70%',
                  }}>
                  <CustomIcon
                    size={70}
                    image_icon={item.image_icon}
                    name={item.icon}
                    icon_selector={item.icon_selector}
                  />
                  <Text style={[styles.title, {color: Colors().textColor}]}>{item.title}</Text>
                  <View style={styles.subTitlePadding}>
                    <Text style={[styles.subTitle, {color: Colors().black}]}>{item.content}</Text>
                  </View>
                </View>
              </View>
            )
          })}
        </PagingScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  skipButton: {
    position: 'absolute',
    top: 10,
    right: 15,
    fontSize: 15,
  },
  itemView: {
    alignItems: 'center',
    width,
    height,
  },
  title: {
    fontSize: responsiveFont(30),
    fontFamily: Fonts.openSansBold,
    marginVertical: responsiveHeight(10),
  },
  subTitle: {
    fontSize: responsiveFont(16),
    marginTop: responsiveHeight(30),
    textAlign: 'center',
  },
  image: {
    width: responsiveWidth(200),
    height: responsiveHeight(100),
    marginBottom: responsiveHeight(40),
    marginTop: responsiveHeight(120),
  },
  bgImage: {
    width: width,
    height: responsiveHeight(373),
    paddingHorizontal: responsiveWidth(30),
    marginTop: responsiveHeight(52),
  },
  subTitlePadding: {
    paddingHorizontal: responsiveWidth(30),
  },
})

export default OnboardingScreen
