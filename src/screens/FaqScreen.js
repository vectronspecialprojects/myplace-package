import React, {useState, useCallback, useEffect, useMemo} from 'react'
import {View, StyleSheet, RefreshControl, FlatList, Text} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import FaqItem from './../components/FaqItem'
import SearchBar from './../components/SearchBar'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig';

const FaqScreen = (props) => {
  const title = props.route.params.params?.page_name
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()
  const faq = useSelector((state) => state.infoServices.faq)
  const [search, setSearch] = useState('')

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchFaq())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const showData = useMemo(() => {
    if (!faq) return []
    if (!search) return faq
    let key = search.toLowerCase()
    return faq.filter(
      (item) => item.question.toLowerCase().includes(key) || item.answer.toLowerCase().includes(key),
    )
  }, [search, faq])

  function renderItem({item, index}) {
    return <FaqItem title={item.question} content={item.answer} />
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      <SearchBar onChangeText={(text) => setSearch(text)} style={{marginTop: 30}}  backgroundColor={Colors().faq.searchBox} />
      <FlatList
        data={showData}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={
          <Text style={Styles.flatlistNoItems}>No Faq found, please check again later</Text>
        }
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
            tintColor={Colors().defaultRefreshSpinner}
            titleColor={Colors().defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default FaqScreen
