import React from 'react'
import {View, ScrollView, StyleSheet} from 'react-native'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import HeaderLeftButton from '../components/HeaderLeftButton'
import {localize} from '../locale/I18nConfig'

const EnquiriesScreen = (props) => {
  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('enquiry.title')} />
      <View style={{flex: 1}}>
        <ScrollView></ScrollView>
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default EnquiriesScreen
