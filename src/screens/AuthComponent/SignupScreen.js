import React, {useReducer, useCallback, useState, useMemo, useEffect} from 'react'
import {View, StyleSheet, KeyboardAvoidingView, ScrollView, Text} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {isIOS, responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import * as authServicesActions from '../../store/actions/authServices'
import Colors from '../../Themes/Colors'
import Styles from '../../Themes/Styles'
import Fonts from '../../Themes/Fonts'
import ButtonView from '../../components/ButtonView'
import {signupData} from '../../modals/modals'
import TermAndCondition from '../../components/TermAndCondition'
import {
  getStateFromPostCode,
  getStatusBarHeight,
  isEmptyValues,
  isTrue,
  parseStringToJson,
} from '../../utilities/utils'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import * as api from '../../utilities/ApiManage'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import DropDownList from '../../components/DropDownList'
import CheckBox from '../../components/CheckBox'
import ProfileFieldInput from '../../components/ProfileFieldInput'
import SubHeaderBar from '../../components/SubHeaderBar'

const BY_VENUE = 'by_venue'
const BY_VENUE_TAG = 'by_venue_tag'
const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const GAMING = {
  odyssey: 'odyssey',
  igt: 'igt',
}

const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    }
  }
  return state
}

const SignupScreen = (props) => {
  const dispatch = useDispatch()
  const venues = useSelector((state) => state.infoServices.venues)
  const emailNotAllowedDomains = useSelector((state) => state.app.emailNotAllowedDomains)
  const memberDefaultTierDetail = useSelector((state) => state.app.memberDefaultTierDetail)
  const memberDefaultTierOption = useSelector((state) => state.app.memberDefaultTierOption)
  const appFlags = useSelector((state) => state.app.appFlags)
  const [listTier, setListTier] = useState('')
  const [options, setOptions] = useState([])
  const [gamingFields, setGamingFields] = useState([])
  const [showSelectVenue, setShowSelectVenue] = useState(false)
  const [optOutMarketing, setOptOutMarketing] = useState(false)
  const [agreeTermAndCondition, setAgreeTermCondition] = useState(false)

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {},
    inputValidities: {},
    formIsValid: false,
  })

  useEffect(() => {
    getProfileSetting()
    if (isTrue(appFlags?.gaming_system_enable)) getGaming()
  }, [])

  useEffect(() => {
    if (memberDefaultTierOption === BY_VENUE && formState.inputValues?.venue?.value) {
      let tierId = +memberDefaultTierDetail?.[formState.inputValues?.venue?.value]?.tier
      if (tierId) inputChangeHandler('tier', tierId, true)
    }
  }, [formState.inputValues?.venue_id])

  useEffect(() => {
    if (formState.inputValues?.post_code?.length >= 3) {
      const state = getStateFromPostCode(+formState.inputValues?.post_code)
      inputChangeHandler('state', state, true)
      if (memberDefaultTierOption === BY_VENUE_TAG && state) {
        const tierId = +memberDefaultTierDetail?.[state]?.tier
        if (tierId) inputChangeHandler('tier', tierId, true)
      }
    }
  }, [formState.inputValues?.post_code])

  async function getProfileSetting() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const response = await api.getUserProfileSetting()
      let hasTier = false
      if (!response.ok) {
        throw new Error(response?.message)
      }

      let list = parseStringToJson(response?.bepozcustomfield?.extended_value)
      list?.map((e) => {
        if (e.required) {
          dispatchFormState({
            type: FORM_INPUT_UPDATE,
            input: e?.id,
            value: '',
            isValid: false,
          })
        }
        if (e.fieldType === 'Tier') {
          hasTier = true
        }
      })
      if (hasTier) setListTier(response.data)
      list = list.filter((item) => item.displayInApp).sort((a, b) => +a.displayOrder - +b.displayOrder)
      setOptions(list)
    } catch (e) {
      Alert.alert(localize('signUp.titleGetTier'), e.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  async function getGaming() {
    try {
      const response = await api.apiGetGaming()
      if (!response.ok) {
        throw new Error(response?.message)
      }
      let list = parseStringToJson(response?.bepozcustomfield?.extended_value)
      list = list
        .filter((item) => isTrue(item.displayInApp))
        .sort((a, b) => +a.displayOrder - +b.displayOrder)
      list?.map((e) => {
        if (e.required) {
          dispatchFormState({
            type: FORM_INPUT_UPDATE,
            input: e?.id,
            value: '',
            isValid: false,
          })
        }
      })
      setGamingFields(list)
    } catch (e) {
      Alert.alert(localize('signUp.titleGetGaming'), e.message, [{text: localize('okay')}])
    }
  }

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        input: inputIdentifier,
        value: inputValue,
        isValid: inputValidity,
      })
    },
    [dispatchFormState],
  )

  const signupHandler = async () => {
    let data = new signupData(
      formState.inputValues.venue_id?.value,
      formState.inputValues.first_name,
      formState.inputValues.last_name,
      formState.inputValues.email,
      formState.inputValues.dob,
      formState.inputValues.password,
      formState.inputValues.password_confirmation,
      formState.inputValues.mobile,
      true,
      formState.inputValues.tier,
      optOutMarketing,
    )
    dispatch(setGlobalIndicatorVisibility(true))
    data = {
      ...formState.inputValues,
      ...data,
    }
    try {
      if (appFlags?.gaming_system === GAMING.odyssey) {
        await dispatch(authServicesActions.signupOdyssey(data))
      } else if (appFlags?.gaming_system === GAMING.igt) {
        await dispatch(authServicesActions.signupIGT(data))
      } else {
        await dispatch(authServicesActions.signup(data))
      }
    } catch (err) {
      Alert.alert(localize('signUp.titleErrSignUp'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const dropDownItems = useMemo(() => {
    const items = []
    let data = venues
    for (const key in data) {
      items.push({label: data[key].name, value: data[key].id})
    }
    return items
  }, [venues])

  const dropDownTier = useMemo(() => {
    if (!listTier?.length) return []
    const list = []
    listTier?.map((item) => {
      list.push({
        label: item?.name,
        value: item?.id,
      })
    })
    return list
  }, [listTier])

  const customFields = (values) => {
    if (isEmptyValues(values)) return []
    let list = []
    Object.values(values).map((item) => {
      list.push({
        label: item?.fieldName || item,
        value: item?.id || item,
      })
    })
    return list
  }

  const renderSignUpFields = useCallback(() => {
    return options?.map((item, index) => {
      return (
        <ProfileFieldInput
          key={index.toString()}
          type={item.fieldType}
          item={item}
          customFields={customFields}
          setShowSelectVenue={setShowSelectVenue}
          dropDownTier={dropDownTier}
          emailNotAllowedDomains={emailNotAllowedDomains}
          formState={formState}
          inputChangeHandler={inputChangeHandler}
        />
      )
    })
  }, [options, formState.inputValues])

  const renderGaming = useCallback(() => {
    return gamingFields?.map((item, index) => {
      return (
        <ProfileFieldInput
          key={index.toString()}
          type={item.fieldType}
          item={item}
          customFields={customFields}
          setShowSelectVenue={setShowSelectVenue}
          dropDownTier={dropDownTier}
          emailNotAllowedDomains={emailNotAllowedDomains}
          formState={formState}
          inputChangeHandler={inputChangeHandler}
        />
      )
    })
  }, [gamingFields])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('signUp.title')} alignLeft />
      <KeyboardAvoidingView
        style={{paddingHorizontal: responsiveWidth(20)}}
        behavior={isIOS() ? 'padding' : null}
        keyboardVerticalOffset={getStatusBarHeight()}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          style={{paddingTop: responsiveHeight(10)}}>
          {renderSignUpFields()}
          {isTrue(appFlags?.gaming_system_enable) && renderGaming()}
          <View style={{marginTop: responsiveHeight(20)}}>
            <View style={{marginTop: responsiveHeight(12)}}>
              <Text style={[styles.checkBoxTitle, {color: Colors().second}]}>
                No thanks, I don’t want exclusive offers
              </Text>
              <CheckBox
                size={responsiveHeight(35)}
                title={localize('signUp.receiveOffer')}
                value={optOutMarketing}
                onPress={() => setOptOutMarketing(!optOutMarketing)}
              />
            </View>
            <Text style={[styles.checkBoxTitle, {color: Colors().second}]}>Terms & Conditions</Text>
            <TermAndCondition
              onCheck={() => setAgreeTermCondition(!agreeTermAndCondition)}
              value={agreeTermAndCondition}
              allowCheck={true}
            />
          </View>
          <View style={styles.buttonContainer}>
            <ButtonView
              titleStyle={{color: Colors().signUp.textSignupButton}}
              style={{backgroundColor: Colors().signUp.backgroundSignupButton}}
              title={localize('signUp.continue')}
              onPress={signupHandler}
              disabled={!formState.formIsValid || !agreeTermAndCondition}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <DropDownList
        data={dropDownItems}
        visible={showSelectVenue}
        hasSearchBar={true}
        modalTitle={localize('signUp.preferredVenue')}
        height={responsiveHeight(350)}
        showValue={'label'}
        onClose={() => setShowSelectVenue(false)}
        value={formState?.inputValues?.venue_id?.value}
        placeholder={localize('signUp.preferredVenuePlaceholder')}
        onSelect={(item, isValid) => {
          inputChangeHandler('venue_id', item, isValid)
        }}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: responsiveHeight(15),
    marginBottom: responsiveHeight(70),
  },
  selectVenueInput: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans,
  },
  checkBoxTitle: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(16),
    marginBottom: responsiveHeight(5),
  },
})

export default SignupScreen
