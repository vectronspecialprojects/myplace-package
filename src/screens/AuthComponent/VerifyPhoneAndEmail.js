/**
 * Created by Hong HP on 9/17/21.
 */
import React, {useCallback, useEffect, useReducer, useState} from 'react'
import {View, StyleSheet, ScrollView, Text} from 'react-native'
import Input from '../../components/Input'
import {localize} from '../../locale/I18nConfig'
import Colors from '../../Themes/Colors'
import ButtonView from '../../components/ButtonView'
import SubHeaderBar from '../../components/SubHeaderBar'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {accountSearch, apiVerifyPhoneAndEmail, getMatchInstruction} from '../../utilities/ApiManage'
import RouteKey from '../../navigation/RouteKey'
import Toast from '../../components/Toast'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Styles from '../../Themes/Styles'
import Html from '../../components/Html'
import {onLinkPress} from '../../components/UtilityFunctions'
import TextInputView from '../../components/TextInputView'

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    }
  }
  return state
}

function VerifyPhoneAndEmail({navigation}) {
  const [instruction, setInstruction] = useState('')
  const dispatch = useDispatch()

  useEffect(() => {
    getMatchInstruction().then((res) => {
      if (res.ok) {
        setInstruction(res?.match_email_mobile)
      }
    })
  }, [])

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      mobile: '',
      email: '',
    },
    inputValidities: {
      mobile: false,
      email: false,
    },
    formIsValid: false,
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier,
      })
    },
    [dispatchFormState],
  )

  async function handleMatchAccount() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await apiVerifyPhoneAndEmail(formState.inputValues.mobile, formState.inputValues.email)
      console.log(res)
      if (res.ok) {
        navigation.navigate(RouteKey.MatchAccountInfo, {
          data: res.data?.AccountFull,
        })
      } else {
        navigation.navigate(RouteKey.MatchAccountScreen)
      }
    } catch (e) {
      Toast.info(e?.message)
      console.log(e?.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('verifyPhoneAndEmail.title')} alignLeft />
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          bounces={false}>
          <Html html={instruction} color={Colors().whatonDetails.description} onLinkPress={onLinkPress} />
          <TextInputView
            id="email"
            floatTitle={localize('verifyPhoneAndEmail.email')}
            placeholder={localize('verifyPhoneAndEmail.email')}
            keyboardType="email-address"
            required
            email
            emailAvailability
            autoCapitalize="none"
            errorText={`${localize('matchAccount.plsEnter')} ${localize('verifyPhoneAndEmail.email')}`}
            onChangeText={inputChangeHandler}
            value={formState.inputValues.email}
            initialValue=""
            placeholderTextColor={Colors().matchAccountScreen.placeholderTextColor}
            style={{
              backgroundColor: Colors().matchAccountScreen.fieldBackground,
              color: Colors().matchAccountScreen.fieldText,
            }}
          />
          <TextInputView
            id="mobile"
            floatTitle={localize('verifyPhoneAndEmail.mobile')}
            placeholder={localize('verifyPhoneAndEmail.mobile')}
            keyboardType="phone-pad"
            required
            minLength={10}
            maxLength={10}
            autoCapitalize="none"
            errorText={`${localize('matchAccount.plsEnter')} ${localize('verifyPhoneAndEmail.mobile')}`}
            onChangeText={inputChangeHandler}
            initialValue=""
            value={formState.inputValues.mobile}
            placeholderTextColor={Colors().matchAccountScreen.placeholderTextColor}
            style={{
              backgroundColor: Colors().matchAccountScreen.fieldBackground,
              color: Colors().matchAccountScreen.fieldText,
            }}
          />
          <View style={{flex: 1}} />
          <ButtonView
            title={localize('verifyPhoneAndEmail.connect')}
            disabled={!formState.formIsValid}
            style={[
              styles.buttonBorder,
              {
                backgroundColor: Colors().matchAccountScreen.backgroundFirstButton,
                borderColor: Colors().matchAccountScreen.boderColorFirstButton,
              },
            ]}
            titleStyle={{color: Colors().matchAccountScreen.textFirstButton}}
            onPress={() => handleMatchAccount()}
          />
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: responsiveWidth(24),
  },
  buttonBorder: {
    borderWidth: 2,
    marginVertical: responsiveHeight(10),
  },
  buttonContainer: {
    marginTop: responsiveHeight(15),
    marginBottom: responsiveHeight(10),
  },
})

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default VerifyPhoneAndEmail
