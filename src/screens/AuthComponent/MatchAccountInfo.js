import React, {useReducer, useMemo, useState, useCallback, useEffect} from 'react'
import {View, StyleSheet, ScrollView, KeyboardAvoidingView} from 'react-native'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import SubHeaderBar from '../../components/SubHeaderBar'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import ButtonView from '../../components/ButtonView'
import TermAndCondition from '../../components/TermAndCondition'
import {isIOS, responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import Toast from '../../components/Toast'
import {signupMatch} from '../../store/actions/authServices'
import {
  getStateFromPostCode,
  getStatusBarHeight,
  isEmptyValues,
  isTrue,
  parseStringToJson,
} from '../../utilities/utils'
import * as api from '../../utilities/ApiManage'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'
import Fonts from '../../Themes/Fonts'
import DropDownList from '../../components/DropDownList'
import ProfileFieldInput from '../../components/ProfileFieldInput'

const BY_VENUE = 'by_venue'
const BY_VENUE_TAG = 'by_venue_tag'
const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    }
  }
  return state
}

function MatchAccountInfo(props) {
  const venues = useSelector((state) => state.infoServices.venues)
  const dispatch = useDispatch()
  const {data} = props.route?.params || {}
  const [listTier, setListTier] = useState('')
  const [options, setOptions] = useState([])
  const emailNotAllowedDomains = useSelector((state) => state.app.emailNotAllowedDomains)
  const memberDefaultTierDetail = useSelector((state) => state.app.memberDefaultTierDetail)
  const memberDefaultTierOption = useSelector((state) => state.app.memberDefaultTierOption)
  const [showSelectVenue, setShowSelectVenue] = useState(false)
  const [agreeTermAndCondition, setAgreeTermCondition] = useState(false)

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      first_name: data?.FirstName || '',
      last_name: data?.LastName || '',
      email: data?.Email1st || '',
      password: '',
      password_confirmation: '',
    },
    inputValidities: {
      first_name: !!data?.FirstName,
      last_name: !!data?.LastName,
      email: !!data?.Email1st,
      password: false,
      password_confirmation: false,
    },
    formIsValid: false,
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        input: inputIdentifier,
        value: inputValue,
        isValid: inputValidity,
      })
    },
    [dispatchFormState],
  )

  const dropDownItems = useMemo(() => {
    const items = []
    for (const key in venues) {
      items.push({label: venues[key].name, value: venues[key].id})
    }
    return items
  }, [venues])

  useEffect(() => {
    getProfileSetting()
  }, [])

  useEffect(() => {
    if (memberDefaultTierOption === BY_VENUE && formState.inputValues?.venue?.value) {
      let tierId = +memberDefaultTierDetail?.[formState.inputValues?.venue?.value]?.tier
      if (tierId) inputChangeHandler('tier', tierId, true)
    }
  }, [formState.inputValues?.venue])

  useEffect(() => {
    if (formState.inputValues?.post_code?.length >= 3) {
      const state = getStateFromPostCode(+formState.inputValues?.post_code)
      inputChangeHandler('state', state, true)
      if (memberDefaultTierOption === BY_VENUE_TAG && state) {
        const tierId = +memberDefaultTierDetail?.[state]?.tier
        if (tierId) inputChangeHandler('tier', tierId, true)
      }
    }
  }, [formState.inputValues?.post_code])

  async function getProfileSetting() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const response = await api.getUserProfileSetting()
      let hasTier = false
      if (!response.ok) {
        throw new Error(response?.message)
      }
      let list = parseStringToJson(response?.bepozcustomfield?.extended_value)
      list.filter((item) => isTrue(item.displayInApp)).sort((a, b) => +a.displayOrder - +b.displayOrder)
      list?.map((e) => {
        if (e.required) {
          dispatchFormState({
            type: FORM_INPUT_UPDATE,
            input: e?.id,
            value: '',
            isValid: false,
          })
        }
        if (e.fieldType === 'Tier') {
          hasTier = true
        }
      })
      if (hasTier) setListTier(response.data)
      setOptions(list)
    } catch (e) {
      Alert.alert(localize('signUp.titleGetTier'), e.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  async function handleSubmitData() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      await dispatch(
        signupMatch({
          AccountID: data?.AccountID,
          AccNumber: data?.AccNumber,
          CardNumber: data?.CardNumber,
          share_info: true,
          venue_id: formState.inputValues.venue,
          first_name: formState.inputValues.first_name,
          last_name: formState.inputValues.last_name,
          email: formState.inputValues.email,
          password: formState.inputValues.password,
          password_confirmation: formState.inputValues.password_confirmation,
        }),
      )
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const dropDownTier = useMemo(() => {
    if (!listTier?.length) return []
    const list = []
    listTier?.map((item) => {
      list.push({
        label: item?.name,
        value: item?.id,
      })
    })
    return list
  }, [listTier])

  const customFields = (values) => {
    if (isEmptyValues(values)) return []
    let list = []
    Object.values(values).map((item) => {
      list.push({
        label: item?.fieldName || item,
        value: item?.id || item,
      })
    })
    return list
  }

  const renderSignUpFields = useCallback(() => {
    return options?.map((item, index) => {
      return (
        <ProfileFieldInput
          key={index.toString()}
          type={item.fieldType}
          item={item}
          customFields={customFields}
          setShowSelectVenue={setShowSelectVenue}
          dropDownTier={dropDownTier}
          emailNotAllowedDomains={emailNotAllowedDomains}
          formState={formState}
          inputChangeHandler={inputChangeHandler}
        />
      )
    })
  }, [options, formState.inputValues])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title="Match My Account" />
      <KeyboardAvoidingView
        style={{paddingHorizontal: responsiveWidth(20)}}
        behavior={isIOS() ? 'padding' : 'height'}
        keyboardVerticalOffset={getStatusBarHeight()}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          style={{paddingTop: responsiveHeight(15)}}>
          {renderSignUpFields()}
          <View style={styles.buttonContainer}>
            <ButtonView
              disabled={!formState.formIsValid}
              title={'Continue'}
              onPress={() => handleSubmitData()}
            />
            <ButtonView
              title={'Cancel'}
              style={[
                styles.buttonBorder,
                {
                  borderColor: Colors().second,
                  backgroundColor: Colors().defaultBackground,
                },
              ]}
              titleStyle={{color: Colors().second}}
              onPress={() => {
                props.navigation.pop()
              }}
            />
            <TermAndCondition
              onCheck={() => setAgreeTermCondition(!agreeTermAndCondition)}
              value={agreeTermAndCondition}
              allowCheck={true}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <DropDownList
        data={dropDownItems}
        visible={showSelectVenue}
        hasSearchBar={true}
        modalTitle={localize('signUp.preferredVenue')}
        height={responsiveHeight(350)}
        showValue={'label'}
        onClose={() => setShowSelectVenue(false)}
        value={formState?.inputValues?.venue?.value}
        placeholder={localize('signUp.preferredVenuePlaceholder')}
        onSelect={(item, isValid) => {
          inputChangeHandler('venue', item, isValid)
        }}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  buttonBorder: {
    borderWidth: 2,
    marginVertical: responsiveHeight(10),
  },
  buttonContainer: {
    marginTop: responsiveHeight(15),
    marginBottom: responsiveHeight(10),
  },
})

export default MatchAccountInfo
