import React, {useEffect, useState, useReducer, useCallback} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import RouteKey from '../../navigation/RouteKey'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import SubHeaderBar from '../../components/SubHeaderBar'
import ButtonView from '../../components/ButtonView'
import TermAndCondition from '../../components/TermAndCondition'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Toast from '../../components/Toast'
import {accountSearch, getMatchInstruction} from '../../utilities/ApiManage'
import {onLinkPress} from '../../components/UtilityFunctions'
import Html from '../../components/Html'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {localize} from '../../locale/I18nConfig'
import TextInputView from '../../components/TextInputView'

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    }
  }
  return state
}

function MatchAccountScreen({navigation}) {
  const [instruction, setInstruction] = useState('')
  const dispatch = useDispatch()

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      accountId: '',
      accountNumber: '',
    },
    inputValidities: {
      accountId: false,
      accountNumber: false,
    },
    formIsValid: false,
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier,
      })
    },
    [dispatchFormState],
  )

  useEffect(() => {
    getMatchInstruction().then((res) => {
      if (res.ok) {
        setInstruction(res?.match_bepoz_id_number)
      }
    })
  }, [])

  async function handleMatchAccount() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await accountSearch(formState.inputValues.accountId, formState.inputValues.accountNumber)
      if (!res.ok) throw new Error(res.message)
      navigation.navigate(RouteKey.MatchAccountInfo, {
        data: res.data?.AccountFull,
      })
    } catch (e) {
      Toast.info(e?.message)
      console.log(e?.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title="Match My Account" />
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          bounces={false}>
          <Html html={instruction} color={Colors().whatonDetails.description} onLinkPress={onLinkPress} />
          <TextInputView
            id="accountId"
            floatTitle={localize('matchAccount.accountId')}
            placeholder={localize('matchAccount.accountId')}
            keyboardType="default"
            required
            minLength={2}
            maxLength={15}
            autoCapitalize="none"
            errorText={`${localize('matchAccount.plsEnter')} ${localize('matchAccount.accountId')}`}
            onChangeText={inputChangeHandler}
            value={formState.inputValues.accountId}
            initialValue=""
            placeholderTextColor={Colors().matchAccountScreen.placeholderTextColor}
            style={{
              backgroundColor: Colors().matchAccountScreen.fieldBackground,
              color: Colors().matchAccountScreen.fieldText,
            }}
          />
          <TextInputView
            id="accountNumber"
            floatTitle={localize('matchAccount.accountNumber')}
            placeholder={localize('matchAccount.accountNumber')}
            keyboardType="default"
            required
            minLength={2}
            maxLength={15}
            autoCapitalize="none"
            errorText={`${localize('matchAccount.plsEnter')} ${localize('matchAccount.accountNumber')}`}
            onChangeText={inputChangeHandler}
            value={formState.inputValues.accountNumber}
            initialValue=""
            placeholderTextColor={Colors().matchAccountScreen.placeholderTextColor}
            style={{
              backgroundColor: Colors().matchAccountScreen.fieldBackground,
              color: Colors().matchAccountScreen.fieldText,
            }}
          />
          <View style={{marginTop: responsiveHeight(100)}} />
          <ButtonView
            title={localize('matchAccount.continue')}
            disabled={!formState.formIsValid}
            style={[
              styles.buttonBorder,
              {
                backgroundColor: Colors().matchAccountScreen.backgroundFirstButton,
                borderColor: Colors().matchAccountScreen.boderColorFirstButton,
              },
            ]}
            titleStyle={{color: Colors().matchAccountScreen.textFirstButton}}
            onPress={() => handleMatchAccount()}
          />
          <ButtonView
            title={localize('matchAccount.cancel')}
            style={[
              styles.buttonBorder,
              {
                backgroundColor: Colors().matchAccountScreen.backgroundFirstButton,
                borderColor: Colors().matchAccountScreen.boderColorFirstButton,
              },
            ]}
            titleStyle={{color: Colors().matchAccountScreen.textSecondButton}}
            onPress={() => {
              navigation.pop()
            }}
          />
          <TermAndCondition />
        </ScrollView>
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: responsiveWidth(24),
  },
  buttonBorder: {
    borderWidth: 2,
    marginBottom: responsiveHeight(8),
  },
})

export default MatchAccountScreen
