import React, {useReducer, useCallback} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import {useDispatch} from 'react-redux'
import * as infoServicesActions from '../../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Colors from '../../Themes/Colors'
import Styles from '../../Themes/Styles'
import SubHeaderBar from '../../components/SubHeaderBar'
import Input from '../../components/Input'
import ButtonView from '../../components/ButtonView'
import Toast from '../../components/Toast'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    }
  }
  return state
}

const ForgotPasswordScreen = (props) => {
  const dispatch = useDispatch()
  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      email: '',
    },
    inputValidities: {
      email: false,
    },
    formIsValid: false,
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier,
      })
    },
    [dispatchFormState],
  )

  const submitHandler = async () => {
    dispatch(setGlobalIndicatorVisibility(true))
    try {
      const rest = await dispatch(infoServicesActions.resetPassword(formState.inputValues.email))
      Toast.success(localize('forgotPassword.success'))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('forgotPassword.title')} />
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={{flex: 1, paddingHorizontal: responsiveWidth(20)}}>
          <View>
            <Input
              id="email"
              placeholder={localize('forgotPassword.email')}
              placeholderTextColor={Colors().forgotPassword.placeholderTextColor}
              label={localize('forgotPassword.email')}
              keyboardType="email-address"
              required
              email
              autoCapitalize="none"
              errorText={localize('forgotPassword.plsEnter')}
              onInputChange={inputChangeHandler}
              initialValue=""
            />
          </View>

          <View style={styles.buttonContainer}>
            <ButtonView
              title={'Submit'}
              disabled={!formState.formIsValid}
              style={[
                styles.buttonBorder,
                {
                  borderColor: Colors().forgotPassword.buttonBorder,
                  backgroundColor: Colors().forgotPassword.buttonBackground,
                },
              ]}
              titleStyle={{color: Colors().forgotPassword.buttonText}}
              onPress={submitHandler}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    marginVertical: responsiveHeight(50),
    paddingVertical: responsiveHeight(50),
  },
  buttonBorder: {
    borderWidth: 2,
    marginBottom: responsiveHeight(8),
  },
})

export default ForgotPasswordScreen
