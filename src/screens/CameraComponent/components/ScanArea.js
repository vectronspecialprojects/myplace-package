/**
 * Created by Hong HP on 1/20/19.
 */
import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import {responsiveFont, responsiveWidth, responsiveHeight} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'

export default class ScanArea extends React.Component {
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{backgroundColor: 'rgba(0,0,0,0.6)', flex: 1, marginBottom: responsiveWidth(100)}} />
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            position: 'absolute',
            zIndex: 9999,
            height: responsiveWidth(200),
          }}>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.6)'}} />
          <View style={{width: responsiveWidth(200), height: responsiveWidth(200), zIndex: 999}}>
            <View style={[styles.borderTopLeft, {borderColor: Colors().white}]} />
            <View style={[styles.borderTopRight, {borderColor: Colors().white}]} />
            <View style={[styles.borderBottomLeft, {borderColor: Colors().white}]} />
            <View style={[styles.borderBottomRight, {borderColor: Colors().white}]} />
          </View>
          <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.6)'}} />
        </View>
        <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.6)', marginTop: responsiveWidth(100)}} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  borderTopLeft: {
    width: 20,
    height: 20,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    position: 'absolute',
    left: -10,
    top: -10,
  },
  borderTopRight: {
    width: 20,
    height: 20,
    borderTopWidth: 1,
    borderRightWidth: 1,
    position: 'absolute',
    right: -10,
    top: -10,
    zIndex: 999,
  },
  borderBottomLeft: {
    width: 20,
    height: 20,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    position: 'absolute',
    left: -10,
    bottom: -10,
    zIndex: 999,
  },
  borderBottomRight: {
    width: 20,
    height: 20,
    borderBottomWidth: 1,
    borderRightWidth: 1,
    position: 'absolute',
    right: -10,
    bottom: -10,
    zIndex: 999,
  },
})
