import React, {useCallback, useEffect, useMemo, useState} from 'react'
import {View, StyleSheet, FlatList, Text} from 'react-native'
import Styles from '../../Themes/Styles'
import SubHeaderBar from '../../components/SubHeaderBar'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {apiGetSurveyDetails, submitSurvey} from '../../utilities/ApiManage'
import MultipleChoiceItem from './components/MultipleChoiceItem'
import ButtonView from '../../components/ButtonView'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import DropdownItem from './components/DropdownItem'
import Toast from '../../components/Toast'
import Colors from '../../Themes/Colors'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {localize} from '../../locale/I18nConfig'

function SurveyDetailsScreen({navigation, route}) {
  const {surveyId, surveyName, refreshList} = route?.params || {}
  const dispatch = useDispatch()
  const [surveyDetail, setSurveyDetail] = useState({})
  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    getData()
  }, [])

  const getData = useCallback(async () => {
    try {
      const res = await apiGetSurveyDetails(surveyId)
      if (!res.ok) throw new Error(res?.message)
      setSurveyDetail(res.data)
    } catch (e) {
      Toast.info(e?.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [surveyId])

  async function handleSubmitSurvey() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await submitSurvey(JSON.stringify(surveyDetail))
      if (!res.ok) throw new Error(res.message)
      Toast.success(localize('survey.messageSuccess'))
      refreshList?.()
      navigation.pop()
    } catch (e) {
      Toast.info(e?.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  function handleSelectAnswer(answerId, index) {
    let listQuestion = [...surveyDetail.questions]
    listQuestion[index] = {...listQuestion[index], answer_id: answerId}
    setSurveyDetail({
      ...surveyDetail,
      questions: listQuestion,
    })
  }

  function renderItem({item, index}) {
    let Comp = item.multiple_choice ? MultipleChoiceItem : DropdownItem
    return <Comp data={item} onSelect={(id) => handleSelectAnswer(id, index)} />
  }

  const allowSubmit = useMemo(() => {
    let allowed = true
    surveyDetail?.questions?.some((item) => {
      if (!item.answer_id) {
        allowed = false
        return false
      }
    })
    return allowed
  }, [surveyDetail])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={surveyName} />
      <FlatList
        data={surveyDetail?.questions?.sort((a, b) => a.pivot?.question_order - b.pivot?.question_order)}
        ListHeaderComponent={
          !!surveyDetail?.description && (
            <Text style={[styles.description, {color: Colors().survey.description}]}>
              {surveyDetail.description}
            </Text>
          )
        }
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
      <ButtonView
        title={localize('survey.submit')}
        style={styles.buttonView}
        disabled={!allowSubmit}
        onPress={() => {
          handleSubmitSurvey()
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  buttonView: {
    marginBottom: responsiveHeight(30),
    marginHorizontal: responsiveWidth(24),
    marginTop: responsiveHeight(10),
  },
  description: {
    textAlign: 'center',
    marginTop: responsiveHeight(36),
    marginBottom: responsiveHeight(10),
    marginHorizontal: responsiveWidth(22),
  },
})

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default SurveyDetailsScreen
