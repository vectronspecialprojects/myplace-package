import React, {useEffect, useState, useCallback} from 'react'
import {View, FlatList, RefreshControl, Text} from 'react-native'
import SubHeaderBar from '../../components/SubHeaderBar'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import * as infoServicesActions from '../../store/actions/infoServices'
import SurveyItem from './components/SurveyItem'
import RouteKey from '../../navigation/RouteKey'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig';

const SurveysScreen = (props) => {
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const listSurvey = useSelector((state) => state.infoServices.surveys)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchSurveys())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])


  function renderItem({item, index}) {
    return (
      <SurveyItem
        data={item}
        onPress={() => {
          props.navigation.navigate(RouteKey.SurveyDetailsScreen, {
            surveyId: item.id,
            surveyName: item.title,
            refreshList: () => loadContent(),
          })
        }}
      />
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      <FlatList
        data={listSurvey}
        renderItem={renderItem}
        ListEmptyComponent={
          <Text style={Styles.flatlistNoItems}>No survey found, please check again later.</Text>
        }
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
            tintColor={Colors().defaultRefreshSpinner}
            titleColor={Colors().defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}


export default SurveysScreen
