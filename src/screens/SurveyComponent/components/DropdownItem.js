import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Colors from '../../../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import {TouchableCmp} from '../../../components/UtilityFunctions'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import DropDownList from '../../../components/DropDownList'

function DropdownItem({data, onSelect}) {
  const [showList, setShowList] = useState(false)
  const [answer, setAnswer] = useState('')
  return (
    <View style={[styles.container, {backgroundColor: Colors().survey.background}]}>
      <View style={[styles.titleWrapper, {borderColor: Colors().survey.answer}]}>
        <Text style={[styles.title, {color: Colors().survey.questionTitle}]}>{data?.question}</Text>
      </View>
      <TouchableCmp onPress={() => setShowList(true)}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={[styles.answer, {color: Colors().survey.answer}]}>
            {answer ? answer.answer : 'Please select one answer'}
          </Text>
          <MaterialIcons name="keyboard-arrow-down" size={24} color={Colors().survey.answer} />
        </View>
      </TouchableCmp>
      <DropDownList
        data={data.answers}
        visible={showList}
        modalTitle={'Please select one answer'}
        height={responsiveHeight(300)}
        showValue={'answer'}
        onClose={() => setShowList(false)}
        value={answer.answer}
        onSelect={(item) => {
          onSelect(item.id)
          setAnswer(item)
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: responsiveWidth(10),
    marginTop: responsiveHeight(20),
    borderRadius: responsiveHeight(10),
    paddingHorizontal: responsiveWidth(16),
  },
  title: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSansBold,
  },
  titleWrapper: {
    borderBottomWidth: 0.5,
    paddingVertical: responsiveHeight(16),
  },
  answer: {
    fontSize: responsiveFont(13),
    fontFamily: Fonts.openSans,
    marginVertical: responsiveHeight(10),
    flex: 1,
  },
})

export default DropdownItem
