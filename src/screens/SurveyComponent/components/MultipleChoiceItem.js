import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import Colors from '../../../Themes/Colors'
import RadioButton from '../../../components/RadioButton'

function MultipleChoiceItem({onSelect, data}) {
  const [answerId, setAnswerId] = useState(data.answer_id)
  return (
    <View style={[styles.container, {backgroundColor: Colors().survey.background}]}>
      <View style={[styles.titleWrapper, {borderColor: Colors().survey.background}]}>
        <Text style={[styles.title, {color: Colors().survey.questionTitle}]}>{data?.question}</Text>
      </View>
      {data?.answers?.map((item, index) => {
        return (
          <RadioButton
            style={{paddingVertical: responsiveHeight(5)}}
            titleStyle={{color: Colors().survey.answer}}
            tintColor={Colors().survey.answer}
            radioButtonColor={Colors().survey.answer}
            title={item.answer}
            onPress={() => {
              setAnswerId(item.id)
              onSelect(item.id)
            }}
            value={answerId === item.id}
          />
        )
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: responsiveWidth(10),
    marginTop: responsiveHeight(20),
    borderRadius: responsiveHeight(10),
    padding: responsiveWidth(16),
  },
  title: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSansBold,
  },
  titleWrapper: {
    borderBottomWidth: 0.5,
    paddingBottom: responsiveHeight(16),
  },
})

export default MultipleChoiceItem
