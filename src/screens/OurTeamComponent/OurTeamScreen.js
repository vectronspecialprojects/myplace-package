import React, {useCallback, useEffect, useState, useMemo} from 'react'
import {View, Text, Alert, RefreshControl, FlatList} from 'react-native'
import SubHeaderBar from '../../components/SubHeaderBar'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import * as infoServicesActions from '../../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {useDispatch, useSelector} from 'react-redux'
import { responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import OurTeamItem from './components/OurTeamItem'
import OurTeamPopup from './components/OurTeamPopup'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {localize} from '../../locale/I18nConfig'

const OurTeamScreen = (props) => {
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const ourTeam = useSelector((state) => state.infoServices.ourTeam)
  const [itemSelected, setItemSelect] = useState('')

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [])

  const showData = useMemo(() => {
    if (!ourTeam?.length) return []
    return ourTeam?.sort((a, b) => {
      return a.listing.display_order - b.listing.display_order
    })
  }, [ourTeam])

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetctListings(6))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message || err, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [])

  function renderItem({item, index}) {
    return <OurTeamItem data={item.listing} onPress={() => setItemSelect(item)} />
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      <FlatList
        data={showData}
        style={{marginHorizontal: responsiveWidth(25), marginBottom: responsiveHeight(10)}}
        renderItem={renderItem}
        numColumns={2}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={
          <Text style={Styles.flatlistNoItems}>No team members found, please check again later.</Text>
        }
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
            tintColor={Colors().defaultRefreshSpinner}
            titleColor={Colors().defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }
      />
      <OurTeamPopup
        item={itemSelected?.listing}
        isVisible={!!itemSelected}
        onPress={() => {
          setItemSelect('')
        }}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}


export default OurTeamScreen
