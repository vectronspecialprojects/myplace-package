import React, {useEffect, useRef, useState} from 'react'
import {View, StyleSheet, Image, ImageBackground, ActivityIndicator, Dimensions} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import {useDispatch, useSelector} from 'react-redux'

import Colors from '../Themes/Colors'
import * as authServicesActions from '../store/actions/authServices'
import * as infoServicesActions from '../store/actions/infoServices'
import * as appServicesActions from '../store/actions/appServices'
import {setAccessToken} from '../utilities/NetworkingAuth'
import {setAppState} from '../store/actions/appServices'
import {fetchAllData} from '../store/actions/infoServices'
import {fetchProfile} from '../store/actions/infoServices'
import CodePush from 'react-native-code-push'

const {width, height} = Dimensions.get('window')
import * as Progress from 'react-native-progress'
import {responsiveHeight} from '../Themes/Metrics'
import Images from '../Themes/Images'
import vars, {codePushKey} from '../constants/env'
import dayjs, {Dayjs} from 'dayjs'
import {backgroundImage, logoImage} from '../constants/constants'

const codePushOptions = {
  installMode: CodePush.InstallMode.IMMEDIATE,
  deploymentKey: codePushKey[vars.buildEvn],
}

const StartupScreen = (props) => {
  const dispatch = useDispatch()
  const internetState = useSelector((state) => state.app.internetState)

  const [receivedBytes, setReceivedBytes] = useState(0)
  const total = useRef(0)

  useEffect(() => {
    CodePush.sync(
      codePushOptions,
      (status) => {
        switch (status) {
          case CodePush.SyncStatus.UP_TO_DATE:
          case CodePush.SyncStatus.UNKNOWN_ERROR:
            tryLogin()
            break
        }
      },
      ({receivedBytes, totalBytes}) => {
        total.current = totalBytes
        setReceivedBytes(receivedBytes)
      },
    )
  }, [])

  const tryLogin = async () => {
    try {
      try {
        if (internetState) {
          await dispatch(appServicesActions.getAppSettings())
          await dispatch(infoServicesActions.fetchVenues())
          await dispatch(infoServicesActions.fetchLegals())
        }
      } catch (e) {
        console.log(e)
        return
      }
      const userData = await AsyncStorage.getItem('CURRENT_USER')
      //User is not login
      if (!userData) {
        dispatch(setAppState('Auth'))
        return
      }
      const user_credentials = JSON.parse(userData)
      console.log('user_credentials', user_credentials)
      setAccessToken(user_credentials.token)
      if (!user_credentials.token) {
        dispatch(setAppState('Auth'))
        return
      }
      //call get profile
      if (internetState) {
        await dispatch(fetchProfile())
      }
      dispatch(authServicesActions.authenticate(user_credentials))
      dispatch(setAppState('Main'))
      if (internetState) {
        dispatch(fetchAllData())
      }
    } catch (err) {
      dispatch(setAppState('Auth'))
    }
  }

  return (
    <View style={styles.screen}>
      <ImageBackground source={{uri: backgroundImage}} style={styles.backgroundImage}>
        <View style={styles.logoContainer}>
          <Image source={{uri: logoImage}} style={styles.logo} />
          <ActivityIndicator size="large" color={Colors().defaultRefreshSpinner} />
        </View>
        {!!total.current && (
          <View style={styles.progressBar}>
            <Progress.Bar
              progress={receivedBytes / total.current}
              color={Colors().white}
              showsText
              width={width * 0.6}
            />
          </View>
        )}
      </ImageBackground>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerTransparent: true,
    headerTitle: () => null,
    headerBackground: () => null,
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors().defaultBackground,
  },
  backgroundImage: {
    flex: 1,
    alignSelf: 'center',
    width: '100%',
  },
  logoContainer: {
    width: '100%',
    height: '70%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: '80%',
    resizeMode: 'contain',
    height: responsiveHeight(150),
  },
  progressBar: {
    position: 'absolute',
    bottom: responsiveHeight(40),
    alignSelf: 'center',
  },
})

export default StartupScreen
