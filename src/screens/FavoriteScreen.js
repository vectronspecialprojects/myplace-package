import React, {useCallback, useEffect, useState} from 'react'
import {View, StyleSheet, FlatList, RefreshControl, Text, ScrollView} from 'react-native'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import FavoriteItem from './../components/FavoriteItem'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import RouteKey from '../navigation/RouteKey'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const FavoriteScreen = ({navigation}) => {
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()
  const favorites = useSelector((state) => state.infoServices.favorites)

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.getFavorite())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  function renderItem({item, index}) {
    return (
      <FavoriteItem
        data={item?.listing}
        onPress={() => {
          navigation.navigate(RouteKey.FavoriteDetailScreen, {
            favoriteDetail: item?.listing,
            title: item?.listing?.type?.name,
          })
        }}
      />
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('favorite.title')} />
      <View style={styles.sessionContainer}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                setIsRefreshing(true)
                loadContent()
              }}
              tintColor={Colors().defaultRefreshSpinner}
              titleColor={Colors().defaultRefreshSpinner}
              title={localize('favorite.title')}
            />
          }>
          {favorites?.map((item, index) => {
            return (
              <View key={index.toString()}>
                <Text style={[styles.title, {color: Colors().favorite.title}]}>{item.title}</Text>
                <FlatList
                  data={item.data}
                  showsHorizontalScrollIndicator={false}
                  renderItem={renderItem}
                  horizontal={true}
                  keyExtractor={(item, index) => index.toString()}
                />
              </View>
            )
          })}
        </ScrollView>
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: responsiveFont(15),
    marginBottom: responsiveHeight(18),
    fontFamily: Fonts.openSansBold,
  },
  sessionContainer: {
    marginLeft: responsiveWidth(17),
    marginTop: responsiveHeight(20),
  },
})

export default FavoriteScreen
