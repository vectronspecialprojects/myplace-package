import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {View, StyleSheet} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import * as infoServicesActions from '../../store/actions/infoServices'
import Colors from '../../Themes/Colors'
import Styles from '../../Themes/Styles'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Alert from '../../components/Alert'
import {isEmptyValues, isTrue} from '../../utilities/utils'
import {apiIsBepozAccountCreated} from '../../utilities/ApiManage'
import {localize} from '../../locale/I18nConfig'
import HomeVersion1 from './components/HomeVersion1'
import HomeVersion2 from './components/HomeVersion2'
import HeaderBackground from './components/HeaderBackground'
import HamburgerButton from '../../navigation/drawerComponents/HamburgerButton'

const SpecialPage = ['points', 'redeems', 'balance', 'saved']
const HomeScreen = (props) => {
  const galleries = useSelector((state) => state.app.galleries)
  const communityImpact = useSelector((state) => state.app.communityImpact)
  const profile = useSelector((state) => state.infoServices.profile)
  const notifications = useSelector((state) => state.infoServices.notifications)
  const homeMenus = useSelector((state) => state.app.homeMenus)
  const appFlags = useSelector((state) => state.app.appFlags)
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const internetState = useSelector((state) => state.app.internetState)
  const useLegacyDesign = useSelector((state) => state.app.useLegacyDesign)

  const ifRoute = (id) => {
    const homeMenu = homeMenus.find((menu) => menu.id === id)
    if (
      !homeMenu?.state ||
      homeMenu?.state === 'none' ||
      (homeMenu?.page_layout === 'special_view' && SpecialPage.includes(homeMenu?.special_page))
    ) {
      return null
    } else {
      props.navigation.navigate('homeMenu' + id)
    }
  }

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchProfile())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    try {
      await dispatch(infoServicesActions.getNotification())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      setGlobalIndicatorVisibility(false)
    }
  }, [])

  const checkBepozAccount = useCallback(async () => {
    const response = await apiIsBepozAccountCreated(profile?.email)
    if (!response.ok && profile.email_confirmation) {
      Alert.alert(localize('canNotMatch'), response.message, [{text: localize('okay')}])
    }
  }, [profile])

  useEffect(() => {
    if (!isEmptyValues(profile) && !profile?.member?.bepoz_account_card_number) {
      if (isTrue(appFlags?.gaming_system_enable)) checkBepozAccount()
      if (!profile?.email_confirmation) {
        Alert.alert(localize('home.titleConfirmEmail'), localize('home.messageConfirmEmail'), [
          {text: localize('okay')},
        ])
      }
    }
  }, [profile])

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      if (internetState) {
        setGlobalIndicatorVisibility(true)
        loadContent()
      }
    })
    return () => {
      unsubscribe()
    }
  }, [])

  const pledge = useMemo(() => {
    try {
      return JSON.parse(profile?.member?.pledge?.replace('﻿\n', ''))
    } catch (err) {
      return {}
    }
  }, [profile])

  const Layout = useLegacyDesign ? HomeVersion1 : HomeVersion2
  return (
    <View style={[Styles.screen, {backgroundColor: Colors().home.barcodeAreaBackground}]}>
      <Layout
        profile={profile}
        isRefreshing={isRefreshing}
        setIsRefreshing={setIsRefreshing}
        loadContent={loadContent}
        galleries={galleries}
        communityImpact={communityImpact}
        homeMenus={homeMenus}
        ifRoute={ifRoute}
        internetState={internetState}
        notifications={notifications}
        pledge={pledge}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerTitle: () => null,
    headerLeft: () => <HamburgerButton navData={navData} position={'left'} />,
    headerRight: () => <HamburgerButton navData={navData} position={'right'} />,
    headerBackground: HeaderBackground,
  }
}

export default HomeScreen
