import React, {useEffect, useMemo, useState} from 'react'
import {View, StyleSheet, ActivityIndicator} from 'react-native'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import SubHeaderBar from '../../components/SubHeaderBar'
import Styles from '../../Themes/Styles'
import {useSelector} from 'react-redux'
import Colors from '../../Themes/Colors'
import {WebView} from 'react-native-webview'

function PondHoppersScreen({route}) {
  const {action, title, params} = route?.params
  const profile = useSelector((state) => state.infoServices.profile)
  const pondHopper = useSelector((state) => state.app.pondHopper)

  const pondHopperAction = useMemo(() => {
    if (action) return action
    if (params?.special_page === 'community-points') return 'PLEDGE'
    if (params?.special_page) return params?.special_page?.toUpperCase()
    return 'PLEDGE'
  }, [])

  return (
    <View style={Styles.screen}>
      {/*<SubHeaderBar title={title || 'Community Points'} />*/}
      <WebView
        startInLoadingState={true}
        source={{
          uri: pondHopper?.url,
          method: 'POST',
          headers: {'Content-Type': 'text/plain'},
          body: JSON.stringify({
            memberNumber: profile?.member?.bepoz_account_number,
            uniqueKey: pondHopper?.uniqueKey,
            venueId: profile?.member?.current_preferred_venue_full?.bepoz_venue_id, //member.current_preferred_venue_full.bepoz_venue_id
            action: pondHopperAction,
          }),
        }}
        renderLoading={() => (
          <View style={[styles.loadingContainer, {backgroundColor: Colors().defaultBackground}]}>
            <ActivityIndicator size="large" />
          </View>
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loadingContainer: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    right: '50%',
  },
})

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default PondHoppersScreen
