import React from 'react'
import {View, StyleSheet, ImageBackground, Text, Modal} from 'react-native'
import BarcodeBar from '../../components/BarcodeBar'
import {deviceWidth, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import {localize} from '../../locale/I18nConfig'
import Colors from '../../Themes/Colors'
import Images from '../../Themes/Images'
import Styles from '../../Themes/Styles'
import {useDispatch, useSelector} from 'react-redux'
import VectorIconButton from '../../components/VectorIconButton'
import {setShowBarCode} from '../../store/actions/appServices'
import Ionicons from 'react-native-vector-icons/Ionicons'

function BarCodeScreen() {
  const profile = useSelector((state) => state.infoServices.profile)
  const showBarCode = useSelector((state) => state.app.showBarCode)
  const dispatch = useDispatch()

  function handleClose() {
    dispatch(setShowBarCode(false))
  }

  return (
    <Modal animationType={'slide'} animated onRequestClose={() => {}} transparent visible={showBarCode}>
      <View style={styles.container}>
        <View style={[styles.barCodeContainer, {backgroundColor: Colors().white}]}>
          {!!profile?.member?.bepoz_account_card_number ? (
            <BarcodeBar
              value={profile?.member?.bepoz_account_card_number}
              width={responsiveWidth(profile?.member?.bepoz_account_card_number.length <= 10 ? 1.7 : 1.2)}
              title={localize('memberNumber')}
              text={profile?.member?.bepoz_account_number}
            />
          ) : (
            <View style={[styles.fakeBarCodeContainer, {borderTopColor: Colors().home.barcodeTopBorder}]}>
              <ImageBackground style={styles.barcode} source={Images.barcode}>
                <View style={styles.textWrapper}>
                  <Text style={[Styles.xSmallNormalText, {color: Colors().home.barcodeGeneratingText}]}>
                    Your membership barcode is being generated.
                  </Text>
                </View>
              </ImageBackground>
            </View>
          )}
        </View>
      </View>
      <VectorIconButton
        name={'close'}
        Component={Ionicons}
        color={Colors().second}
        size={responsiveWidth(35)}
        style={[styles.buttonClose, {backgroundColor: Colors().white}]}
        onPress={() => {
          handleClose()
        }}
      />
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fakeBarCodeContainer: {
    borderTopWidth: responsiveHeight(1),
    paddingTop: responsiveHeight(30),
  },
  barcode: {
    width: responsiveWidth(200),
    height: responsiveHeight(50),
    alignSelf: 'center',
  },
  textWrapper: {
    flex: 1,
    paddingHorizontal: responsiveWidth(2),
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  barCodeContainer: {
    borderRadius: responsiveWidth(10),
    width: responsiveWidth(300),
    height: responsiveHeight(130),
    justifyContent: 'center',
  },
  buttonClose: {
    position: 'absolute',
    bottom: responsiveHeight(150),
    width: responsiveHeight(42),
    height: responsiveHeight(42),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: responsiveHeight(21),
    left: (deviceWidth() - responsiveHeight(42)) / 2,
  },
})
export default BarCodeScreen
