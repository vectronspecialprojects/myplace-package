import React from 'react'
import {View, StyleSheet, Image, Text} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Images from '../../../Themes/Images'
import Colors from '../../../Themes/Colors'
import Fonts from '../../../Themes/Fonts'
import {useSelector} from 'react-redux'
import {logoImage} from "../../../constants/constants";

function HeaderBackground() {
  const profile = useSelector((state) => state.infoServices.profile)
  const useLegacyDesign = useSelector((state) => state.app.useLegacyDesign)
  return (
    <View
      style={[
        styles.logoContainer,
        !useLegacyDesign && {alignItems: null, backgroundColor: Colors().headerBackground},
      ]}>
      {!useLegacyDesign ? (
        <Text style={[styles.title, {color: Colors().white}]}>Welcome back, {profile?.member?.first_name}</Text>
      ) : (
        <Image style={styles.logo} source={{uri: logoImage}} />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  logoContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: responsiveWidth(20),
  },
  logo: {
    width: responsiveWidth(100),
    height: responsiveHeight(50),
    resizeMode: 'contain',
  },
  title: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(20),
    marginBottom: responsiveHeight(10),
    marginLeft: responsiveWidth(15),
  },
})
export default HeaderBackground
