/**
 * Created by HongHP on 19/01/2022
 */
import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import Fonts from '../../../Themes/Fonts'
import CustomIcon from '../../../components/CustomIcon'

function LabelComponent({data}) {
  return (
    <View style={styles.container}>
      {(data.icon_selector === 'image' && !!data?.image_icon) ||
        (data?.icon_selector === 'icon' && !!data?.icon && (
          <CustomIcon
            image_icon={data?.image_icon}
            name={data.icon}
            icon_selector={data?.icon_selector}
            size={responsiveFont(16)}
            color={data?.icon_color}
            tintColor={data?.icon_color}
          />
        ))}
      <Text style={[styles.title, {color: Colors().home.menuText}, {color: data?.icon_color}]}>
        {data?.page_name}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: responsiveWidth(15),
    flexDirection: 'row',
  },
  title: {
    fontSize: responsiveFont(18),
    flex: 1,
    fontFamily: Fonts.openSansBold,
    marginBottom: responsiveHeight(8),
  },
})
export default LabelComponent
