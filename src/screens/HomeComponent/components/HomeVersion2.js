import React from 'react'
import {View, StyleSheet, ScrollView, RefreshControl, Text, ImageBackground} from 'react-native'
import Colors from '../../../Themes/Colors'
import {localize} from '../../../locale/I18nConfig'
import {responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import {isTrue} from '../../../utilities/utils'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import MenuItem from './MenuItem'
import ButtonView from '../../../components/ButtonView'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {getNumberData} from './contanst'
import CommunityDataComponent from './CommunityDataComponent'
import WhatOnHorizontalComponent from './WhatOnHorizontalComponent'
import {useSelector} from 'react-redux'
import LabelComponent from './LabelComponent'

function HomeVersion2({
  isRefreshing,
  setIsRefreshing,
  loadContent,
  communityImpact,
  pledge,
  profile,
  internetState,
  homeMenus,
  ifRoute,
}) {
  const pondHoppersEnable = useSelector((state) => state.app.pondHoppersEnable)
  return (
    <View style={styles.container}>
      <View style={{backgroundColor: Colors().headerBackground}}>
        <ButtonView
          onPress={() => {
            navigate(RouteKey.PreferredVenueScreen)
          }}
          title={profile?.member?.current_preferred_venue_name || 'Venue name'}
          style={[styles.venueButton, {backgroundColor: Colors().home.menuBackground}]}
          titleStyle={{color: Colors().home.name, textAlign: null}}
          rightIcon={
            <MaterialCommunityIcons
              name={'chevron-down'}
              size={22}
              style={{marginRight: responsiveWidth(20)}}
            />
          }
        />
      </View>
      <View style={{flex: 1}}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => {
                if (internetState) {
                  setIsRefreshing(true)
                  loadContent()
                }
              }}
              tintColor={Colors().defaultRefreshSpinner}
              titleColor={Colors().defaultRefreshSpinner}
              title={localize('pullToRefresh')}
            />
          }>
          {homeMenus?.map((item, index) => {
            switch (item.special_page) {
              case 'community-points':
                return (
                  <CommunityDataComponent
                    key={index.toString()}
                    communityImpact={communityImpact}
                    pledge={pledge}
                    item={item}
                    ifRoute={ifRoute}
                    profile={profile}
                  />
                )
              case 'points':
                return (
                  <MenuItem
                    key={index.toString()}
                    title={'Your membership'}
                    subTitle={isTrue(pondHoppersEnable) && 'Explore perks'}
                    actionSubTitle={'PERKS'} //Just hard code
                    style={{marginTop: responsiveHeight(15)}}
                    color={item.icon_color}
                    dataTitle={item.page_name}
                    value={getNumberData(item, profile)}
                    iconName={item.icon}
                    iconImage={item.image_icon}
                    valueRight={false}
                    disabled={item.state === 'none'}
                    onPress={() => ifRoute(item.id)}
                    iconSelector={item.icon_selector}
                  />
                )
              case 'what_on':
                return <WhatOnHorizontalComponent key={index.toString()} />
              case 'label':
                return <LabelComponent key={index.toString()} data={item} />
              default:
                return (
                  <MenuItem
                    key={index.toString()}
                    dataTitle={item.page_name}
                    value={getNumberData(item, profile)}
                    iconName={item.icon}
                    iconImage={item.image_icon}
                    onPress={() => ifRoute(item.id)}
                    color={item.icon_color}
                    disabled={item.state === 'none'}
                    iconSelector={item.icon_selector}
                  />
                )
            }
          })}
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  venueButton: {
    height: responsiveHeight(38),
    marginHorizontal: responsiveWidth(20),
    borderRadius: responsiveHeight(19),
    marginBottom: responsiveHeight(10),
  },
})
export default HomeVersion2
