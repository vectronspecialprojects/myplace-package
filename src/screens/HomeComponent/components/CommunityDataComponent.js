import React, {useEffect, useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Swiper from 'react-native-swiper'
import {deviceWidth, responsiveFont, responsiveHeight, responsiveWidth, shadow} from '../../../Themes/Metrics'
import FastImage from 'react-native-fast-image'
import Fonts from '../../../Themes/Fonts'
import ButtonView from '../../../components/ButtonView'
import Colors from '../../../Themes/Colors'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import MenuItem from './MenuItem'
import {getCommunityPoint} from '../../../utilities/ApiManage'
import {useSelector} from 'react-redux'

function CommunityDataComponent({profile, communityImpact, pledge, item, ifRoute}) {
  const pondHopper = useSelector((state) => state.app.pondHopper)
  const [pointData, sePointData] = useState()

  useEffect(() => {
    getCommunityPoint(
      pondHopper.url,
      pondHopper.uniqueKey,
      profile?.member?.bepoz_account_number,
      profile?.member?.current_preferred_venue_full?.bepoz_venue_id,
    ).then((res) => {
      sePointData(res)
    })
  }, [])

  return (
    <View style={{marginBottom: responsiveHeight(15), marginHorizontal: responsiveWidth(15)}}>
      <MenuItem
        title={'Community impact'}
        subTitle={'Pledge & nominate'}
        actionSubTitle={'NOMINATE'}
        style={{marginTop: responsiveHeight(15), marginHorizontal: 0}}
        color={item.icon_color}
        dataTitle={item.page_name}
        value={`${pointData?.pointsAvailable || 0} Community Points ready to pledge`}
        iconName={item.icon}
        iconImage={item.image_icon}
        valueRight={false}
        onPress={() => ifRoute(item.id)}
        iconSelector={item.icon_selector}
      />
      <View style={[styles.carouselContainer, {backgroundColor: Colors().white}]}>
        <Swiper loop={true} style={{height: responsiveHeight(130)}} autoplay={true} showsPagination={false}>
          {communityImpact?.community_impact_gallery?.map((item, index) => {
            return (
              <FastImage
                key={index.toString()}
                source={{uri: item.url}}
                style={{width: '100%', height: responsiveHeight(130)}}
              />
            )
          })}
        </Swiper>
        <View style={styles.totalView}>
          <Text
            style={[
              styles.totalText,
              {
                fontFamily: Fonts.openSansBold,
                fontSize: responsiveFont(18),
                color: Colors().second,
              },
            ]}>
            {pointData?.totalPledgedToDate || '$0.00'}
          </Text>
          <Text style={[styles.totalText, {color: Colors().defaultTextColor}]}>
            {communityImpact?.community_impact_content_text}
          </Text>
          <ButtonView
            title={'Read Now'}
            style={[styles.buttonReadNow, {borderColor: Colors().red}]}
            titleStyle={{color: Colors().red}}
            onPress={() => {
              navigate(RouteKey.PondHoppersScreen, {
                action: communityImpact?.community_impact_button_option.toUpperCase(),
                title: communityImpact?.community_impact_button_option.toLowerCase(),
              })
            }}
          />
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  carouselContainer: {
    ...shadow,
  },
  buttonReadNow: {
    width: responsiveWidth(120),
    height: responsiveHeight(40),
    backgroundColor: 'transparent',
    borderWidth: 1,
    marginTop: responsiveHeight(15),
  },
  totalView: {
    paddingVertical: responsiveHeight(15),
    paddingHorizontal: responsiveWidth(10),
  },
  totalText: {
    fontSize: responsiveFont(12),
    fontFamily: Fonts.openSans,
  },
})
export default CommunityDataComponent
