import React from 'react'
import {View, StyleSheet, FlatList, TouchableOpacity, Text} from 'react-native'
import FastImage from 'react-native-fast-image'
import Images from '../../../Themes/Images'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import Colors from '../../../Themes/Colors'

function WhatOnHorizontalComponent() {
  function renderItem() {
    return (
      <TouchableOpacity style={styles.itemContainer}>
        <FastImage source={Images.defaultImage} style={styles.image} />
        <View style={styles.contentContainer}>
          <View style={styles.dayContainer}>
            <Text style={{color: Colors().second}}>TUE</Text>
          </View>
          <Text style={styles.title}>$12 Burger Tuesdays</Text>
          <Text style={styles.desc}>Minskys Hotel</Text>
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <View style={styles.container}>
      <FlatList
        data={[{}, {}, {}]}
        renderItem={renderItem}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    marginLeft: responsiveWidth(15),
  },
  image: {
    height: responsiveHeight(150),
    width: responsiveWidth(250),
    borderRadius: responsiveHeight(15),
  },
  title: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(16),
    color: Colors().white,
  },
  desc: {
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(16),
    color: Colors().white,
  },
  dayContainer: {
    borderWidth: 1,
    borderColor: Colors().second,
    height: responsiveHeight(18),
    borderRadius: responsiveHeight(9),
    paddingHorizontal: responsiveWidth(5),
  },
  contentContainer: {
    position: 'absolute',
    bottom: responsiveHeight(15),
    left: responsiveHeight(15),
    alignItems: 'flex-start',
  },
})
export default WhatOnHorizontalComponent
