import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth, shadow} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import Fonts from '../../../Themes/Fonts'
import {TouchableCmp} from '../../../components/UtilityFunctions'
import CustomIcon from '../../../components/CustomIcon'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'

function MenuItem({
  title,
  subTitle,
  dataTitle,
  style,
  iconImage,
  iconName,
  value,
  onPress,
  valueRight = true,
  color = Colors().home.menuText,
  iconSelector,
  disabled,
  actionSubTitle,
}) {
  return (
    <View style={[styles.container, style]}>
      {!!title && (
        <View style={styles.row}>
          <Text style={[styles.title, {color: Colors().home.menuText}]}>{title}</Text>
          {!!subTitle && (
            <Text
              style={[styles.subTitle, {color: Colors().home.subTitle}]}
              onPress={() => {
                navigate(RouteKey.PondHoppersScreen, {
                  action: actionSubTitle,
                })
              }}>
              {subTitle}
            </Text>
          )}
        </View>
      )}
      <TouchableCmp
        disabled={disabled}
        onPress={() => {
          onPress?.()
        }}>
        <View style={[styles.buttonContainer, {backgroundColor: Colors().home.menuBackground}]}>
          <CustomIcon
            image_icon={iconImage}
            icon_selector={iconSelector}
            name={iconName}
            size={responsiveFont(35)}
            color={color}
            tintColor={color}
          />
          <View style={{flex: 1, justifyContent: 'center', marginLeft: responsiveWidth(15)}}>
            {!!dataTitle && <Text style={[styles.itemTitle, {color: color}]}>{dataTitle}</Text>}
            {!valueRight && !!value && (
              <Text style={[styles.itemValue, {color: Colors().home.menuText}]}>{value}</Text>
            )}
          </View>
          {valueRight && !!value && (
            <Text style={[styles.itemValue, {color: Colors().home.menuText}]}>{value}</Text>
          )}
        </View>
      </TouchableCmp>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginBottom: responsiveHeight(15),
    marginHorizontal: responsiveWidth(15),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  title: {
    fontSize: responsiveFont(18),
    flex: 1,
    fontFamily: Fonts.openSansBold,
    marginBottom: responsiveHeight(8),
  },
  subTitle: {
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(12),
  },
  buttonContainer: {
    flexDirection: 'row',
    ...shadow,
    borderRadius: responsiveWidth(15),
    paddingVertical: responsiveWidth(12),
    paddingHorizontal: responsiveWidth(12),
    alignItems: 'center',
  },
  itemTitle: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(16),
  },
  itemValue: {
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(12),
  },
})
export default MenuItem
