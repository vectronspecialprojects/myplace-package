import React from 'react'
import {View, StyleSheet, ScrollView, RefreshControl, Text, ImageBackground} from 'react-native'
import Colors from '../../../Themes/Colors'
import {localize} from '../../../locale/I18nConfig'
import Styles from '../../../Themes/Styles'
import {deviceWidth, responsiveFont, responsiveHeight, responsiveWidth, shadow} from '../../../Themes/Metrics'
import Swiper from 'react-native-swiper'
import FastImage from 'react-native-fast-image'
import {isTrue} from '../../../utilities/utils'
import Fonts from '../../../Themes/Fonts'
import StarButton from '../../../components/StarButton'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import Notification from '../../../components/Notification'
import MemberNameBar from '../../../components/MemberNameBar'
import HomeTilesBar from '../../../components/HomeTilesBar'
import PreferredVenueButton from '../../../components/PreferredVenueButton'
import BarcodeBar from '../../../components/BarcodeBar'
import {useSelector} from 'react-redux'

function HomeVersion1({
  isRefreshing,
  internetState,
  setIsRefreshing,
  loadContent,
  galleries,
  communityImpact,
  pledge,
  profile,
  notifications,
  homeMenus,
  ifRoute,
}) {
  const enableShowTier = useSelector(state => state.app.enableShowTier)
  const appFlags = useSelector(state => state.app.appFlags)
  return (
    <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={isRefreshing}
          onRefresh={() => {
            if (internetState) {
              setIsRefreshing(true)
              loadContent()
            }
          }}
          tintColor={Colors().defaultRefreshSpinner}
          titleColor={Colors().defaultRefreshSpinner}
          title={localize('pullToRefresh')}
        />
      }>
      <View
        style={{
          ...Styles.centerContainer,
          ...{height: responsiveHeight(300)},
        }}>
        <Swiper loop={true} style={{height: responsiveHeight(300)}} autoplay={true} showsPagination={false}>
          {galleries.map((item, index) => {
            return (
              <FastImage
                key={index.toString()}
                source={{uri: item}}
                style={{width: deviceWidth(), height: responsiveHeight(300)}}
              />
            )
          })}
        </Swiper>
      </View>

      {isTrue(appFlags?.app_is_show_star_bar) && (
        <View
          style={{
            ...styles.tiersContainer,
            ...{
              backgroundColor: profile?.member?.member_tier?.tier?.color || Colors().second,
            },
          }}
        />
      )}

      <View style={{backgroundColor: Colors().defaultBackground}}>
        {isTrue(appFlags?.app_is_show_star_bar) && (
          <StarButton
            style={{marginBottom: responsiveHeight(10)}}
            color={profile?.member?.member_tier?.tier?.color || Colors().second}
            onPress={() => navigate(RouteKey.CameraNavigator)}
          />
        )}
        {notifications?.length >= 1 && <Notification notifications={notifications} />}
        <MemberNameBar
          name={profile}
          onIconPress={() => {
            navigate(RouteKey.ProfileScreen)
          }}
          style={{marginTop: isTrue(appFlags?.app_is_show_star_bar) ? 0 : responsiveHeight(24)}}
        />
        {isTrue(appFlags?.app_show_tier_below_name) && (
          <Text style={Styles.smallCapText}>{profile?.member?.member_tier?.tier?.name}</Text>
        )}
        {isTrue(appFlags?.app_show_points_below_name) && (
          <Text style={[Styles.smallNormalText, {alignSelf: 'center'}]}>
            POINTS BALANCE: {+profile?.member?.numberOf?.points}
          </Text>
        )}
        <HomeTilesBar
          homeMenus={homeMenus}
          userInfo={profile}
          onPress={ifRoute}
          marginVertical={responsiveHeight(30)}
        />
      </View>

      {!isTrue(appFlags?.app_is_show_preferred_venue) && (
        <View
          style={{
            ...styles.singleTierBar,
            ...{
              backgroundColor: profile?.member?.member_tier?.tier?.color || Colors().home.tierBackground,
            },
          }}>
          {isTrue(appFlags?.app_show_tier_name) && (
            <Text
              style={{
                ...Styles.largeCapText,
                ...{color: Colors().home.tierBarText},
              }}>
              {profile?.member?.member_tier?.tier?.name}
            </Text>
          )}
        </View>
      )}

      {isTrue(appFlags?.app_is_show_preferred_venue) && (
        <PreferredVenueButton
          style={{
            backgroundColor:
              profile?.member?.current_preferred_venue_full?.is_preffered_venue_color === 'true' &&
              !!profile?.member?.current_preferred_venue_full?.color
                ? profile?.member?.current_preferred_venue_full?.color
                : Colors().home.preferredVenueButton,
          }}
          icon="up"
          venueName={profile?.member?.current_preferred_venue_name}
          ifOnlineOrdering={profile?.member?.current_preferred_venue_full?.your_order_integration}
          onPress={() => {
            navigate(RouteKey.PreferredVenueScreen)
          }}
        />
      )}

      <BarcodeBar
        value={profile?.member?.bepoz_account_card_number}
        width={responsiveWidth(profile?.member?.bepoz_account_card_number?.length <= 10 ? 1.7 : 1.2)}
        title={localize('memberNumber')}
        text={profile?.member?.bepoz_account_number}
        style={{
          paddingTop: responsiveHeight(30),
          borderTopWidth: responsiveHeight(1),
          borderTopColor: Colors().home.barcodeTopBorder,
        }}
      />
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  tiersContainer: {
    width: '100%',
    height: responsiveHeight(37),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#65c8c6',
  },
  barcode: {
    width: responsiveWidth(200),
    height: responsiveHeight(50),
    alignSelf: 'center',
  },
  textWrapper: {
    flex: 1,
    paddingHorizontal: responsiveWidth(2),
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  singleTierBar: {
    height: responsiveHeight(55),
    justifyContent: 'center',
  },
})
export default HomeVersion1
