import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {View, Text, RefreshControl, FlatList} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {responsiveHeight} from '../Themes/Metrics'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import {wait} from './../components/UtilityFunctions'
import SubHeaderBar from './../components/SubHeaderBar'
import MessageBoxPopup from './../components/MessageBoxPopup'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import CardItem from './../components/CardItem'
import CardDetail from './../components/CardDetail'
import Filter from './../components/Filter'
import {Card} from '../modals/modals'
import RouteKey from '../navigation/RouteKey'
import Toast from '../components/Toast'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const GiftCertificateScreen = (props) => {
  const title = props.route.params.params.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [isFilter, setIsFilter] = useState(false)
  const [selectedId, setSelectedId] = useState(useSelector((state) => state.infoServices.preferredVenueId))
  const [selectedGift, setSelectedGift] = useState(null)
  const [isOpen, setIsOpen] = useState(false)
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const [comment, setComment] = useState('')
  const giftCertificate = useSelector((state) => state.infoServices.giftCertificate)
  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetctListings(5))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const filterHandler = (venue) => {
    setSelectedId(venue?.id)
    setIsFilter(false)
  }

  const isGiftExpanded = (giftId) => {
    if (selectedGift === giftId) {
      setIsOpen(false)
      setSelectedGift(null)
    } else {
      setIsOpen(true)
      setSelectedGift(giftId)
    }
  }

  const cancelPopup = () => {
    setIsPopupVisible(false)
    setComment('')
  }

  const submitEnquiry = async () => {
    setIsPopupVisible(false)
    try {
      const rest = await dispatch(infoServicesActions.submitEnquiry(selectedGift, popupHeader, comment))
      Toast.success(rest)
    } catch (err) {
      wait(400).then(() =>
        Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}]),
      )
    }
    setComment('')
  }

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      dispatch(setGlobalIndicatorVisibility(true))
      setIsOpen(false)
      setSelectedGift(null)
      loadContent()
    })
    return () => {
      unsubscribe()
    }
  }, [loadContent])

  const switchFavorite = async (listingId, favorite) => {
    try {
      const rest = await dispatch(infoServicesActions.switchFavorite(listingId, favorite))
      dispatch(setGlobalIndicatorVisibility(true))
      loadContent()
      Toast.success(rest)
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
  }

  const popupHeader = useMemo(() => {
    if (giftCertificate === undefined || !selectedGift) return ''
    if (giftCertificate !== undefined && selectedGift) {
      return giftCertificate?.filter((data) => data.listing.id === selectedGift)[0].listing.heading
    }
  }, [giftCertificate, selectedGift])

  const showData = useMemo(() => {
    if (giftCertificate === undefined) return []
    if (giftCertificate !== undefined) {
      if (selectedId === 0)
        return giftCertificate?.sort((a, b) => {
          return a.listing.display_order - b.listing.display_order
        })
      return giftCertificate
        ?.filter((data) => data.listing.venue.id === 0 || data.listing.venue.id === selectedId)
        .sort((a, b) => {
          return a.listing.display_order - b.listing.display_order
        })
    }
  }, [giftCertificate, selectedId])

  const renderItem = (itemData) => {
    const listing = itemData.item.listing
    const cardDetail = new Card(listing.heading, listing.desc_short, 0, 0, 0, listing.image_banner)
    return (
      <View style={{marginBottom: responsiveHeight(5)}}>
        <CardItem
          showDetail={!(selectedGift === listing.id && isOpen)}
          cardDetail={cardDetail}
          titleStyle={{color: Colors().giftCertificate.cardTitle}}
          onPress={isGiftExpanded.bind(this, listing.id)}
        />
        {selectedGift === listing.id && isOpen && (
          <CardDetail
            title={listing.heading}
            titleStyle={{color: Colors().giftCertificate.cardTitleExpanded}}
            data={listing}
            html={listing.desc_long}
            favorite={listing.favorite}
            onFavoritePress={switchFavorite.bind(this, listing.id, listing.favorite)}
            onChatPress={() => setIsPopupVisible(!isPopupVisible)}
            products={listing.products}
            allowBooking={listing?.extra_settings?.add_booking === 'true' && listing?.products?.length === 1}
            allowChat={listing?.extra_settings?.add_enquiry === 'true'}
            handleCartPress={() =>
              props.navigation.navigate(RouteKey.BuyGiftCertificateScreen, {
                listing: listing,
              })
            }
          />
        )}
      </View>
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar
        title={title}
        filterBtn={true}
        onFilterPress={() => {
          setIsFilter(true)
        }}
      />
      <Filter
        isVisible={isFilter}
        backScreenOnPress={() => {
          setIsFilter(false)
        }}
        selectedId={selectedId}
        onFilterPress={filterHandler}
      />
      <FlatList
        data={showData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        ListEmptyComponent={
          <Text style={Styles.flatlistNoItems}>
            No gift giftCertificates found, please check again later.
          </Text>
        }
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
            tintColor={Colors().defaultRefreshSpinner}
            titleColor={Colors().defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }
      />
      <MessageBoxPopup
        isVisible={isPopupVisible}
        header={popupHeader}
        onCancelPress={cancelPopup}
        onOkPress={submitEnquiry}
        onChangeText={(text) => setComment(text)}
        value={comment}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default GiftCertificateScreen
