import React, {useState} from 'react'
import {View, StyleSheet, ScrollView, Image} from 'react-native'
import {useDispatch} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {wait} from '../components/UtilityFunctions'
import CardDetail from './../components/CardDetail'
import MessageBoxPopup from './../components/MessageBoxPopup'
import Styles from '../Themes/Styles'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Toast from '../components/Toast'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Colors from '../Themes/Colors'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import dayjs from 'dayjs'
import { isTrue } from '../utilities/utils'

const WhatsonDetailsScreen = (props) => {
  const dispatch = useDispatch()
  const listingDetail = props.route?.params.listing || {}
  const [favorite, setFavorite] = useState(!!props.route?.params.listing.favorite)
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const [comment, setComment] = useState('')

  console.log('listingDetail', listingDetail)

  const cancelPopup = () => {
    setIsPopupVisible(false)
    setComment('')
  }

  const submitEnquiry = async () => {
    setIsPopupVisible(false)
    try {
      const rest = await dispatch(
        infoServicesActions.submitEnquiry(listingDetail.id, listingDetail.heading, comment),
      )
      Toast.success(rest)
    } catch (err) {
      wait(400).then(() =>
        Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}]),
      )
    }
    setComment('')
  }

  const switchFavorite = async () => {
    try {
      const rest = await dispatch(infoServicesActions.switchFavorite(listingDetail.id, favorite))
      Toast.success(rest)
      setFavorite(!favorite)
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
  }


  return (
    <View style={Styles.screen}>
      <View style={{flex: 1}}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
          <View style={styles.imageContainer}>
            <Image style={styles.imageBanner} source={{uri: listingDetail.image_banner}} />
          </View>

          <CardDetail
            title={listingDetail.heading}
            titleStyle={{color: Colors().whatonDetails.title}}
            venueName={listingDetail.venue.name}
            html={listingDetail.desc_long}
            days={JSON.parse(listingDetail.payload).occurrence}
            favorite={favorite}
            data={listingDetail}
            onFavoritePress={switchFavorite}
            onChatPress={() => setIsPopupVisible(!isPopupVisible)}
            products={listingDetail.products}
            allowBooking={
              isTrue(listingDetail?.extra_settings?.add_booking) && listingDetail?.products?.length >= 1
            }
            allowChat={isTrue(listingDetail?.extra_settings?.add_enquiry)}
            startTime={
              listingDetail.type.id === 2
                ? dayjs(listingDetail?.time_start, 'HH:mm:ss').format('h:mm a')
                : dayjs(listingDetail?.datetime_start, 'YYYY-MM-DD HH:mm:ss').format('h:mm a')
            }
            endTime={
              listingDetail.type.id === 2
                ? dayjs(listingDetail?.time_end, 'HH:mm:ss').format('h:mm a')
                : dayjs(listingDetail?.datetime_end, 'YYYY-MM-DD HH:mm:ss').format('h:mm a')
            }
          />
        </ScrollView>
      </View>
      <MessageBoxPopup
        isVisible={isPopupVisible}
        header={listingDetail.heading}
        onCancelPress={cancelPopup}
        onOkPress={submitEnquiry}
        onChangeText={(text) => setComment(text)}
        value={comment}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  imageContainer: {
    width: '100%',
    height: responsiveHeight(120),
    minHeight: 110,
  },
  imageBanner: {
    width: '100%',
    height: responsiveHeight(120),
    minHeight: 110,
    resizeMode: 'cover',
  },
  venueName: {
    marginTop: responsiveWidth(10),
    alignSelf: 'center',
  },
})

export default WhatsonDetailsScreen
