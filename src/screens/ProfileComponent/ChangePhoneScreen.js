import React, {useMemo, useState} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {changePhone} from '../../store/actions/authServices'
import Toast from '../../components/Toast'
import {localize} from '../../locale/I18nConfig'
import Styles from '../../Themes/Styles'
import SubHeaderBar from '../../components/SubHeaderBar'
import TextInputView from '../../components/TextInputView'
import ButtonView from '../../components/ButtonView'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Colors from '../../Themes/Colors'
import HeaderLeftButton from '../../components/HeaderLeftButton'

function ChangePhoneScreen(props) {
  const [newPhone, setNewPhone] = useState('')
  const [confirmPhone, setConfirmPhone] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()

  async function handleChangeEmail() {
    try {
      if (validateData()) {
        dispatch(setGlobalIndicatorVisibility(true))
        await dispatch(changePhone(newPhone, password))
        props.navigation.pop()
        Toast.success(localize('changePhone.messageSuccess'))
      }
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  function validateData() {
    if (newPhone !== confirmPhone) {
      Toast.success(localize('changePhone.confirmPhoneNotMatch'))
      return false
    }
    return true
  }

  const disableButton = useMemo(() => {
    return !(newPhone && confirmPhone && password)
  }, [newPhone, confirmPhone, password])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('changePhone.title')} />
      <ScrollView style={styles.container} bounces={false}>
        <TextInputView
          floatTitle={localize('changePhone.newPhone')}
          value={newPhone}
          onChangeText={setNewPhone}
          maxLength={10}
          keyboardType={'numeric'}
        />
        <TextInputView
          floatTitle={localize('changePhone.confirmNewPhone')}
          value={confirmPhone}
          onChangeText={setConfirmPhone}
          maxLength={10}
          keyboardType={'numeric'}
        />
        <TextInputView
          floatTitle={localize('changePhone.password')}
          secureTextEntry={true}
          value={password}
          onChangeText={setPassword}
        />
        <ButtonView
          title={localize('changePhone.continue')}
          disabled={disableButton}
          style={{marginVertical: responsiveHeight(20)}}
          onPress={() => handleChangeEmail()}
        />
        <ButtonView
          title={localize('changePhone.cancel')}
          style={[
            styles.buttonBorder2,
            {
              borderColor: Colors().changePhone.secondButtonBorder,
              backgroundColor: Colors().changePhone.secondButtonBackground,
            },
          ]}
          titleStyle={{color: Colors().changePhone.secondButtonText}}
          onPress={() => {
            props.navigation.pop()
          }}
        />
      </ScrollView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(24),
    marginTop: responsiveHeight(20),
  },
  buttonBorder2: {
    borderWidth: 2,
  },
})
export default ChangePhoneScreen
