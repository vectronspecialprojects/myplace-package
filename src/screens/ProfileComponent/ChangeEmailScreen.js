import React, {useMemo, useState} from 'react'
import {View, ScrollView, StyleSheet} from 'react-native'
import SubHeaderBar from '../../components/SubHeaderBar'
import Styles from '../../Themes/Styles'
import TextInputView from '../../components/TextInputView'
import ButtonView from '../../components/ButtonView'
import {isIOS, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Colors from '../../Themes/Colors'
import {validateEmail} from '../../utilities/utils'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {changeEmail} from '../../store/actions/authServices'
import Toast from '../../components/Toast'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {localize} from '../../locale/I18nConfig'

const ChangeEmailScreen = (props) => {
  const [newMail, setNewMail] = useState('')
  const [confirmMail, setConfirmMail] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()

  async function handleChangeEmail() {
    try {
      if (validateData()) {
        dispatch(setGlobalIndicatorVisibility(true))
        await dispatch(changeEmail(newMail, password))
        props.navigation.pop()
        Toast.success(localize('changeEmail.messageSuccess'))
      }
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  function validateData() {
    if (!validateEmail(newMail)) {
      Toast.success(localize('changeEmail.invalidEmail'))
      return false
    }
    if (newMail !== confirmMail) {
      Toast.success(localize('changeEmail.confirmEmailNotMatch'))
      return false
    }
    return true
  }

  const disableButton = useMemo(() => {
    return !(confirmMail && newMail && password)
  }, [confirmMail, newMail, password])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('changeEmail.title')} />
      <ScrollView style={styles.container} bounces={false}>
        <TextInputView
          floatTitle={localize('changeEmail.newEmail')}
          value={newMail}
          onChangeText={(text) => setNewMail(text)}
        />
        <TextInputView
          floatTitle={localize('changeEmail.confirmNewEmail')}
          value={confirmMail}
          onChangeText={(text) => setConfirmMail(text)}
        />
        <TextInputView
          floatTitle={localize('changeEmail.password')}
          secureTextEntry={true}
          value={password}
          onChangeText={(text) => setPassword(text)}
        />
        <ButtonView
          title={localize('changeEmail.continue')}
          disabled={disableButton}
          style={{marginVertical: responsiveHeight(20)}}
          onPress={() => handleChangeEmail()}
        />
        <ButtonView
          title={localize('changeEmail.cancel')}
          style={[
            styles.buttonBorder2,
            {
              borderColor: Colors().changeEmail.secondButtonBorder,
              backgroundColor: Colors().changeEmail.secondButtonBackground,
            },
          ]}
          titleStyle={{color: Colors().changeEmail.secondButtonText}}
          onPress={() => {
            props.navigation.pop()
          }}
        />
      </ScrollView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(24),
    marginTop: responsiveHeight(20),
  },
  buttonBorder2: {
    borderWidth: 2,
  },
})

export default ChangeEmailScreen
