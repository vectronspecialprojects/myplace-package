import React from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import Avatar from '../../../components/Avatar'
import {EntypoTouch} from '../../../components/UtilityFunctions'
import RouteKey from '../../../navigation/RouteKey'
import TextInputView from '../../../components/TextInputView'
import {localize} from '../../../locale/I18nConfig'
import ButtonView from '../../../components/ButtonView'
import {isTrue} from '../../../utilities/utils'
import {useSelector} from 'react-redux'

function ProfileVersion1({
  updateImage,
  setUpdateImage,
  profile,
  navigation,
  dataUpdate,
  handleChangeData,
  isAllowUpdate,
  handleUpdateData,
}) {
  const appFlags = useSelector((state) => state.app.appFlags)
  return (
    <View style={styles.container}>
      <View style={{height: responsiveHeight(100), backgroundColor: Colors().second}}>
        <Avatar
          uri={updateImage?.path || profile?.member?.profile_img}
          size={responsiveWidth(128)}
          style={styles.avatar}
          editable={true}
          onImageSelect={(image) => {
            setUpdateImage(image)
          }}
        />
      </View>
      <View style={styles.actionContainer}>
        <EntypoTouch
          name="heart-outlined"
          size={30}
          color={Colors().profile.icon}
          onPress={() => {
            navigation.navigate(RouteKey.FavoriteScreen)
          }}
        />
        <EntypoTouch
          name="back-in-time"
          size={30}
          color={Colors().profile.icon}
          onPress={() => {
            navigation.navigate(RouteKey.HistoryScreen)
          }}
        />
      </View>
      <ScrollView>
        <View style={{paddingHorizontal: responsiveWidth(20)}}>
          <TextInputView
            textInputStyle={{color: Colors().profile.textInput}}
            value={dataUpdate?.first_name ?? profile?.member?.first_name}
            floatTitle={localize('profile.firstName')}
            onChangeText={(value) => handleChangeData('first_name', value)}
          />
          <TextInputView
            textInputStyle={{color: Colors().profile.textInput}}
            value={dataUpdate?.last_name ?? profile?.member?.last_name}
            floatTitle={localize('profile.lastName')}
            onChangeText={(value) => handleChangeData('last_name', value)}
          />
          <TextInputView
            textInputStyle={{color: Colors().profile.textInput}}
            value={profile?.member?.dob}
            floatTitle={localize('profile.dob')}
            editable={false}
          />
          <TextInputView
            textInputStyle={{color: Colors().profile.textInput}}
            value={profile?.mobile}
            floatTitle={localize('profile.mobile')}
            editable={false}
          />
          <TextInputView
            textInputStyle={{color: Colors().profile.textInput}}
            value={profile?.email}
            floatTitle={localize('profile.email')}
            editable={false}
          />
          {isTrue(appFlags?.app_show_tier_name) && (
            <TextInputView
              textInputStyle={{color: Colors().profile.textInput}}
              value={profile?.member?.member_tier?.tier?.name}
              floatTitle={localize('profile.tierName')}
              editable={false}
            />
          )}
          <View style={styles.buttonContainer}>
            <ButtonView
              title={localize('profile.resetPassword')}
              style={[
                styles.buttonBorder,
                {
                  borderColor: Colors().profile.borderButtonColor,
                  backgroundColor: Colors().profile.backgroundButton,
                },
              ]}
              titleStyle={{color: Colors().profile.buttonText}}
              onPress={() => {
                navigation.navigate(RouteKey.ResetPasswordScreen)
              }}
            />
            <ButtonView
              title={localize('profile.changeEmail')}
              style={[
                styles.buttonBorder,
                {
                  borderColor: Colors().profile.borderButtonColor,
                  backgroundColor: Colors().profile.backgroundButton,
                },
              ]}
              titleStyle={{color: Colors().profile.buttonText}}
              onPress={() => {
                navigation.navigate(RouteKey.ChangeEmailScreen)
              }}
            />
            <ButtonView
              title={localize('profile.changePhone')}
              style={[
                styles.buttonBorder,
                {
                  borderColor: Colors().profile.borderButtonColor,
                  backgroundColor: Colors().profile.backgroundButton,
                },
              ]}
              titleStyle={{color: Colors().profile.buttonText}}
              onPress={() => {
                navigation.navigate(RouteKey.ChangePhoneScreen)
              }}
            />
            <ButtonView
              title={localize('profile.saveChange')}
              disabled={!isAllowUpdate && !updateImage}
              onPress={handleUpdateData}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    alignSelf: 'center',
    bottom: responsiveWidth(-64),
    position: 'absolute',
    zIndex: 999,
  },
  buttonBorder: {
    borderWidth: 2,
    marginBottom: responsiveHeight(8),
  },
  buttonContainer: {
    marginTop: responsiveHeight(68),
    marginBottom: responsiveHeight(20),
  },
  actionContainer: {
    height: responsiveHeight(80),
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: responsiveWidth(20),
    paddingTop: responsiveHeight(10),
  },
})
export default ProfileVersion1
