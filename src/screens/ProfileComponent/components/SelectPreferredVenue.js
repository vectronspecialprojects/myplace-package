import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import ButtonView from '../../../components/ButtonView'
import {navigate} from '../../../navigation/NavigationService'
import RouteKey from '../../../navigation/RouteKey'
import Colors from '../../../Themes/Colors'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import {useSelector} from 'react-redux'
import Fonts from '../../../Themes/Fonts'
import {localize} from '../../../locale/I18nConfig'

function SelectPreferredVenue() {
  const profile = useSelector(state => state.infoServices.profile)
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{localize('profile.preferredVenue')}</Text>
      <ButtonView
        onPress={() => {
          navigate(RouteKey.PreferredVenueScreen)
        }}
        title={profile?.member?.current_preferred_venue_name || 'Venue name'}
        style={[styles.venueButton, {backgroundColor: Colors().home.menuBackground}]}
        titleStyle={{color: Colors().home.name, textAlign: null, fontFamily: Fonts.openSans}}
        rightIcon={
          <MaterialCommunityIcons
            name={'chevron-down'}
            size={22}
            style={{marginRight: responsiveWidth(20)}}
          />
        }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: responsiveHeight(10),
  },
  venueButton: {
    height: responsiveHeight(38),
    borderRadius: responsiveHeight(19),
    marginBottom: responsiveHeight(10),
    borderWidth: 1,
  },
  title: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(16),
    marginBottom: responsiveHeight(5),
  },
})
export default SelectPreferredVenue
