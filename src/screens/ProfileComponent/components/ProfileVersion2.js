import React, {useRef} from 'react'
import {View, StyleSheet, ScrollView, Text} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Colors from '../../../Themes/Colors'
import Avatar from '../../../components/Avatar'
import Fonts from '../../../Themes/Fonts'
import {localize} from '../../../locale/I18nConfig'
import BarcodeBar from '../../../components/BarcodeBar'
import SelectPreferredVenue from './SelectPreferredVenue'
import MenuItem from './MenuItem'
import RouteKey from '../../../navigation/RouteKey'
import {useDispatch, useSelector} from 'react-redux'
import ButtonView from '../../../components/ButtonView'
import * as authServicesActions from '../../../store/actions/authServices'
import Alert from '../../../components/Alert'

const ACCOUNT_MENU = [
  {
    title: 'Edit my detail',
    route: RouteKey.UpdateAccountScreen,
  },
  {
    title: 'Change my password',
    route: RouteKey.ResetPasswordScreen,
  },
  // {
  //   title: 'Change my profile picture',
  //   route: '',
  //   key: 'updateAvatar',
  // },
  {
    title: 'My favourites',
    route: RouteKey.FavoriteScreen,
    params: '',
  },
  // {
  //   title: 'Notification preferences',
  //   route: '',
  // },
]

function ProfileVersion2({
  updateImage,
  setUpdateImage,
  profile,
  navigation,
  dataUpdate,
  handleChangeData,
  isAllowUpdate,
  handleUpdateData,
}) {
  const profileMenu = useSelector((state) => state.app.profileMenu)
  const avatarRef = useRef()
  const dispatch = useDispatch()

  return (
    <View style={styles.container}>
      <View style={{height: responsiveHeight(56), backgroundColor: Colors().second}}>
        <Avatar
          uri={updateImage?.path || profile?.member?.profile_img}
          size={responsiveWidth(112)}
          style={styles.avatar}
          editable={true}
          onImageSelect={(image) => {
            setUpdateImage(image)
          }}
          ref={avatarRef}
        />
      </View>
      <ScrollView
        style={{marginTop: responsiveHeight(60), paddingHorizontal: responsiveWidth(20)}}
        showsVerticalScrollIndicator={false}>
        <Text style={[styles.name, {color: Colors().profile.name}]}>
          {profile?.member?.first_name} {profile?.member?.last_name}
        </Text>
        <Text style={[styles.tier, {color: Colors().profile.tier}]}>
          {profile?.member?.member_tier?.tier?.name}
        </Text>
        <BarcodeBar
          value={profile?.member?.bepoz_account_card_number}
          width={responsiveWidth(profile?.member?.bepoz_account_card_number.length <= 10 ? 1.7 : 1.2)}
          title={localize('memberNumber')}
          text={profile?.member?.bepoz_account_number}
          style={[styles.barcode, {borderTopColor: Colors().profile.barcodeTopBorder}]}
        />
        <SelectPreferredVenue />
        <Text style={styles.title}>Account</Text>
        {ACCOUNT_MENU.map((item, index) => {
          return (
            <MenuItem
              key={index.toString()}
              title={item.title}
              onPress={() => {
                if (item.key === 'updateAvatar') {
                  avatarRef.current?.showOption(true)
                } else {
                  navigation.navigate(item.route)
                }
              }}
            />
          )
        })}
        <Text style={[styles.title, {marginTop: responsiveHeight(20)}]}>{localize('profile.support')}</Text>
        {profileMenu?.map((item, index) => {
          return (
            <MenuItem
              key={index.toString()}
              title={item.page_name}
              onPress={() => {
                navigation.navigate(item.state + item.id)
              }}
            />
          )
        })}
        <ButtonView
          style={[styles.buttonSignOut, {borderColor: Colors().second}]}
          title={localize('profile.signOut')}
          titleStyle={{color: Colors().second}}
          onPress={() => {
            Alert.alert('Logout', 'Are you sure you want to logout?', [
              {
                text: 'No',
              },
              {
                text: 'Yes',
                onPress: () => dispatch(authServicesActions.logout()),
                style: {backgroundColor: Colors().white, borderWidth: 1, borderColor: Colors().second},
                textStyle: {color: Colors().second},
              },
            ])
          }}
        />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    alignSelf: 'center',
    bottom: responsiveWidth(-56),
    position: 'absolute',
    zIndex: 999,
  },
  name: {
    fontSize: responsiveFont(20),
    textAlign: 'center',
    fontFamily: Fonts.openSansBold,
  },
  tier: {
    fontSize: responsiveFont(18),
    textAlign: 'center',
    fontFamily: Fonts.openSans,
  },
  barcode: {
    paddingTop: responsiveHeight(24),
  },
  title: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(16),
    marginTop: responsiveHeight(5),
  },
  buttonSignOut: {
    marginVertical: responsiveHeight(30),
    backgroundColor: null,
    borderWidth: 1,
  },
})
export default ProfileVersion2
