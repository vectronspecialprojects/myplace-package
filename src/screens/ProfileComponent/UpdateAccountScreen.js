import React, {useCallback, useEffect, useMemo, useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import TextInputView from '../../components/TextInputView'
import Colors from '../../Themes/Colors'
import {localize} from '../../locale/I18nConfig'
import {useDispatch, useSelector} from 'react-redux'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Fonts from '../../Themes/Fonts'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {updateProfile, updateProfileAvatar} from '../../store/actions/infoServices'
import Toast from '../../components/Toast'
import ButtonView from '../../components/ButtonView'
import * as api from '../../utilities/ApiManage'

function UpdateAccountScreen({navigation}) {
  const profile = useSelector((state) => state.infoServices.profile)
  const [dataUpdate, setDataUpdate] = useState({})
  const [dataFields, setDataFields] = useState([])
  const dispatch = useDispatch()
  console.log(profile)
  useEffect(() => {
    getFields()
  }, [])

  async function getFields() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await api.getUserProfileSetting()
      if (!res.ok) throw new Error(res?.message)
      let list = res?.bepozcustomfield?.extended_value
      list = list
        .filter((item) => item.displayInApp && item.shownOnProfile)
        .sort((a, b) => +a.displayOrder - +b.displayOrder)
      setDataFields(list)
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const handleChangeData = useCallback(
    (key, value) => {
      setDataUpdate({
        ...dataUpdate,
        [key]: value,
      })
    },
    [dataUpdate],
  )

  console.log(dataUpdate)

  async function handleUpdateData() {
    try {
      await dispatch(updateProfile(dataUpdate))
    } catch (e) {
      console.log(e)
      dispatch(setGlobalIndicatorVisibility(false))
      Toast.info(e.message)
    } finally {
    }
  }

  function renderItem(item, index) {
    return (
      <TextInputView
        key={index.toString()}
        textInputStyle={{color: Colors().profile.textInput}}
        value={dataUpdate?.[item.id] ?? (profile?.[item.id] || profile?.member?.[item.id])}
        floatTitle={item.fieldDisplayTitle}
        onChangeText={(value) => handleChangeData(item.id, value)}
        style={{marginBottom: responsiveHeight(20)}}
        titleStyle={{fontFamily: Fonts.openSansBold}}
        editable={!!item.editOnProfile}
        keyboardType={item.keyboardType}
      />
    )
  }

  return (
    <View style={[styles.container, {backgroundColor: Colors().white}]}>
      <Text style={[styles.title, {color: Colors().second}]}>{localize('profile.updateTitle')}</Text>
      {dataFields.map(renderItem)}
      <View style={{flexDirection: 'row', marginTop: responsiveHeight(15)}}>
        <ButtonView
          title={'Revert changes'}
          style={styles.revertChange}
          titleStyle={{color: Colors().black}}
          onPress={() => {
            setDataUpdate({})
          }}
        />
        <ButtonView title={'Save changes'} style={{flex: 1}} onPress={handleUpdateData} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(15),
  },
  title: {
    fontSize: responsiveFont(20),
    fontFamily: Fonts.openSansBold,
    marginVertical: responsiveHeight(24),
    marginLeft: responsiveHeight(20),
  },
  revertChange: {
    backgroundColor: null,
    flex: 1,
    marginRight: responsiveWidth(15),
    borderWidth: 1,
  },
})
export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default UpdateAccountScreen
