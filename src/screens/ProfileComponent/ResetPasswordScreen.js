import React, {useMemo, useState} from 'react'
import {View, ScrollView, StyleSheet} from 'react-native'
import SubHeaderBar from '../../components/SubHeaderBar'
import Styles from '../../Themes/Styles'
import TextInputView from '../../components/TextInputView'
import {isIOS, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import ButtonView from '../../components/ButtonView'
import Colors from '../../Themes/Colors'
import {resetPassword} from '../../store/actions/authServices'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import Toast from '../../components/Toast'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {localize} from '../../locale/I18nConfig'

const ResetPasswordScreen = (props) => {
  const [oldPassword, setOldPassword] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const dispatch = useDispatch()

  async function handleChangePassword() {
    try {
      if (validateData()) {
        dispatch(setGlobalIndicatorVisibility(true))
        await dispatch(resetPassword(oldPassword, newPassword))
        props.navigation.pop()
        Toast.success(localize('resetPassword.messageSuccess'))
      }
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  function validateData() {
    if (confirmPassword !== newPassword) {
      Toast.success(localize('resetPassword.confirmNotMatch'))
      return false
    }
    return true
  }

  const disableButton = useMemo(() => {
    return !(oldPassword && newPassword && confirmPassword)
  }, [oldPassword, newPassword, confirmPassword])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('resetPassword.title')} />
      <ScrollView style={styles.container} bounces={false}>
        <TextInputView
          floatTitle={localize('resetPassword.oldPassword')}
          secureTextEntry={true}
          value={oldPassword}
          onChangeText={(text) => setOldPassword(text)}
        />
        <TextInputView
          floatTitle={localize('resetPassword.newPassword')}
          secureTextEntry={true}
          value={newPassword}
          onChangeText={(text) => setNewPassword(text)}
        />
        <TextInputView
          floatTitle={localize('resetPassword.confirmPassword')}
          secureTextEntry={true}
          value={confirmPassword}
          onChangeText={(text) => setConfirmPassword(text)}
        />
        <ButtonView
          title={localize('resetPassword.continue')}
          disabled={disableButton}
          style={{marginVertical: responsiveHeight(20)}}
          onPress={() => handleChangePassword()}
        />
        <ButtonView
          title={localize('resetPassword.cancel')}
          style={[
            styles.buttonBorder2,
            {
              borderColor: Colors().resetPassword.secondButtonBorder,
              backgroundColor: Colors().resetPassword.secondButtonBackground,
            },
          ]}
          titleStyle={{color: Colors().resetPassword.secondButtonText}}
          onPress={() => {
            props.navigation.pop()
          }}
        />
      </ScrollView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(24),
    marginTop: responsiveHeight(20),
  },
  buttonBorder2: {
    borderWidth: 2,
  },
})

export default ResetPasswordScreen
