import React, {useCallback, useMemo, useState} from 'react'
import {View, StyleSheet, ScrollView} from 'react-native'
import Styles from '../../Themes/Styles'
import {useDispatch, useSelector} from 'react-redux'
import {updateProfile, updateProfileAvatar} from '../../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import Toast from '../../components/Toast'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {localize} from '../../locale/I18nConfig'
import ProfileVersion1 from './components/ProfileVersion1'
import ProfileVersion2 from './components/ProfileVersion2'

const ProfileScreen = (props) => {
  const profile = useSelector((state) => state.infoServices.profile)
  const [dataUpdate, setDataUpdate] = useState({
    first_name: profile?.member?.first_name,
    last_name: profile?.member?.last_name,
    // dob: profile?.member?.dob,
  })
  const [updateImage, setUpdateImage] = useState('')
  const dispatch = useDispatch()
  const useLegacyDesign = useSelector((state) => state.app.useLegacyDesign)

  const handleChangeData = useCallback(
    (key, value) => {
      setDataUpdate({
        ...dataUpdate,
        [key]: value,
      })
    },
    [dataUpdate],
  )

  async function handleUpdateData() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      if (isAllowUpdate) await dispatch(updateProfile(dataUpdate, updateImage))
      if (updateImage) {
        await dispatch(updateProfileAvatar(updateImage))
        setUpdateImage('')
      }
      Toast.success(localize('profile.messUpdateSuccess'))
    } catch (e) {
      Toast.info(e.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  const isAllowUpdate = useMemo(() => {
    return (
      dataUpdate?.first_name !== profile?.member?.first_name ||
      dataUpdate?.last_name !== profile?.member?.last_name
    )
  }, [profile, dataUpdate])

  const Layout = useLegacyDesign ? ProfileVersion1 : ProfileVersion2

  return (
    <View style={Styles.screen}>
      <Layout
        profile={profile}
        navigation={props.navigation}
        dataUpdate={dataUpdate}
        handleUpdateData={handleUpdateData}
        handleChangeData={handleChangeData}
        isAllowUpdate={isAllowUpdate}
        setUpdateImage={setUpdateImage}
        updateImage={updateImage}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default ProfileScreen
