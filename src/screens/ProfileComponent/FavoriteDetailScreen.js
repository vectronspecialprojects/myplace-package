import React, {useState} from 'react'
import {View, StyleSheet, ScrollView, Image} from 'react-native'
import Styles from '../../Themes/Styles'
import SubHeaderBar from '../../components/SubHeaderBar'
import CardDetail from '../../components/CardDetail'
import MessageBoxPopup from '../../components/MessageBoxPopup'
import {responsiveHeight} from '../../Themes/Metrics'
import * as infoServicesActions from '../../store/actions/infoServices'
import {useDispatch} from 'react-redux'
import FastImage from 'react-native-fast-image'
import Toast from '../../components/Toast'
import HeaderLeftButton from '../../components/HeaderLeftButton'

function FavoriteDetailScreen({route, navigation}) {
  const {title, favoriteDetail} = route.params
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const [comment, setComment] = useState('')
  const [isFavorite, setFavorite] = useState(favoriteDetail?.favorite)
  const dispatch = useDispatch()

  const switchFavorite = async (listingId, favorite) => {
    try {
      setFavorite(!favorite)
      const rest = await dispatch(infoServicesActions.switchFavorite(listingId, favorite))
      Toast.success(rest)
      dispatch(infoServicesActions.getFavorite())
    } catch (err) {}
  }

  const cancelPopup = () => {
    setIsPopupVisible(false)
    setComment('')
  }

  const submitEnquiry = async () => {
    setIsPopupVisible(false)
    try {
      const res = await dispatch(
        infoServicesActions.submitEnquiry(favoriteDetail?.id, favoriteDetail?.heading, comment),
      )
      Toast.success(res)
    } catch (err) {
    } finally {
      setComment('')
    }
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      <View style={{flex: 1}}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
          <FastImage style={styles.imageBanner} source={{uri: favoriteDetail?.image_banner}} />
          <CardDetail
            title={favoriteDetail.heading}
            html={favoriteDetail?.desc_long}
            favorite={isFavorite}
            data={favoriteDetail}
            days={JSON.parse(favoriteDetail.payload || '{}').occurrence}
            onFavoritePress={() => switchFavorite(favoriteDetail?.id, isFavorite)}
            onChatPress={() => setIsPopupVisible(!isPopupVisible)}
            products={favoriteDetail.products}
            allowBooking={favoriteDetail?.extra_settings?.add_booking || !!favoriteDetail.products?.length}
          />
        </ScrollView>
      </View>
      <MessageBoxPopup
        isVisible={isPopupVisible}
        header={favoriteDetail?.heading}
        onCancelPress={cancelPopup}
        onOkPress={submitEnquiry}
        onChangeText={(text) => setComment(text)}
        value={comment}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  imageBanner: {
    width: '100%',
    height: responsiveHeight(120),
    minHeight: 110,
    resizeMode: 'cover',
  },
})

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default FavoriteDetailScreen
