import React, {useEffect, useCallback, useState, useMemo} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {View, Text, FlatList, RefreshControl} from 'react-native'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {Card} from '../../modals/modals'
import CardItem from '../../components/CardItem'
import Alert from '../../components/Alert'
import {fetchMembershipList} from '../../store/actions/infoServices'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import SubHeaderBar from '../../components/SubHeaderBar'
import {localize} from '../../locale/I18nConfig'
import {responsiveHeight} from '../../Themes/Metrics'
import RouteKey from '../../navigation/RouteKey'

const MembershipScreen = (props) => {
  const title = props.route.params.params?.page_name
  const membership = useSelector((state) => state.infoServices.membershiplist)
  const internetState = useSelector((state) => state.app.internetState)
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()

  const loadContent = useCallback(async () => {
    try {
      await dispatch(fetchMembershipList())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    if (internetState) {
      dispatch(setGlobalIndicatorVisibility(true))
      loadContent()
    }
  }, [loadContent, dispatch])

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      if (internetState) {
        dispatch(setGlobalIndicatorVisibility(true))
        loadContent()
      }
    })
    return () => {
      unsubscribe()
    }
  }, [loadContent])

  const showData = useMemo(() => {
    if (!membership?.length) return []
    return membership.sort((a, b) => a.listing.display_order - b.listing.display_order)
  }, [membership])

  const renderItem = (itemData) => {
    const listing = itemData.item.listing
    const cardDetail = new Card(listing.heading, listing.desc_short, 0, 0, 0, listing.image_banner)
    return (
      <View style={{marginBottom: responsiveHeight(5)}}>
        <CardItem
          cardDetail={cardDetail}
          titleStyle={{color: Colors().membershipScreen.cardTitle}}
          onPress={() => {
            props.navigation.navigate(RouteKey.MembershipDetailsScreen, {listing})
          }}
        />
      </View>
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} filterBtn={false} />
      <FlatList
        data={showData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        ListEmptyComponent={
          <Text style={Styles.flatlistNoItems}>No memberships found, please check again later.</Text>
        }
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
            tintColor={Colors().defaultRefreshSpinner}
            titleColor={Colors().defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default MembershipScreen
