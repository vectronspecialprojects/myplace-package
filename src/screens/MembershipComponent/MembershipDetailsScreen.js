import React, {useState} from 'react'
import {View, ScrollView, StyleSheet, Image} from 'react-native'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import MessageBoxPopup from '../../components/MessageBoxPopup'
import CardDetail from '../../components/CardDetail'
import {submitEnquiry, switchFavorite} from '../../store/actions/infoServices'
import {useDispatch} from 'react-redux'
import Colors from '../../Themes/Colors'
import Styles from '../../Themes/Styles'
import {responsiveHeight} from '../../Themes/Metrics'
import Alert from '../../components/Alert'
import Toast from '../../components/Toast'
import {localize} from '../../locale/I18nConfig'

const MembershipDetailsScreen = (props) => {
  const dispatch = useDispatch()
  const membershipDetail = props?.route?.params?.listing || {}
  const [favorite, setFavorite] = useState(!!membershipDetail?.favorite)
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const [comment, setComment] = useState('')


  const cancelPopup = () => {
    setIsPopupVisible(false)
    setComment('')
  }

  const submittingEnquiry = async () => {
    setIsPopupVisible(false)
    try {
      const rest = await dispatch(submitEnquiry(membershipDetail.id, membershipDetail.heading, comment))
      console.log(rest)
      Toast.success(rest)
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    setComment('')
  }

  const switchFavoritePress = async () => {
    try {
      const rest = await dispatch(switchFavorite(membershipDetail.id, favorite))
      Toast.success(rest)
      setFavorite(!favorite)
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
  }

  return (
    <View style={Styles.screen}>
      <View style={{flex: 1}}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
          <View style={styles.imageContainer}>
            <Image style={styles.imageBanner} source={{uri: membershipDetail?.image_banner}} />
          </View>
          <CardDetail
            title={membershipDetail.heading}
            titleStyle={{color: Colors().whatonDetails.title}}
            venueName={membershipDetail.venue.name}
            html={membershipDetail.desc_long}
            favorite={favorite}
            onFavoritePress={switchFavoritePress}
            data={membershipDetail}
            allowChat={membershipDetail?.extra_settings?.add_enquiry === 'true'}
            onChatPress={() => setIsPopupVisible(!isPopupVisible)}
            products={membershipDetail.products}
            allowBooking={
              membershipDetail?.extra_settings?.add_booking === 'true' &&
              membershipDetail?.products?.length >= 1
            }
          />
        </ScrollView>
      </View>
      <MessageBoxPopup
        isVisible={isPopupVisible}
        header={membershipDetail?.heading}
        onCancelPress={cancelPopup}
        onOkPress={submittingEnquiry}
        onChangeText={(text) => setComment(text)}
        value={comment}
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  imageContainer: {
    width: '100%',
    height: responsiveHeight(120),
    minHeight: 110,
  },
  imageBanner: {
    width: '100%',
    height: responsiveHeight(120),
    minHeight: 110,
    resizeMode: 'cover',
  },
})

export default MembershipDetailsScreen
