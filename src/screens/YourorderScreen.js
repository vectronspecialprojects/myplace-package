import React, {useMemo, useEffect, useState, useCallback} from 'react'
import {View, ActivityIndicator} from 'react-native'
import {WebView} from 'react-native-webview'
import {useDispatch, useSelector} from 'react-redux'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'
import {checkTokenValid} from '../store/actions/authServices'
import {URL} from 'react-native-url-polyfill'

const BASE_YOUR_ORDER = 'https://yomyplace-'
const YouorderScreen = (props) => {
  const [idToken, setValidToken] = useState('')
  const dispatch = useDispatch()
  const dynamicUri = useSelector(
    (state) => state.infoServices.profile.member.current_preferred_venue_full?.your_order_link,
  )
  const profile = useSelector((state) => state.infoServices.profile)
  const appFlags = useSelector((state) => state.app.appFlags)
  const staticUri = props.route.params.params?.special_link
  const qrcodeUri = props.route.params.params?.qrcodeUri

  useEffect(() => {
    dispatch(
      checkTokenValid((value) => {
        setValidToken(value?.id_token)
      }),
    )
  }, [])

  const checkVenueValid = useCallback(() => {
    let valid = false
    const {venue_pivot_tags} = profile?.member.member_tier?.tier?.allowed_venues_tag
    //Case check location
    if (qrcodeUri?.includes(BASE_YOUR_ORDER)) {
      let data = new URL(qrcodeUri?.replace('#!/', ''))
      let location = data.searchParams?.get('location')
      if (location) {
        venue_pivot_tags.some((item) => {
          if (location === item.your_order_location) {
            valid = true
            return true
          }
        })
      }
    } else {
      //Case check your order link
      venue_pivot_tags.some((item) => {
        if (qrcodeUri?.includes(item?.your_order_link)) {
          valid = true
          return true
        }
      })
    }
    if (valid) {
      return `${qrcodeUri}&id_token=${idToken}`
    } else {
      return `${qrcodeUri}`
    }
  }, [qrcodeUri, profile])

  const uri = useMemo(() => {
    if (qrcodeUri) return checkVenueValid()
    if (staticUri) return `${staticUri}&id_token=${idToken}`
    else if (!staticUri && dynamicUri) return `${dynamicUri}&id_token=${idToken}`
    return ''
  }, [dynamicUri, staticUri, qrcodeUri, idToken])

  useEffect(() => {
    if (!uri) {
      Alert.alert(localize('yourOrder.notAvailable'), localize('yourOrder.message'), [
        {text: localize('okay')},
      ])
      props.navigation.goBack()
    }
  }, [uri])

  return (
    <View style={Styles.screen}>
      {!!idToken && (
        <View style={{flex: 1}}>
          <WebView
            startInLoadingState={true}
            source={{uri: uri}}
            renderLoading={() => (
              <View
                style={{
                  backgroundColor: Colors().defaultBackground,
                  position: 'absolute',
                  top: '50%',
                  left: '50%',
                  right: '50%',
                }}>
                <ActivityIndicator size="large" />
              </View>
            )}
          />
        </View>
      )}
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default YouorderScreen
