/**
 * Created by Hong HP on 8/30/21.
 */
import React from 'react'
import {View, StyleSheet, TouchableOpacity, FlatList, Text} from 'react-native'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import Colors from '../../Themes/Colors'
import {deviceWidth, responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import RouteKey from '../../navigation/RouteKey'
import FastImage from 'react-native-fast-image'
import Images from '../../Themes/Images'

function GamingScreen({navigation, route}) {
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={[styles.item, {backgroundColor: Colors().gamingScreen.gameItemBackground}]}
        onPress={() => {
          navigation.navigate(item.route)
        }}>
        <FastImage source={item.image} style={styles.icon} tintColor={Colors().white} resizeMode={'contain'} />
      </TouchableOpacity>
    )
  }
  return (
    <View style={[styles.container, {backgroundColor: Colors().defaultBackground}]}>
      <FlatList
        data={[
          {route: RouteKey.WheelGameScreen, image: Images.wheelGame},
          {route: RouteKey.ScratchGameScreen, image: Images.scratchGame},
        ]}
        style={{marginTop: responsiveHeight(50)}}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        numColumns={2}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: responsiveWidth(24),
  },
  item: {
    marginRight: responsiveWidth(24),
    width: (deviceWidth() - responsiveWidth(72)) / 2,
    aspectRatio: 1,
    borderRadius: responsiveWidth(12),
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: responsiveWidth(90),
    height: responsiveWidth(90),
  },
})

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default GamingScreen
