import React, {useRef, useState} from 'react'
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import ButtonView from '../../components/ButtonView'
import {deviceWidth, responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import WheelOfFortune from '../../components/WheelOfFortune';
import Images from '../../Themes/Images';
const participants = ['10', '20', '30', '40', '50', '60', '70', '90', 'FREE']
function WheelGameScreen() {
  const wheelRef = useRef()

  const [result, setResult] = useState()

  return (
    <View style={styles.container}>
      <View style={styles.result}>
        <Text style={{textAlign: 'center', fontSize: responsiveFont(25)}}>{result?.winnerValue}</Text>
      </View>
      <WheelOfFortune
        options={{
          rewards: participants,
          knobSize: 20,
          borderWidth: 5,
          borderColor: '#000',
          innerRadius: 50,
          duration: 4000,
          backgroundColor: 'transparent',
          textAngle: 'horizontal',
          knobSource: Images.knob,
          onRef: (ref) => (wheelRef.current = ref),
        }}
        getWinner={(value, index) => {
          setResult({winnerValue: value, winnerIndex: index})
        }}
      />
      <ButtonView
        title={'Spin To Win'}
        onPress={() => {
          if (result) {
            wheelRef.current?._tryAgain()
          } else {
            wheelRef.current?._onPress()
          }
        }}
        style={styles.startButton}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  startButton: {
    marginBottom: responsiveHeight(25),
    marginHorizontal: responsiveWidth(12),
  },
  result: {
    position: 'absolute',
    top: responsiveHeight(30),
    width: deviceWidth(),
  },
})

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default WheelGameScreen
