import React, {useState, useCallback, useEffect, useMemo} from 'react'
import {View, FlatList, Text, RefreshControl, Linking, LayoutAnimation} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import AddressCard from './../components/AddressCard'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import MapDetail from './../components/MapDetail'
import VenueTags from './../components/VenueTags'
import RouteKey from '../navigation/RouteKey'
import analytics from '@react-native-firebase/analytics'
import {isTrue, openMaps} from '../utilities/utils'
import Geolocation from 'react-native-geolocation-service'
import Geocoder from '@timwangdev/react-native-geocoder'
import HeaderLeftButton from '../components/HeaderLeftButton'
import {isIOS, responsiveHeight} from '../Themes/Metrics'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const LocationsScreen = (props) => {
  const venues = useSelector((state) => state.infoServices.venues)
  const venueTags = useSelector((state) => state.infoServices.venueTags)
  const venueTagsSetting = useSelector((state) => state.infoServices.venueTagsSetting)
  const venueTagWithVenue = useSelector((state) => state.infoServices.venueTagWithVenue)
  const appFlags = useSelector((state) => state.app.appFlags)
  const title = props.route.params.params?.page_name
  const dispatch = useDispatch()
  const [mapLocation, setMapLocation] = useState()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [selectedVenue, setSelectedVenue] = useState(null)

  const venueTagShow = useMemo(() => {
    let newArr
    if (venueTagWithVenue?.length) {
      newArr = venueTagWithVenue
        ?.filter((item) => !!item.listVenue?.length)
        .sort((a, b) => a?.display_order - b?.display_order)
    } else {
      newArr = venueTags.sort((a, b) => a?.display_order - b?.display_order)
    }
    newArr = [{id: 0, display_order: 0, name: 'All Venues', listVenue: venues}, ...newArr]
    return newArr
  }, [venueTagWithVenue])
  const [selectedTag, setSelectedTag] = useState(venueTagShow[0])

  const verifyPermissions = async () => {
    const result = await Geolocation.requestAuthorization('whenInUse')
    if (result !== 'granted') {
      Alert.alert(localize('location.titlePer'), localize('location.titleMess'), [{text: localize('okay')}])
      return false
    }
    return true
  }

  const venueHandler = async (venue) => {
    if (selectedVenue === venue.id) {
      setSelectedVenue(null)
    } else {
      if (isTrue(appFlags?.app_is_show_map)) {
        let hasPermission = true
        if (isIOS()) {
          hasPermission = await verifyPermissions()
        }
        if (!hasPermission) {
          return
        } else {
          if (venue.longitude && venue?.latitude) {
            setMapLocation({
              lat: +venue?.latitude,
              lng: +venue.longitude,
              name: venue.name,
              address: venue.address,
            })
          } else if (venue.address) {
            try {
              const venuePosition = await Geocoder.geocodeAddress(venue.address, {maxResult: 1, locale: 'us'})
              setMapLocation({
                lat: venuePosition[0]?.position?.lat,
                lng: venuePosition[0]?.position?.lng,
                name: venue.name,
                address: venue.address,
              })
            } catch (err) {
              Alert.alert(localize('location.titleFetch'), localize('location.plsTryAgain'), [{text: 'Okay'}])
            }
          }
          setSelectedVenue(venue.id)
        }
      } else {
        setSelectedVenue(venue.id)
      }
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  const linkHandler = (uri) => {
    Linking.openURL(uri)
  }

  const loadContent = useCallback(async () => {
    setIsRefreshing(true)
    try {
      await dispatch(infoServicesActions.fetchVenues())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
    }
  }, [dispatch])

  useEffect(() => {
    async function firebase() {
      await analytics().logScreenView({screen_name: 'Find us'})
    }

    firebase()
  }, [])

  useEffect(() => {
    if (!isTrue(appFlags?.isMultipleVenue)) venueHandler(venues[0])
  }, [venues, appFlags])

  const callMapHandler = () => {
    props.navigation.navigate(RouteKey.MapScreen, {location: mapLocation})
  }

  const direction = () => {
    const location = {
      latitude: mapLocation.lat,
      longitude: mapLocation.lng,
      provider: 'google',
      start: 'My Location',
      end: `${mapLocation.lat},${mapLocation.lng}`,
    }
    openMaps(location)
  }

  const renderItem = (itemData) => {
    const venue = itemData.item

    return (
      <View>
        {isTrue(appFlags?.isMultipleVenue) && (
          <AddressCard
            imgSource={venue?.image}
            venueName={venue?.name}
            venueAddress={venue?.address}
            onPress={venueHandler.bind(this, venue)}
            distance={venue?.distance}
          />
        )}
        {(!isTrue(appFlags?.isMultipleVenue) || selectedVenue === venue?.id) && (
          <MapDetail
            onMapPress={callMapHandler}
            location={mapLocation}
            address={venue?.address}
            telp={venue?.telp}
            onTelpPress={() => linkHandler(`tel: ${venue?.telp || 0}`)}
            email={venue?.email}
            onEmailPress={linkHandler.bind(this, 'mailto:' + venue?.email)}
            onDirectionsPress={direction}
            openHours={!!venue?.hide_opening_hours_info ? '' : JSON.parse(venue?.open_and_close_hours)}
            isHideOpenHours={venue?.hide_opening_hours_info}
            DeliveryHours={!!venue.hide_delivery_info ? '' : JSON.parse(venue?.pickup_and_delivery_hours)}
            isHideDeliveryHours={venue?.hide_delivery_info}
            socialLinks={JSON.parse(venue?.social_links)}
            menuLinks={JSON.parse(venue?.menu_links)}
          />
        )}
      </View>
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      {isTrue(venueTagsSetting?.isTagEnabled) &&
        isTrue(appFlags?.isMultipleVenue) &&
        !!venueTagShow?.length && (
          <VenueTags
            data={venueTagShow}
            selectedTag={selectedTag?.id}
            onPress={(venueTag) => setSelectedTag(venueTag)}
          />
        )}
      <View style={{flex: 1, marginTop: responsiveHeight(12)}}>
        <FlatList
          data={selectedTag?.listVenue}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
          ListEmptyComponent={
            <Text style={Styles.flatlistNoItems}>No venues found, please check again later.</Text>
          }
          refreshControl={
            <RefreshControl
              refreshing={isRefreshing}
              onRefresh={loadContent}
              tintColor={Colors().defaultRefreshSpinner}
              titleColor={Colors().defaultRefreshSpinner}
              title={localize('pullToRefresh')}
            />
          }
        />
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default LocationsScreen
