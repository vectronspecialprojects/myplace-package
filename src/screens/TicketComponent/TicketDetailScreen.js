import React from 'react'
import {View, StyleSheet, ScrollView, Text} from 'react-native'
import Styles from '../../Themes/Styles'
import SubHeaderBar from '../../components/SubHeaderBar'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import BarcodeBar from '../../components/BarcodeBar'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import Colors from '../../Themes/Colors'
import {localize} from '../../locale/I18nConfig'
import dayjs from 'dayjs'

const TicketType = ['1']

function TicketDetailScreen({route}) {
  const {ticketDetail} = route.params

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('ticket.title')} />
      <View style={{flex: 1, paddingHorizontal: responsiveWidth(20)}}>
        <ScrollView>
          <Text
            style={{
              ...Styles.largeCapBoldText,
              ...{marginTop: responsiveHeight(30), color: Colors().ticket.title},
            }}>
            {ticketDetail.name}
          </Text>
          {ticketDetail.description && (
            <View style={styles.descContainer}>
              <Text style={[Styles.smallCapText, {color: Colors().ticket.description}]}>
                {ticketDetail.description}
              </Text>
            </View>
          )}
          {!!ticketDetail.barcode && (
            <BarcodeBar
              value={ticketDetail.barcode}
              width={responsiveWidth(1.5)}
              title={localize('ticket.lookupNumber')}
              text={ticketDetail.barcode}
              style={{marginTop: responsiveHeight(30)}}
            />
          )}
          <View style={styles.ticketInfoContainer}>
            {ticketDetail.issue_date && (
              <Text style={[Styles.xSmallCapText, {color: Colors().ticket.description}]}>
                Issue Date: {dayjs(ticketDetail.issue_date).format('DD/MM/YYYY')}
              </Text>
            )}
            {ticketDetail.expire_date && (
              <Text style={[Styles.xSmallCapText, {color: Colors().ticket.description}]}>
                Expiry Date: {dayjs(ticketDetail.expire_date).format('DD/MM/YYYY')}
              </Text>
            )}
            {TicketType.includes(ticketDetail?.voucher_type) && (
              <Text style={[Styles.xSmallCapText, {color: Colors().ticket.description}]}>
                Remaining Value:{' '}
                {ticketDetail.amount_left
                  ? '$0'
                  : (ticketDetail.amount_left / 100).toLocaleString('en-US', {
                      style: 'currency',
                      currency: 'USD',
                    })}
              </Text>
            )}
          </View>
          {!!ticketDetail?.member_barcode && (
            <BarcodeBar
              value={ticketDetail.member_barcode}
              width={1.5}
              title={localize('memberNumber')}
              text={ticketDetail.member_barcode}
              style={{marginTop: responsiveHeight(100)}}
            />
          )}
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  ticketInfoContainer: {
    marginTop: responsiveHeight(50),
  },
  descContainer: {
    marginTop: responsiveHeight(30),
  },
})
export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}
export default TicketDetailScreen
