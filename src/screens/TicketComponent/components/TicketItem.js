import React from 'react'
import {View, StyleSheet, ImageBackground, Text} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import Colors from '../../../Themes/Colors'
import {TouchableCmp} from '../../../components/UtilityFunctions'
import dayjs from 'dayjs'

function TicketItem({data, onPress}) {
  return (
    <TouchableCmp onPress={onPress}>
      <View style={styles.container}>
        <ImageBackground source={{uri: data.img}} style={styles.image}>
          <View style={[styles.contentWrapper, {backgroundColor: Colors().opacity}]}>
            <Text style={[styles.title, {color: Colors().ticket.title}]}>{data.name}</Text>
            <Text style={[styles.date, {color: Colors().ticket.date}]}>
              {dayjs(data?.issue_date).format('DD/MM/YYYY')}
            </Text>
          </View>
        </ImageBackground>
      </View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: responsiveHeight(20),
    borderRadius: responsiveWidth(5),
    overflow: 'hidden',
    marginRight: responsiveWidth(20),
  },
  image: {
    width: responsiveWidth(150),
    height: responsiveWidth(150),
  },
  contentWrapper: {
    padding: responsiveWidth(8),
    flex: 1,
    justifyContent: 'space-between',
  },
  title: {
    fontSize: responsiveFont(18),
    fontFamily: Fonts.openSansBold,
    textAlign: 'center',
  },
  date: {
    fontSize: responsiveFont(10),
  },
})

export default TicketItem
