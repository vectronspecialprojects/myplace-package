import React, {useEffect, useState} from 'react'
import {View, StyleSheet, ActivityIndicator} from 'react-native'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import {useSelector} from 'react-redux'
import Styles from '../../Themes/Styles'
import SubHeaderBar from '../../components/SubHeaderBar'
import {WebView} from 'react-native-webview'
import Colors from '../../Themes/Colors'

function ConnectWithUsScreen() {
  const profile = useSelector((state) => state.infoServices.profile)
  const pondHopper = useSelector((state) => state.app.pondHopper)

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={'Connect With Us'} />
      {!!pondHopper?.url && (
        <WebView
          startInLoadingState={true}
          source={{
            uri: pondHopper?.url,
            method: 'POST',
            headers: {'Content-Type': 'text/plain'},
            body: JSON.stringify({
              memberNumber: profile?.member?.bepoz_account_number,
              uniqueKey: pondHopper?.uniqueKey,
              venueId: profile?.member?.current_preferred_venue_full?.bepoz_venue_id,
              action: 'CONTACT',
            }),
          }}
          renderLoading={() => (
            <View style={[styles.loadingContainer, {backgroundColor: Colors().defaultBackground}]}>
              <ActivityIndicator size="large" />
            </View>
          )}
        />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loadingContainer: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    right: '50%',
  },
})

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}
export default ConnectWithUsScreen
