import React, {useState, useEffect, useCallback, useMemo} from 'react'
import {View, Text, RefreshControl, FlatList} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'
import {responsiveHeight} from '../Themes/Metrics'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import * as infoServicesActions from '../store/actions/infoServices'
import SubHeaderBar from './../components/SubHeaderBar'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import TabsBar from './../components/TabsBar'
import CardItem from './../components/CardItem'
import Filter from './../components/Filter'
import RouteKey from '../navigation/RouteKey'
import {Card} from '../modals/modals'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const WhatsonScreen = (props) => {
  const pages = props?.route?.params?.params?.pages
  const title = props?.route?.params?.params?.page_name
  const dispatch = useDispatch()
  const [isRefreshing, setIsRefreshing] = useState(false)
  const [selectedPage, setSelectedPage] = useState(+pages[0]?.listing_type_id || 2)
  const [isFilter, setIsFilter] = useState(false)
  const [selectedId, setSelectedId] = useState(useSelector((state) => state.infoServices.preferredVenueId))
  const specialEvents = useSelector((state) => state.infoServices.specialEvents)
  const regularEvents = useSelector((state) => state.infoServices.regularEvents)
  const internetState = useSelector((state) => state.app.internetState)
  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetctListings(1))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    try {
      await dispatch(infoServicesActions.fetctListings(2))
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    setIsRefreshing(false)
    dispatch(setGlobalIndicatorVisibility(false))
  }, [dispatch])

  useEffect(() => {
    if (internetState) {
      dispatch(setGlobalIndicatorVisibility(true))
      loadContent()
    }
  }, [dispatch, loadContent])

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      if (internetState) {
        dispatch(setGlobalIndicatorVisibility(true))
        loadContent()
      }
    })
    return () => {
      unsubscribe()
    }
  }, [loadContent])

  const tabsHandler = (id) => {
    if (selectedPage !== id) {
      setSelectedPage(+id)
    }
  }

  const filterHandler = (venue) => {
    setSelectedId(venue?.id)
    setIsFilter(false)
  }

  const showData = useMemo(() => {
    if (regularEvents !== undefined && specialEvents !== undefined) {
      if (selectedPage === 2) {
        if (selectedId === 0)
          return regularEvents?.sort((a, b) => {
            return a?.listing?.display_order - b?.listing?.display_order
          })
        return regularEvents
          ?.filter((data) => data?.listing?.venue?.id === 0 || data?.listing?.venue?.id === selectedId)
          ?.sort((a, b) => {
            return a?.listing?.display_order - b?.listing?.display_order
          })
      }
      if (selectedId === 0)
        return specialEvents?.sort((a, b) => {
          return a?.listing?.display_order - b?.listing?.display_order
        })
      return specialEvents
        ?.filter((data) => data?.listing?.venue.id === 0 || data?.listing?.venue?.id === selectedId)
        ?.sort((a, b) => {
          return a?.listing?.display_order - b?.listing?.display_order
        })
    }
  }, [regularEvents, specialEvents, selectedPage, selectedId])

  const renderItem = (itemData) => {
    const listing = itemData.item?.listing
    const cardDetail = new Card(
      listing?.heading,
      listing?.desc_short,
      listing?.payload,
      0,
      0,
      listing?.image_banner,
    )
    return (
      <CardItem
        style={{marginBottom: responsiveHeight(5)}}
        cardDetail={cardDetail}
        titleStyle={{color: Colors().whaton.titleColor}}
        onPress={() => {
          props.navigation.navigate(RouteKey.WhatsonDetailsScreen, {listing})
        }}
      />
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar
        title={title}
        filterBtn={true}
        onFilterPress={() => {
          setIsFilter(true)
        }}
      />
      <Filter
        isVisible={isFilter}
        backScreenOnPress={() => {
          setIsFilter(false)
        }}
        selectedId={selectedId}
        onFilterPress={filterHandler}
      />
      <TabsBar isShadow={true} pages={pages} onPress={tabsHandler} selectedPageId={selectedPage} />
      <FlatList
        data={showData}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        ListEmptyComponent={
          <Text style={Styles.flatlistNoItems}>No Events found, please check again later.</Text>
        }
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              loadContent()
              setIsRefreshing(true)
            }}
            tintColor={Colors().defaultRefreshSpinner}
            titleColor={Colors().defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default WhatsonScreen
