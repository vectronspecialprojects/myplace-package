import React, {useState, useMemo} from 'react'
import {View, Text, StyleSheet, FlatList, TouchableOpacity, LayoutAnimation} from 'react-native'
import {useSelector} from 'react-redux'
import Modal from 'react-native-modal'
import {responsiveHeight, responsiveWidth, responsiveFont} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import Ionicons from 'react-native-vector-icons/Ionicons'
import {isTrue} from '../utilities/utils'

const Filter = ({showAllVenue = true, ...props}) => {
  const [selectedTag, setSelectedTag] = useState(null)
  const [isOpen, setIsOpen] = useState(false)
  const venues = useSelector((state) => state.infoServices.venues)
  const venueTagWithVenue = useSelector((state) => state.infoServices.venueTagWithVenue)
  const appFlags = useSelector((state) => state.app.appFlags)

  const tagsHandler = (tag) => {
    if (selectedTag === tag.id) {
      setIsOpen(false)
      setSelectedTag(null)
    } else {
      setIsOpen(true)
      setSelectedTag(tag.id)
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }

  const showData = useMemo(() => {
    if (isTrue(appFlags?.app_is_group_filter)) {
      let venuesList = []
      venues.forEach((venue) => {
        if (venue.venue_pivot_tags.filter((tag) => tag.venue_tag_id === selectedTag).length > 0)
          venuesList = venuesList.concat(venue)
      })
      return venuesList
    }
    if (showAllVenue) {
      return venues.concat({id: 0, name: 'All Venues'}).sort((a, b) => a.id - b.id)
    }
    return venues.sort((a, b) => a.id - b.id)
  }, [selectedTag, venues, appFlags, showAllVenue])

  const renderItem = (itemData) => {
    return (
      <TouchableOpacity onPress={props.onFilterPress.bind(this, itemData.item)}>
        <View style={styles.listWrapper}>
          {props.selectedId === itemData.item.id && (
            <Ionicons name="ios-checkmark" size={24} color={Colors().defaultFilterText} />
          )}
          <Text style={[styles.list, {color: Colors().defaultFilterText}]}>{itemData.item.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  const renderGroupItem = (itemData) => {
    return (
      <View>
        <TouchableOpacity onPress={tagsHandler.bind(this, itemData.item)}>
          <View style={styles.listWrapper}>
            <Text style={[styles.list, {color: Colors().defaultFilterText}]}>{itemData.item.name}</Text>
          </View>
        </TouchableOpacity>
        {selectedTag === itemData.item.id && isOpen && (
          <TouchableOpacity style={{flex: 1}} onPress={props.backScreenOnPress}>
            <FlatList
              style={{
                paddingLeft: isTrue(appFlags?.app_is_group_filter) ? responsiveWidth(10) : 0,
              }}
              data={itemData?.item?.listVenue}
              renderItem={renderItem}
              keyExtractor={(item) => item.id.toString()}
            />
          </TouchableOpacity>
        )}
      </View>
    )
  }

  const venueTagShow = useMemo(() => {
    return venueTagWithVenue.filter((item) => !!item.listVenue?.length)
  }, [venueTagWithVenue])

  if (isTrue(appFlags?.app_is_group_filter)) {
    return (
      <Modal
        backdropOpacity={0.2}
        isVisible={props.isVisible}
        animationIn="slideInDown"
        animationOut="slideOutRight">
        <TouchableOpacity style={{flex: 1}} onPress={props.backScreenOnPress}>
          <View style={[styles.container, {backgroundColor: Colors().defaultFilterBackground}]}>
            {showAllVenue && (
              <TouchableOpacity onPress={props.onFilterPress.bind(this, {id: 0})}>
                <View style={styles.listWrapper}>
                  {props.selectedId === 0 && (
                    <Ionicons name="ios-checkmark" size={24} color={Colors().defaultFilterText} />
                  )}
                  <Text style={[styles.list, {color: Colors().defaultFilterText}]}>All Venues</Text>
                </View>
              </TouchableOpacity>
            )}
            <FlatList
              data={venueTagShow}
              renderItem={renderGroupItem}
              keyExtractor={(item) => item.id.toString()}
            />
          </View>
        </TouchableOpacity>
      </Modal>
    )
  }

  return (
    <Modal
      backdropOpacity={0.2}
      isVisible={props.isVisible}
      animationIn="slideInDown"
      animationOut="slideOutRight">
      <TouchableOpacity style={{flex: 1}} onPress={props.backScreenOnPress}>
        <View style={[styles.container, {backgroundColor: Colors().defaultFilterBackground}]}>
          <FlatList data={showData} renderItem={renderItem} keyExtractor={(item) => item.id.toString()} />
        </View>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    width: responsiveWidth(200),
    minHeight: responsiveHeight(100),
    maxHeight: responsiveHeight(250),
    marginTop: responsiveHeight(110),
    marginRight: responsiveWidth(5),
    paddingLeft: responsiveWidth(20),
    paddingVertical: responsiveWidth(10),
    alignSelf: 'flex-end',
    borderRadius: responsiveWidth(20),
  },
  listWrapper: {
    flexDirection: 'row',
    marginBottom: responsiveHeight(5),
    alignItems: 'center',
  },
  list: {
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(14),
    marginLeft: responsiveWidth(10),
  },
})

export default Filter
