import React from 'react'
import {View, StyleSheet} from 'react-native'
import TextInputView from './TextInputView'
import {localize} from '../locale/I18nConfig'
import Colors from '../Themes/Colors'
import Dropdown from './Dropdown'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import DateInputView from './DateInputView'
import {useSelector} from 'react-redux'
import Fonts from '../Themes/Fonts'

function ProfileFieldInput({
  setShowSelectVenue,
  formState,
  type,
  inputChangeHandler,
  dropDownTier,
  item,
  customFields,
  emailNotAllowedDomains,
}) {
  const useLegacyDesign = useSelector(state => state.app.useLegacyDesign)
  switch (type) {
    case 'Venue':
      return (
        <TextInputView
          title={item?.fieldDisplayTitle}
          onPress={() => setShowSelectVenue(true)}
          placeholder={localize('signUp.preferredVenuePlaceholder')}
          editable={false}
          backgroundDisabled={Colors().defaultTextInputBackgound}
          value={formState?.inputValues?.venue_id?.label}
        />
      )
    case 'Tier':
      return (
        <Dropdown
          id="tier"
          required
          onInputChange={inputChangeHandler}
          label={localize('signUp.tier')}
          placeholder={localize('signUp.tierPlaceholder')}
          items={dropDownTier}
          initValue={formState.inputValues?.[item.id]}
          titleStyle={
            !useLegacyDesign && {
              ...styles.titleEdge,
              top: responsiveHeight(-7),
              marginVertical: 0,
            }
          }
          style={{borderRadius: responsiveHeight(24)}}
          containerStyle={!useLegacyDesign && styles.border}
        />
      )
    case 'Dropdown':
      return (
        <Dropdown
          id={item?.id}
          placeholder={item?.fieldDisplayTitle}
          required
          onInputChange={inputChangeHandler}
          label={item?.fieldDisplayTitle}
          items={customFields(item?.multiValues)}
          initValue={formState.inputValues?.[item.id]}
          titleStyle={
            !useLegacyDesign && {
              ...styles.titleEdge,
              top: responsiveHeight(-7),
              marginVertical: 0,
            }
          }
          style={{borderRadius: responsiveHeight(24)}}
          containerStyle={!useLegacyDesign && styles.border}
        />
      )
    case 'Text':
    case 'Number':
      return (
        <TextInputView
          id={item?.id}
          title={item?.fieldDisplayTitle}
          keyboardType={item.keyboardType}
          required
          email={item?.id?.includes('email')}
          emailAvailability={item?.id?.includes('email')}
          emailNotAllowedDomains={item?.id?.includes('email') ? emailNotAllowedDomains : null}
          autoCapitalize="words"
          placeholder={item?.fieldDisplayTitle}
          onChangeText={inputChangeHandler}
          initialValue=""
          placeholderTextColor={Colors().signUp.placeholderTextColor}
          errorText={`Please enter a valid ${item?.fieldName}.`}
          value={formState.inputValues?.[item?.id]}
          minLength={+item.minLength || 0}
          maxLength={+item.maxLength || 999}
        />
      )
    case 'Password':
      return (
        <View>
          <TextInputView
            id="password"
            title={localize('signUp.password')}
            placeholder={localize('signUp.password')}
            keyboardType="default"
            secureTextEntry
            required
            password
            autoCapitalize="none"
            errorText={localize('signUp.errPassword')}
            onChangeText={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors().signUp.placeholderTextColor}
            value={formState.inputValues.password}
          />
          <TextInputView
            id="password_confirmation"
            title={localize('signUp.confirmPassword')}
            placeholder={localize('signUp.confirmPassword')}
            keyboardType="default"
            secureTextEntry
            required
            resetEvent={formState.inputValues?.password?.length <= 1}
            confirmPassword
            crossCheckPassword={formState.inputValues.password}
            autoCapitalize="none"
            errorText={localize('signUp.confirmPassErr')}
            onChangeText={inputChangeHandler}
            initialValue=""
            placeholderTextColor={Colors().signUp.placeholderTextColor}
            value={formState.inputValues.password_confirmation}
          />
        </View>
      )
    case 'Date':
      return (
        <DateInputView
          id={item?.id}
          required
          onInputChange={inputChangeHandler}
          label={item?.fieldDisplayTitle}
          style={{
            backgroundColor: Colors().signUp.fieldBackground,
          }}
          errorText={`Please enter a valid ${item?.fieldName}.`}
        />
      )
    default:
      return (
        <TextInputView
          id={item?.id}
          title={item?.fieldDisplayTitle}
          keyboardType={item.keyboardType}
          required
          email={item?.id?.includes('email')}
          emailAvailability={item?.id?.includes('email')}
          emailNotAllowedDomains={item?.id?.includes('email') ? emailNotAllowedDomains : null}
          autoCapitalize="words"
          placeholder={item?.fieldDisplayTitle}
          onChangeText={inputChangeHandler}
          initialValue=""
          placeholderTextColor={Colors().signUp.placeholderTextColor}
          errorText={`Please enter a valid ${item?.fieldName}.`}
          value={formState.inputValues?.[item?.id]}
          minLength={+item.minLength || 0}
          maxLength={+item.maxLength || 999}
        />
      )
  }
}

const styles = StyleSheet.create({
  container: {},
  border: {
    borderRadius: responsiveHeight(24),
    marginBottom: responsiveHeight(10),
    marginTop: responsiveHeight(10),
  },
  titleEdge: {
    position: 'absolute',
    zIndex: 99,
    fontSize: responsiveFont(11),
    left: responsiveWidth(20),
    top: responsiveHeight(0),
    fontFamily: Fonts.openSans,
  },
})
export default ProfileFieldInput
