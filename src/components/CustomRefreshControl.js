import React from 'react'
import {View, StyleSheet, RefreshControl} from 'react-native'
import Colors from '../Themes/Colors'
import {localize} from '../locale/I18nConfig'
import {useSelector} from 'react-redux'

function CustomRefreshControl({isRefreshing, setIsRefreshing, loadContent, internetState}) {
  // const internetState = useSelector(state => state.app.internetState)
  return (
    <RefreshControl
      refreshing={isRefreshing}
      onRefresh={() => {
        if (internetState) {
          setIsRefreshing(true)
          loadContent()
        }
      }}
      tintColor={Colors().defaultRefreshSpinner}
      titleColor={Colors().defaultRefreshSpinner}
      title={localize('pullToRefresh')}
    />
  )
}
export default CustomRefreshControl
