import React, {useEffect, useImperativeHandle, useState} from 'react'
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native'
import Images from '../Themes/Images'
import BottomSheet from './BottomSheet'
import Colors from '../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import fonts from '../Themes/Fonts'
import FastImage from 'react-native-fast-image'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import ImagePicker from 'react-native-image-crop-picker'

const Avatar = React.forwardRef(({size, uri, style, onImageSelect, editable}, ref) => {
  const [loadError, setLoadError] = useState(false)
  const [showOption, setShowOption] = useState(false)

  useImperativeHandle(ref, () => ({
    showOption: (value) => setShowOption(value),
  }))

  useEffect(() => {
    if (loadError) setLoadError(false)
  }, [uri])

  const pickImage = async () => {
    setTimeout(async () => {
      try {
        let result = await ImagePicker.openPicker({
          mediaType: 'photo',
          cropping: true,
          width: 400,
          height: 300,
          compressImageQuality: 0.5,
        })
        onImageSelect?.(result)
      } catch (e) {
        console.log(e)
      }
    }, 1000)
  }

  const openCamera = async () => {
    setTimeout(async () => {
      let result = await ImagePicker.openCamera({
        mediaType: 'photo',
        cropping: true,
        width: 400,
        height: 300,
        compressImageQuality: 0.5,
      })
      if (!result.cancelled) {
        onImageSelect?.(result)
      }
    }, 1000)
  }

  return (
    <View
      style={[
        styles.container,
        {
          width: size,
          height: size,
          borderRadius: size / 2,
        },
        style,
      ]}>
      {(loadError || editable) && (
        <TouchableOpacity
          style={[styles.buttonAddImage, {backgroundColor: Colors().gray}]}
          onPress={() => {
            setShowOption(true)
          }}>
          <MaterialIcons name={'edit'} size={25} color={Colors().white} />
        </TouchableOpacity>
      )}
      <View style={{width: size, height: size, borderRadius: size / 2, overflow: 'hidden'}}>
        <FastImage
          source={!!uri && !loadError ? {uri: uri} : Images.defaultImage}
          style={{width: size, height: size}}
          resizeMode={'cover'}
          onError={() => setLoadError(true)}
        />
      </View>
      <BottomSheet
        visible={showOption}
        onClose={() => setShowOption(false)}
        confirmButtonTitle={'Cancel'}
        onConfirm={() => setShowOption(false)}>
        <TouchableOpacity
          style={[styles.buttonOption, {borderBottomWidth: 0.5}]}
          onPress={() => {
            setShowOption(false)
            openCamera()
          }}>
          <Text style={styles.buttonTitle}>Open Camera</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonOption}
          onPress={() => {
            setShowOption(false)
            pickImage()
          }}>
          <Text style={styles.buttonTitle}>Select from the Gallery</Text>
        </TouchableOpacity>
      </BottomSheet>
    </View>
  )
})

export default Avatar

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonAddImage: {
    position: 'absolute',
    zIndex: 999,
    right: 0,
    top: 0,
    width: responsiveWidth(30),
    height: responsiveWidth(30),
    borderRadius: responsiveWidth(15),
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonOption: {
    height: responsiveHeight(50),
    minHeight: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonTitle: {
    fontSize: responsiveFont(17),
    fontFamily: fonts.openSansBold,
  },
})
