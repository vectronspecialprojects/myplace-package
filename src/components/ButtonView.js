import {StyleSheet, Text, View} from 'react-native'
import React from 'react'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {TouchableCmp} from './UtilityFunctions'
import {responsiveFont, responsiveHeight, responsiveWidth, shadow} from '../Themes/Metrics'
import {useSelector} from 'react-redux'

export default function ButtonView({
  title,
  desc,
  style,
  onPress,
  disabled,
  hasIcon,
  titleStyle,
  descStyle,
  children,
  wrapper,
  leftIcon,
  rightIcon,
  isShadow
}) {
  const formInputStyle = useSelector((state) => state.app.formInputStyle)
  const useLegacyDesign = useSelector((state) => state.app.useLegacyDesign)
  return (
    <View
      style={[
        styles.container,
        {backgroundColor: Colors().dufaultButtonBackground},
        styles[formInputStyle],
        style,
        isShadow && {...shadow},
        disabled && {opacity: 0.5},
        useLegacyDesign && {height: responsiveHeight(44), borderRadius: responsiveHeight(22)}
      ]}>
      <TouchableCmp style={{flex: 1}} disabled={disabled} onPress={onPress}>
        <View style={[styles.textWrapper, wrapper]}>
          {!!leftIcon && leftIcon}
          <View style={{flex: 1, paddingHorizontal: responsiveWidth(20)}}>
            {!!title && (
              <Text
                style={[
                  styles.titleStyle,
                  {color: Colors().defaultButtonText},
                  !hasIcon && {textAlign: 'center'},
                  titleStyle
                ]}>
                {title}
              </Text>
            )}
            {desc && (
              <Text
                style={[
                  styles.descStyle,
                  {color: Colors().defaultButtonText},
                  !hasIcon && {textAlign: 'center'},
                  descStyle
                ]}>
                {desc}
              </Text>
            )}
          </View>
          {!!rightIcon && rightIcon}
          {children}
        </View>
      </TouchableCmp>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: responsiveHeight(38),
    borderRadius: responsiveHeight(19),
    overflow: 'hidden'
  },
  rounded: {
    borderRadius: responsiveHeight(19)
  },
  rectangle: {
    borderRadius: responsiveHeight(3)
  },
  textWrapper: {
    flex: 1,
    height: responsiveHeight(38),
    borderRadius: 12,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  wrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: responsiveWidth(16)
  },
  titleStyle: {
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(15)
  },
  descStyle: {
    fontFamily: Fonts.openSans,
    fontSize: responsiveFont(10)
  }
})
