import React from 'react'
import {ImageBackground, View, Text, StyleSheet} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Styles from '../Themes/Styles'
import {TouchableCmp} from './UtilityFunctions'
import Colors from '../Themes/Colors'
import dayjs from 'dayjs'

const VoucherTile = (props) => {
  return (
    <ImageBackground
      style={styles.gridItem}
      resizeMode="contain"
      source={{uri: props.imgSource || 'https://s3-ap-southeast-2.amazonaws.com/bepoz-loyalty-app/000.png'}}>
      <TouchableCmp
        activeOpacity={0.6}
        style={{flex: 1}}
        onPress={props.onPress}
        disabled={props?.isInProgress}>
        <View style={[styles.container, {backgroundColor: Colors().opacity}]}>
          <Text style={[Styles.largeCapBoldText, {color: Colors().voucher.title}]}>{props.title}</Text>
          {props.isInProgress && (
            <Text style={[Styles.largeCapText, {fontSize: responsiveFont(16)}]}>In Progress</Text>
          )}
          {props.expiredDate && (
            <View style={styles.expiredContainer}>
              <Text
                style={[Styles.x2SmallNormalText, {alignSelf: 'flex-start'}, {color: Colors().voucher.date}]}>
                {dayjs(props.expiredDate).format('DD/MM/YYYY')}
              </Text>
            </View>
          )}
        </View>
      </TouchableCmp>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  gridItem: {
    width: responsiveWidth(150),
    aspectRatio: 1,
    marginTop: responsiveHeight(20),
    borderRadius: responsiveWidth(5),
    overflow: 'hidden',
  },
  container: {
    flex: 1,
    borderRadius: responsiveWidth(5),
    paddingHorizontal: responsiveWidth(10),
    paddingTop: '30%',
    alignItems: 'center',
  },
  expiredContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    width: '100%',
    paddingBottom: responsiveHeight(5),
  },
})

export default VoucherTile
