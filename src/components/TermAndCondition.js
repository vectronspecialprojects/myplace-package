import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import {useSelector} from 'react-redux'
import HtmlTextPopup from './HtmlTextPopup'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import CheckBox from './CheckBox'
import Fonts from '../Themes/Fonts'

function TermAndCondition({onCheck, value, allowCheck}) {
  const [showData, setShowData] = useState({header: '', html: ''})
  const legals = useSelector(state => state.infoServices.legals)
  const [isPopupVisible, setIsPopupVisible] = useState(false)
  const appFlags = useSelector(state => state.app.appFlags)

  const legalsPopup = cases => {
    setIsPopupVisible(true)
    if (cases === 'termAndCondition') {
      setShowData({header: legals[1][0], html: legals[1][1]})
    } else if (cases === 'policy') {
      setShowData({header: legals[0][0], html: legals[0][1]})
    }
  }

  return (
    <View style={styles.container}>
      {allowCheck && <CheckBox size={responsiveHeight(35)} value={value} onPress={onCheck} />}
      <View style={[styles.termContainer, !allowCheck && {paddingHorizontal: responsiveWidth(20)}]}>
        <Text style={[Styles.xSmallNormalText, {textAlign: 'left'}]}>
          <Text>
            Yes, I am aged 18 years + and have read, understand, and agree to {appFlags?.app_name}'s{' '}
          </Text>
          <Text
            onPress={() => {
              legalsPopup('termAndCondition')
            }}
            style={{color: Colors().termAndConditionTextColor, fontFamily: Fonts.openSansBold}}>
            Term & Conditions
          </Text>
          <Text> and </Text>
          <Text
            onPress={() => {
              legalsPopup('policy')
            }}
            style={{color: Colors().termAndConditionTextColor, fontFamily: Fonts.openSansBold}}>
            Privacy Policy.
          </Text>{' '}
          These can be found within the Legal Stuff section of the App menu
          {/*<Text*/}
          {/*  style={{color: Colors().second, textDecorationLine: 'underline'}}*/}
          {/*  onPress={() => {*/}
          {/*    Linking.openURL('www.publinc.com.au.')*/}
          {/*  }}>*/}
          {/*  www.publinc.com.au.*/}
          {/*</Text>*/}
        </Text>
      </View>
      <HtmlTextPopup
        isVisible={isPopupVisible}
        header={showData.header}
        html={showData.html}
        onOkPress={() => setIsPopupVisible(false)}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  termContainer: {
    alignItems: 'center',
    paddingRight: responsiveWidth(20),
  },
})

export default TermAndCondition
