import React, {useMemo} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {TouchableCmp} from './UtilityFunctions'
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {useSelector} from 'react-redux'
import {isTrue} from '../utilities/utils'

const SubHeaderBar = (props) => {
  const {filterBtn, title} = props
  const appFlags = useSelector((state) => state.app.appFlags)

  const showFilter = useMemo(() => {
    return filterBtn && isTrue(appFlags?.isMultipleVenue)
  }, [appFlags, filterBtn])

  if (isTrue(appFlags?.app_sub_header_align_left)) {
    return (
      <View style={{flexDirection: 'row', height: responsiveHeight(44), alignItems: 'center'}}>
        <Text style={[styles.title, {color: Colors().second}]}>{title}</Text>
        {showFilter && (
          <View style={[styles.iconContainer, {borderColor: Colors().second}]}>
            <TouchableCmp onPress={props.onFilterPress} style={{flex: 1}}>
              <View style={styles.wrapper}>
                <FontAwesome5Pro
                  name="filter"
                  size={responsiveFont(15)}
                  color={Colors().second}
                  activeOpacity={0.6}
                />
              </View>
            </TouchableCmp>
          </View>
        )}
      </View>
    )
  }

  return (
    <View style={{...styles.container, backgroundColor: Colors().subHeaderBackground, ...props.style}}>
      <Text style={{...Styles.mediumBoldText, ...{color: Colors().pageTitle}}}>{title}</Text>
      {showFilter && (
        <View style={styles.iconContainer}>
          <TouchableCmp onPress={props.onFilterPress} style={{flex: 1}}>
            <View style={styles.wrapper}>
              <FontAwesome5Pro
                name="filter"
                size={responsiveFont(15)}
                color={Colors().defaultFilterIcon}
                activeOpacity={0.6}
              />
            </View>
          </TouchableCmp>
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: responsiveHeight(44),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconContainer: {
    position: 'absolute',
    right: responsiveWidth(15),
    borderRadius: 5,
    borderWidth: 0,
    overflow: 'hidden',
  },
  wrapper: {
    padding: 5,
  },
  title: {
    fontSize: responsiveFont(20),
    fontFamily: Fonts.openSansBold,
    marginTop: responsiveHeight(5),
    marginLeft: responsiveHeight(20),
  },
})

export default SubHeaderBar
