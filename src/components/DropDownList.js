import React, {useMemo, useState} from 'react'
import {Text, StyleSheet, View, TouchableOpacity, FlatList, Platform, Modal} from 'react-native'
import TextInputView from './TextInputView'
import {
  deviceHeight,
  deviceWidth,
  hitSlop,
  isIOS,
  responsiveFont,
  responsiveHeight,
  responsiveWidth,
} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import {Picker} from '@react-native-picker/picker'

export default function DropDownList({
  visible,
  onClose,
  data,
  hasSearchBar,
  onSelect,
  value,
  modalTitle,
  style,
  textStyle,
  height = deviceHeight(),
  showValue,
  listEmptyText = '',
  children,
  placeholder,
}) {
  const [keyWord, setKeyword] = useState('')

  const filterList = useMemo(() => {
    if (!keyWord) {
      return data
    } else {
      let listGroup = []
      const formatKeyword = keyWord.trim().toLowerCase()
      listGroup = data.filter((item) => {
        const selected = formatShowValue(item)
        return selected.toLowerCase().includes(formatKeyword)
      })
      return listGroup
    }
  }, [keyWord, data])

  function formatShowValue(item) {
    if (typeof showValue === 'string') return item[showValue]
    else if (Array.isArray(showValue)) return showValue.map((value) => item[value]).join(', ')
    else return item
  }

  const renderItem = ({item, index}) => {
    const selected = formatShowValue(item)

    return (
      <TouchableOpacity
        key={item.value}
        style={[styles.itemContainer, {backgroundColor: Colors().white}]}
        onPress={() => {
          onSelect(item, true)
          onClose()
          setKeyword('')
        }}>
        <Text
          style={{
            fontFamily: value === selected ? Fonts.openSansBold : Fonts.openSans,
            fontSize: responsiveFont(15),
            marginVertical: 2,
            flex: 1,
          }}>
          {selected}
        </Text>
        {value === selected && <MaterialIcons name={'check'} size={22} color={'#62CAA4'} />}
      </TouchableOpacity>
    )
  }

  function renderListForIOS() {
    return (
      <View>
        <Picker
          selectedValue={value}
          onValueChange={(itemValue, itemIndex) => {
            if (itemIndex === 0) {
              onSelect(null, false)
            } else {
              onSelect(filterList[itemIndex - 1], true)
            }
          }}>
          {[{[showValue]: placeholder, value: 'none'}, ...filterList]?.map((item) => {
            const selected = formatShowValue(item)
            return <Picker.Item label={selected} value={item.value} key={item.value} />
          })}
        </Picker>
      </View>
    )
  }

  function renderListForAndroid() {
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        bounces={false}
        keyExtractor={(item, index) => index.toString()}
        keyboardShouldPersistTaps={'handled'}
        data={filterList}
        renderItem={renderItem}
        ItemSeparatorComponent={() => (
          <View style={{height: 0.5, backgroundColor: Colors().defaultBackground}} />
        )}
        ListEmptyComponent={
          <Text style={{fontSize: responsiveFont(14), marginTop: responsiveHeight(20)}}>{listEmptyText}</Text>
        }
      />
    )
  }

  return (
    <Modal visible={visible} animationType={'slide'} transparent={true} onRequestClose={onClose}>
      <TouchableOpacity
        onPress={() => {
          // onClose()
          // setKeyword('')
        }}
        style={styles.container}
        activeOpacity={1}>
        <View style={{...styles.wrapper, backgroundColor: Colors().white, height}}>
          {modalTitle && (
            <View style={[styles.titleWrapper, {backgroundColor: Colors().white}]}>
              {!isIOS() && (
                <TouchableOpacity
                  hitSlop={hitSlop}
                  onPress={() => {
                    setKeyword('')
                    onClose()
                  }}
                  style={{position: 'absolute', zIndex: 999}}>
                  <MaterialIcons name={'close'} size={30} color={Colors().gray} />
                </TouchableOpacity>
              )}
              <Text style={styles.textSemi}>{modalTitle}</Text>
              {isIOS() && (
                <TouchableOpacity
                  hitSlop={hitSlop}
                  onPress={() => {
                    setKeyword('')
                    onClose()
                  }}
                  style={{position: 'absolute', zIndex: 999, right: 0}}>
                  <Text
                    style={{
                      color: Colors().btnDone,
                      fontFamily: Fonts.openSansBold,
                      fontSize: responsiveFont(15),
                    }}>
                    Done
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          )}
          {hasSearchBar && (
            <TextInputView
              placeholder={'Search'}
              inputStyle={{
                backgroundColor: Colors().white,
                borderWidth: StyleSheet.hairlineWidth,
                borderRadius: 4,
              }}
              placeholderTextColor={Colors().black}
              value={keyWord}
              onChangeText={(text) => setKeyword(text)}
              rightIcon={<MaterialIcons name={'search'} size={20} color={Colors().white} />}
              textInputStyle={{
                backgroundColor: Colors().white,
                color: Colors().black,
                fontFamily: Fonts.openSans,
              }}
            />
          )}
          {!!children ? children : isIOS() ? renderListForIOS() : renderListForAndroid()}
        </View>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  wrapper: {
    paddingBottom: responsiveHeight(25),
    borderTopLeftRadius: responsiveWidth(20),
    borderTopRightRadius: responsiveWidth(20),
    paddingHorizontal: responsiveWidth(15),
    borderRadius: 5,
  },
  titleWrapper: {
    paddingVertical: responsiveHeight(10),
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: responsiveHeight(10),
    flexDirection: 'row',
  },
  textSemi: {
    fontSize: responsiveFont(18),
    textAlign: 'center',
    flex: 1,
  },
  itemContainer: {
    minHeight: 40,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: responsiveWidth(10),
  },
})
