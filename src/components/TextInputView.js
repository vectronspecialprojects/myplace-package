import React, {useEffect, useState} from 'react'
import {View, StyleSheet} from 'react-native'
import {FloatTitleTextInput} from './FloatTitleTextInput'
import {apiCheckEmail} from '../utilities/ApiManage'
import {responsiveHeight} from '../Themes/Metrics'
import {useSelector} from 'react-redux'

function TextInputView({
  value,
  title,
  onChangeText,
  keyboardType,
  messageError,
  secureTextEntry,
  icon,
  onPress,
  style,
  messageInfo,
  countryCodeComponent,
  countryCode,
  onSelectCountry,
  placeholder,
  onSubmitEditing,
  titleStyle,
  isRequire,
  editable = true,
  inputStyle,
  multiline = false,
  rightIcon,
  textInputStyle,
  maxLength,
  onFocus,
  onBlur,
  leftIcon,
  floatTitle,
  placeholderTextInput,
  blurOnSubmit,
  onEndEditing,
  numberOfLines,
  scrollEnabled,
  autoFocus,
  placeholderTextColor,
  pointerEvents,
  textAlignVertical,
  id,
  emailAvailability,
  required,
  email,
  alphabet,
  minLength,
  min,
  max,
  emailNotAllowedDomains,
  crossCheckPassword,
  confirmPassword,
  resetEvent,
  errorText,
  backgroundDisabled,
}) {
  const TextInput = FloatTitleTextInput
  const [isEmailAvailable, setEmailAvailable] = useState(true)
  const [isValid, setValid] = useState(true)
  const formInputStyle = useSelector(state => state.app.formInputStyle)

  useEffect(() => {
    if (resetEvent) {
      onChangeText?.(id, '', false)
      setValid(false)
    }
  }, [resetEvent])

  function handleChangeText(text) {
    if (!id) {
      onChangeText?.(text)
    } else {
      const emailRegex =
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      const alphabetRegex = /^[a-zA-Z]+$/
      const passwordRegex = /^(?=.*\d)(?=.*[a-zA-Z!@#$&])[a-zA-Z0-9!@#$&]{6,}$/
      let valid = true
      if (required && text.trim().length === 0) {
        valid = false
      } else if (email && !emailRegex.test(text.toLowerCase())) {
        valid = false
      } else if (alphabet && !alphabetRegex.test(text)) {
        valid = false
      } else if (secureTextEntry && !passwordRegex.test(text.toLowerCase())) {
        valid = false
      } else if (confirmPassword && text !== crossCheckPassword) {
        valid = false
      } else if (min && +text < min) {
        valid = false
      } else if (max && +text > max) {
        valid = false
      } else if (minLength != null && text.length < minLength) {
        valid = false
      } else if (email && !!emailNotAllowedDomains && emailNotAllowedDomains.test(text)) {
        valid = false
      }
      onChangeText?.(id, text, valid)
      setValid(valid)
    }
  }

  async function handleSubmitText() {
    if (id) {
      let valid = isValid
      if (emailAvailability) {
        const res = await apiCheckEmail(value)
        setEmailAvailable(res.ok)
        if (!res.ok && isValid) {
          setValid(false)
          valid = false
        }
      }
      onChangeText?.(id, value, valid)
    }
  }

  return (
    <TextInput
      value={value}
      title={title}
      floatTitle={title || floatTitle}
      style={[styles.textInputStyle, style]}
      onPress={onPress}
      secureTextEntry={secureTextEntry}
      onChangeText={handleChangeText}
      titleStyle={titleStyle}
      messageError={
        !isEmailAvailable ? 'Email already in use' : !isValid && value && (messageError || errorText)
      }
      editable={editable}
      multiline={multiline}
      placeholder={placeholder}
      keyboardType={keyboardType}
      maxLength={maxLength}
      minLength={minLength}
      blurOnSubmit={blurOnSubmit}
      onBlur={onBlur || handleSubmitText}
      countryCode={countryCode}
      countryCodeComponent={countryCodeComponent}
      icon={icon}
      inputStyle={[styles[formInputStyle], inputStyle]}
      isRequire={isRequire}
      leftIcon={leftIcon}
      messageInfo={messageInfo}
      onEndEditing={onEndEditing}
      onFocus={onFocus}
      onSelectCountry={onSelectCountry}
      onSubmitEditing={onSubmitEditing || handleSubmitText}
      placeholderTextInput={placeholderTextInput}
      textInputStyle={textInputStyle}
      rightIcon={rightIcon}
      numberOfLines={numberOfLines}
      scrollEnabled={scrollEnabled}
      autoFocus={autoFocus}
      placeholderTextColor={placeholderTextColor}
      pointerEvents={pointerEvents}
      textAlignVertical={textAlignVertical}
      backgroundDisabled={backgroundDisabled}
    />
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textInputStyle: {},
  inputStyle: {
    height: responsiveHeight(38),
    borderRadius: responsiveHeight(19),
    borderWidth: 0.5,
  },
  rounded: {
    height: responsiveHeight(38),
    borderRadius: responsiveHeight(19),
    borderWidth: 0.5,
  },
  rectangle: {
    height: responsiveHeight(38),
    borderRadius: responsiveHeight(3),
    borderWidth: 0.5,
  },
})
export default TextInputView
