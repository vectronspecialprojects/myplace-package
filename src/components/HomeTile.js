import React from 'react'
import {View, Text, StyleSheet, Platform} from 'react-native'
import {TouchableCmp} from './UtilityFunctions'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import {responsiveWidth, responsiveFont} from '../Themes/Metrics'
import CustomIcon from './CustomIcon'

const HomeTile = (props) => {
  return (
    <View style={styles.tile}>
      <TouchableCmp activeOpacity={0.6} style={{flex: 1}} onPress={props.onPress}>
        <View style={[styles.container, props.style]}>
          <CustomIcon
            image_icon={props.imageIcon}
            name={props.icon}
            size={responsiveFont(16)}
            color={Colors().home.menuIcon}
          />
          <Text
            style={{
              ...Styles.x2SmallNormalText,
              ...{marginTop: responsiveWidth(5), color: Colors().home.menuText},
            }}
            numberOfLines={1}>
            {props.title}
          </Text>
          <Text style={{...Styles.smallCapText, ...styles.point, color: Colors().home.menuDesc}} numberOfLines={1}>
            {props.numberShow}
          </Text>
        </View>
      </TouchableCmp>
    </View>
  )
}

const styles = StyleSheet.create({
  tile: {
    width: responsiveWidth(70),
    aspectRatio: 1,
    borderRadius: 10,
    overflow: 'hidden',
    elevation: 5,
  },
  container: {
    flex: 1,
    borderRadius: 10,
    padding: responsiveWidth(2),
    justifyContent: 'center',
    alignItems: 'center',
  },
  point: {
    marginTop: responsiveWidth(5),
  },
})

export default HomeTile
