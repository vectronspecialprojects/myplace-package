import React from 'react'
import {View, StyleSheet, Text, ImageBackground} from 'react-native'
import Barcode from 'react-native-barcode-builder'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Images from '../Themes/Images'
import {useSelector} from 'react-redux'

const BarcodeBar = (props) => {
  const useLegacyDesign = useSelector((state) => state.app.useLegacyDesign)

  if (!props.value) {
    return (
      <View
        style={{
          borderTopWidth: responsiveHeight(1),
          borderTopColor: Colors().home.barcodeTopBorder,
          paddingTop: responsiveHeight(30),
        }}>
        <ImageBackground style={styles.barcode} source={Images.barcode}>
          <View style={styles.textWrapper}>
            <Text style={[Styles.xSmallNormalText, {color: Colors().home.barcodeGeneratingText}]}>
              Your membership barcode is being generated.
            </Text>
          </View>
        </ImageBackground>
      </View>
    )
  }
  return (
    <View style={props.style}>
      <View style={styles.barcodeContainer}>
        <Barcode
          value={props.value}
          format="CODE128B"
          width={props.width}
          lineColor={Colors().black}
          background={Colors().white}
        />
      </View>
      <Text style={[Styles.xSmallNormalText, !useLegacyDesign && {color: Colors().black}]}>
        {props.title.concat(' ', props.text)}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  barcodeContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: responsiveHeight(42),
    overflow: 'hidden',
  },
  barcode: {
    width: responsiveWidth(200),
    height: responsiveHeight(50),
    alignSelf: 'center',
  },
  textWrapper: {
    flex: 1,
    paddingHorizontal: responsiveWidth(2),
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default BarcodeBar
