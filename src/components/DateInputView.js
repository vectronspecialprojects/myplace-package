import React, {useEffect, useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import {responsiveFont, responsiveHeight} from '../Themes/Metrics'
import DateTime from './DateTime'
import Fonts from '../Themes/Fonts'
import dayjs from 'dayjs'

function DateInputView({
  onInputChange,
  id,
  style,
  styleText,
  maxDate,
  minDate,
  containerStyle,
  required,
  label,
  errorText,
}) {
  const [isValid, setIsValid] = useState(true)
  const [date, setDate] = useState('')

  const dateChangeHandler = date => {
    if (required && dayjs().diff(date, 'years') >= 18) {
      setIsValid(true)
    } else {
      setIsValid(false)
    }
    setDate(date)
  }

  useEffect(() => {
    onInputChange(id, dayjs(date).format('DD/MM/YYYY'), isValid)
  }, [onInputChange, id, date, isValid])

  return (
    <View style={[containerStyle]}>
      <DateTime
        pickerStyle={[styles.pickerStyle, style]}
        value={date}
        onChange={dateChangeHandler}
        maxDate={maxDate}
        minDate={minDate}
        title={label}
        isInput={true}
      />
      {!isValid && (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{errorText}</Text>
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  errorContainer: {
    marginBottom: responsiveHeight(5),
  },
  errorText: {
    fontFamily: Fonts.openSans,
    color: 'red',
    fontSize: responsiveFont(11),
  },
})
export default DateInputView
