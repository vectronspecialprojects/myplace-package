import React, {useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import fonts from '../Themes/Fonts'
import Html from '../components/Html'
import {onLinkPress, TouchableCmp} from '../components/UtilityFunctions'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Fonts from '../Themes/Fonts'

const FaqItem = (props) => {
  const [show, setShow] = useState(false)
  return (
    <View style={[styles.container, {backgroundColor: Colors().faq.background}]}>
      <TouchableCmp activeOpacity={0.6} onPress={() => setShow(!show)}>
        <View style={styles.titleContainer}>
          <Text style={[styles.title, {color: Colors().faq.title}]}>{props.title}</Text>
          <AntDesign name={show ? 'down' : 'right'} size={22} color={Colors().faq.title} />
        </View>
      </TouchableCmp>
      {show && (
        <View style={{marginTop: responsiveHeight(5)}}>
          <Html
            html={`<div>${props.content}</div>`}
            color={Colors().faq.description}
            textAlign={'left'}
            onLinkPress={onLinkPress}
          />
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: responsiveWidth(20),
    marginBottom: responsiveHeight(18),
    padding: responsiveWidth(15),
    borderRadius: responsiveWidth(14),
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(15),
    textAlign: 'left',
  },
})

export default FaqItem
