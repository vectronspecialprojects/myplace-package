import React from 'react'
import {Text, TouchableOpacity, View} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import Colors from '../Themes/Colors'
import VectorIconButton from './VectorIconButton'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

export default function ({title, onPress, value, size = 22, style, textStyle}) {
  return (
    <View
      style={[
        {
          flexDirection: 'row',
          marginBottom: responsiveHeight(15),
        },
        style,
      ]}>
      <VectorIconButton
        Component={MaterialIcons}
        name={value ? 'check-box' : 'check-box-outline-blank'}
        size={22}
        color={value ? Colors().second : Colors().second}
        style={{marginRight: responsiveWidth(10)}}
        onPress={onPress}
      />
      <Text
        style={[
          {
            fontFamily: Fonts.openSans,
            color: Colors().defaultTextColor,
            fontSize: responsiveFont(13),
            flex: 1,
          },
          textStyle,
        ]}>
        {title}
      </Text>
    </View>
  )
}
