import React, {useCallback, useEffect, useState} from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Colors from '../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import DateTime from './DateTime'
import dayjs from 'dayjs'
function DateFilterComponent({onChange}) {
  const [dateFrom, setDateFrom] = useState('')
  const [dateTo, setDateTo] = useState('')

  useEffect(() => {
    onChange({
      from: dateFrom ? dayjs(dateFrom).format('YYYY-MM-DD') : '',
      to: dateTo ? `${dayjs(dateTo).format('YYYY-MM-DD')} 23:59:59` : '',
    })
  }, [dateFrom, dateTo])

  return (
    <View style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.buttonDate}>
          <Text style={[styles.textTitle, {color: Colors().history.filterText}]}>From</Text>
          <DateTime
            maxDate={dateTo}
            value={dateFrom}
            onChange={date => {
              setDateFrom(date)
            }}
          />
        </View>
        <View style={styles.buttonDate}>
          <Text style={[styles.textTitle, {color: Colors().history.filterText}]}>To</Text>
          <DateTime
            minDate={dateFrom}
            value={dateTo}
            onChange={date => {
              setDateTo(date)
            }}
          />
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: responsiveWidth(12),
    paddingVertical: responsiveHeight(20),
  },
  buttonDate: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textTitle: {
    fontSize: responsiveFont(14),
  },
})

export default DateFilterComponent
