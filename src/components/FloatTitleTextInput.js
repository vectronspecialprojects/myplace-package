import React, {useEffect, useRef, useState} from 'react'
import {View, Animated, StyleSheet, TextInput, TouchableOpacity, Text, Keyboard, Platform} from 'react-native'
import Colors from '../Themes/Colors'
import {isIOS, responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

export function FloatTitleTextInput({
  value,
  title,
  onChangeText,
  keyboardType,
  messageError,
  secureTextEntry,
  icon,
  onPress,
  style,
  messageInfo,
  countryCodeComponent,
  countryCode,
  onSelectCountry,
  placeholder,
  onSubmitEditing,
  titleStyle,
  isRequire,
  editable = true,
  inputStyle,
  multiline = false,
  rightIcon,
  textInputStyle,
  maxLength,
  refs,
  onFocus,
  onBlur,
  leftIcon,
  floatTitle,
  placeholderTextInput,
  blurOnSubmit,
  onEndEditing,
  backgroundDisabled = Colors().disabled
}) {
  const [isFieldActive, setFieldActive] = useState(false)
  const position = useRef(new Animated.Value(!!value ? 1 : 0)).current
  const textInputRef = useRef(null)
  const [showPassword, setShowPassword] = useState(false)
  const [isFocus, setFocus] = useState(false)
  const inputHeight = inputStyle.height || responsiveHeight(48)
  const [titleWidth, setTitleWidth] = useState(0)

  useEffect(() => {
    if (!isFieldActive && value) {
      setFieldActive(true)
      Animated.timing(position, {
        toValue: 1,
        duration: 200,
        useNativeDriver: false
      }).start()
    }
  }, [value])

  const _handleFocus = () => {
    if (!isFieldActive) {
      !refs && textInputRef.current.focus()
      setFieldActive(true)
      Animated.timing(position, {
        toValue: 1,
        duration: 200,
        useNativeDriver: false
      }).start()
    }
  }

  const _handleBlur = () => {
    if (isFieldActive && !value) {
      setFieldActive(false)
      return Animated.timing(position, {
        toValue: 0,
        duration: 200,
        useNativeDriver: false
      }).start()
    }
  }

  return (
    <TouchableOpacity
      onPress={() => {
        Keyboard.dismiss()
        _handleFocus()
        if (onPress) {
          onPress()
        }
      }}
      style={[styles.container, style]}
      disabled={!onPress}>
      <View style={{flex: 1, flexDirection: 'row'}} pointerEvents={!!onPress ? 'none' : 'auto'}>
        {!!floatTitle && (
          <>
            <Animated.View
              style={[
                styles.viewHideLine,
                {
                  width: titleWidth,
                  opacity: position.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 1]
                  })
                }
              ]}
            />
            <Animated.Text
              onLayout={(e) => {
                setTitleWidth(e.nativeEvent.layout.width)
              }}
              pointerEvents={'none'}
              onPress={() => _handleFocus()}
              style={[
                styles.titleStyles,
                {
                  color: Colors().defaultTextInputColor
                },
                titleStyle,
                {
                  paddingLeft: countryCodeComponent ? (isIOS() ? 90 : 100) : 0,
                  top: position.interpolate({
                    inputRange: [0, 1],
                    outputRange: [responsiveHeight(12), responsiveHeight(-6)]
                  }),
                  fontSize: position.interpolate({
                    inputRange: [0, 1],
                    outputRange: [responsiveFont(14), responsiveFont(11)]
                  })
                }
              ]}>
              {floatTitle}
            </Animated.Text>
          </>
        )}
        <View style={{flex: 1, justifyContent: 'flex-end'}}>
          <View
            style={[
              styles.textInputContainer,
              {backgroundColor: Colors().defaultTextInputBackgound},
              inputStyle,
              messageError && {borderColor: Colors().red},
              !editable && {backgroundColor: backgroundDisabled}
            ]}
            pointerEvents={editable ? 'auto' : 'none'}>
            {!!countryCodeComponent && countryCodeComponent}
            {!!leftIcon && leftIcon}
            <TextInput
              value={value}
              ref={refs || textInputRef}
              style={[
                styles.textInput,
                {
                  paddingLeft: countryCode ? 15 : 0,
                  color: Colors().defaultTextInputColor
                },
                textInputStyle
              ]}
              onChangeText={(text) => {
                onChangeText && onChangeText(text)
              }}
              maxLength={maxLength}
              placeholder={placeholderTextInput}
              placeholderTextColor={Colors().defaultPlaceholderTextColor}
              keyboardType={keyboardType || 'default'}
              underlineColorAndroid="transparent"
              secureTextEntry={secureTextEntry && !showPassword}
              onEndEditing={onEndEditing}
              blurOnSubmit={blurOnSubmit}
              onSubmitEditing={() => {
                if (onSubmitEditing) {
                  onSubmitEditing()
                }
                setFocus(false)
              }}
              onBlur={() => {
                setFocus(false)
                _handleBlur()
                !!onBlur && onBlur()
              }}
              autoCapitalize="none"
              editable={editable && !onPress}
              scrollEnabled={false}
              multiline={multiline}
              textAlignVertical={'top'}
              onFocus={() => {
                _handleFocus()
                setFocus(true)
                !!onFocus && onFocus()
              }}
            />
            {isFocus && value?.length > 0 && (
              <TouchableOpacity onPress={() => onChangeText('')} style={{marginRight: 10}}>
                <MaterialCommunityIcons
                  name={'close-circle'}
                  color={Colors().defaultTextInputColor}
                  size={20}
                />
              </TouchableOpacity>
            )}
            {!!rightIcon && rightIcon}
          </View>
          {!!messageError && (
            <Text
              style={{
                fontSize: responsiveHeight(11),
                color: Colors().red,
                marginTop: responsiveHeight(3)
              }}>
              {messageError || messageInfo}
            </Text>
          )}
          {!!messageInfo && !messageError && (
            <Text
              style={{
                fontSize: responsiveHeight(11),
                marginTop: responsiveHeight(3)
              }}>
              {messageInfo}
            </Text>
          )}
        </View>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    minHeight: 40,
    marginBottom: responsiveHeight(10),
    marginTop: responsiveHeight(5)
  },
  titleStyles: {
    position: 'absolute',
    fontSize: responsiveFont(14),
    left: responsiveWidth(20),
    zIndex: 999
  },
  textInputContainer: {
    paddingLeft: responsiveWidth(10),
    height: responsiveHeight(48),
    minHeight: 38,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: responsiveWidth(24)
  },
  textInput: {
    flex: 1,
    textAlignVertical: 'center',
    fontSize: responsiveFont(16),
    marginBottom: 0,
    paddingVertical: 0
  },
  viewHideLine: {
    backgroundColor: Colors().defaultTextInputBackgound,
    height: 2,
    position: 'absolute',
    left: responsiveWidth(20),
    zIndex: 999
  }
})
