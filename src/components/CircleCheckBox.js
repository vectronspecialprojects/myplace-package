import React from 'react'
import {Text, TouchableOpacity, View} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import Colors from '../Themes/Colors'
import VectorIconButton from './VectorIconButton'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

export default function ({title, onPress, value, size = 30, style, textStyle, outerColor, innerColor}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        {
          flexDirection: 'row',
          alignItems: 'center',
        },
        style,
      ]}>
      <VectorIconButton
        Component={MaterialIcons}
        name={value ? 'radio-button-checked' : 'radio-button-unchecked'}
        size={size}
        color={value ? innerColor : outerColor}
        disabled={true}
      />
      <Text
        style={[
          {
            fontFamily: Fonts.openSans,
            color: Colors().defaultTextColor,
            fontSize: responsiveFont(13),
            flex: 1,
          },
          textStyle,
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  )
}
