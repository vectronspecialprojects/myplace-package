import React from 'react'
import {View, StyleSheet} from 'react-native'
import Colors from '../Themes/Colors'
import {responsiveWidth, responsiveFont, responsiveHeight} from '../Themes/Metrics'
import {TouchableCmp} from './UtilityFunctions'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

const StarButton = (props) => {
  return (
    <View style={{...styles.container, backgroundColor: Colors().home.iconBackground, ...props.style}}>
      <TouchableCmp onPress={props.onPress}>
        <View style={styles.button}>
          <FontAwesome name="star" size={responsiveFont(35)} color={props.color} />
        </View>
      </TouchableCmp>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: responsiveWidth(47),
    aspectRatio: 1,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: responsiveHeight(-20),
    overflow: 'hidden',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    borderRadius: 10,
  },
})

export default StarButton
