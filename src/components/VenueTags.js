import React, {useMemo, useRef, useState} from 'react'
import {View, Text, FlatList, StyleSheet} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import {TouchableCmp} from './UtilityFunctions'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'

const VenueTags = ({selectedTag, onPress, data}) => {
  const listRef = useRef()

  const renderItem = ({item, index}) => {
    const isSelected = selectedTag === item.id
    return (
      <View
        style={[
          styles.tags,
          {
            backgroundColor: Colors().venueTags.tagsBackgroundInactive,
            borderColor: Colors().venueTags.borderColor,
          },
          isSelected && {backgroundColor: Colors().venueTags.tagsBackgroundActive},
        ]}>
        <TouchableCmp
          style={{flex: 1}}
          onPress={() => {
            onPress(item)
            listRef.current?.scrollToIndex({index: index, animated: true, viewPosition: 0.5})
          }}>
          <View style={styles.textWrapper}>
            <Text
              style={[
                Styles.smallCapBoldText,
                isSelected
                  ? {color: Colors().venueTags.tagsTitleActive}
                  : {color: Colors().venueTags.tagsTitleInactive},
              ]}>
              {item.name}
            </Text>
          </View>
        </TouchableCmp>
      </View>
    )
  }
  return (
    <View>
      <FlatList
        ref={listRef}
        data={data}
        style={{
          height: responsiveHeight(50),
          paddingVertical: responsiveHeight(8),
          backgroundColor: Colors().venueTags.tagsBarBackground,
        }}
        showsHorizontalScrollIndicator={false}
        renderItem={renderItem}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  tags: {
    marginHorizontal: responsiveWidth(5),
    paddingHorizontal: responsiveWidth(10),
    borderRadius: 12,
    overflow: 'hidden',
    borderWidth: 2,
  },
  textWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default VenueTags
