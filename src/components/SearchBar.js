import React from 'react'
import {View, StyleSheet, TextInput, Text} from 'react-native'
import Colors from '../Themes/Colors'
import fonts from '../Themes/Fonts'

function SearchBar({
  value,
  onChangeText,
  placeholder,
  autoFocus = false,
  onSubmitEditing,
  style,
  backgroundColor,
}) {
  return (
    <View style={[styles.container, style]}>
      <Text style={[styles.title, {color: Colors().defaultTextColor}]}>Search</Text>
      <TextInput
        style={[
          styles.input,
          {
            backgroundColor: Colors().first,
            color: Colors().defaultTextColor,
          },
          {backgroundColor: backgroundColor},
        ]}
        value={value}
        autoFocus={autoFocus}
        onChangeText={onChangeText}
        underlineColorAndroid="transparent"
        placeholderTextColor={Colors().defaultTextColor}
        placeholder={placeholder}
        onSubmitEditing={onSubmitEditing}
        returnKeyType={'search'}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginBottom: 20,
  },
  input: {
    height: 44,
    fontFamily: fonts.openSans,
    borderRadius: 14,
    paddingHorizontal: 10,
  },
  title: {
    marginBottom: 10,
    fontSize: 12,
    fontFamily: fonts.openSans,
  },
})

export default SearchBar
