import React, {useState, useEffect} from 'react'
import {StyleSheet, View, Text} from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import Colors from '../Themes/Colors'
import {useSelector} from 'react-redux'

const Dropdown = (props) => {
  const {
    style,
    placeholder,
    disabled = false,
    onInputChange,
    id,
    items,
    initValue,
    titleStyle,
    containerStyle,
  } = props
  const [isValid, setIsValid] = useState(false)
  const useLegacyDesign = useSelector((state) => state.app.useLegacyDesign)
  const [value, setValue] = useState(null)

  useEffect(() => {
    setValue(initValue)
  }, [initValue])

  const valueChangeHandler = (value) => {
    let isValid = true
    if (props.required && value === null) isValid = false
    setIsValid(isValid)
    setValue(value)
    onInputChange(id, value, isValid)
  }

  useEffect(() => {
    if (items?.length === 1) {
      valueChangeHandler(items[0].value)
    }
  }, [props.items])

  return (
    <View style={[styles.formControl, containerStyle]}>
      <Text style={[styles.label, {color: Colors().defaultTextColor}, titleStyle]}>{props?.label}</Text>
      <View style={!useLegacyDesign && {borderRadius: responsiveHeight(24), overflow: 'hidden'}}>
        <RNPickerSelect
          placeholder={{
            label: placeholder,
            value: null,
          }}
          style={{
            inputIOS: {
              ...pickerSelectStyles.inputIOS,
              backgroundColor: Colors().signUp.fieldBackground,
              color: Colors().signUp.fieldText,
            },
            inputAndroid: {
              ...pickerSelectStyles.inputAndroid,
              backgroundColor: Colors().signUp.fieldBackground,
              color: Colors().signUp.fieldText,
            },
            ...style,
          }}
          onValueChange={valueChangeHandler}
          items={props.items}
          value={value}
          disabled={disabled}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  formControl: {
    width: '100%',
  },
  label: {
    fontFamily: Fonts.openSansBold,
    marginVertical: responsiveHeight(4),
  },
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    paddingHorizontal: responsiveWidth(20),
    paddingVertical: responsiveHeight(5),
    fontFamily: Fonts.openSans,
    fontSize: 14,
    height: responsiveHeight(48),
    borderRadius: 15,
  },
  inputAndroid: {
    paddingHorizontal: responsiveWidth(10),
    paddingVertical: responsiveHeight(5),
    fontFamily: Fonts.openSans,
    fontSize: 14,
    height: responsiveHeight(48),
    borderRadius: 15,
  },
})

export default Dropdown
