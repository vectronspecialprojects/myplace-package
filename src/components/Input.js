import React, {useReducer, useEffect} from 'react'
import {View, Text, TextInput, StyleSheet} from 'react-native'
import {apiCheckEmail} from '../utilities/ApiManage'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'

const INPUT_CHANGE = 'INPUT_CHANGE'
const INPUT_BLUR = 'INPUT_BLUR'
const CHECK_EMAIL = 'CHECK_EMAIL'

const inputReducer = (state, action) => {
  switch (action.type) {
    case INPUT_CHANGE:
      return {
        ...state,
        value: action.value,
        isValid: action.isValid,
      }
    case INPUT_BLUR:
      return {
        ...state,
        touched: true,
      }
    case CHECK_EMAIL:
      return {
        ...state,
        isEmailAvailable: action.isEmailAvailable,
      }
    default:
      return state
  }
}

const Input = (props) => {
  const [inputState, dispatch] = useReducer(inputReducer, {
    value: props.initialValue ? props.initialValue : '',
    isValid: props.initiallyValid,
    isEmailAvailable: props.isEmailAvailable,
    touched: false,
  })

  const {onInputChange, id, emailNotAllowedDomains} = props

  useEffect(() => {
    if (inputState.touched) {
      onInputChange(id, inputState.value, inputState.isValid)
    }
  }, [inputState, onInputChange, id])

  useEffect(() => {
    if (props.resetEvent) {
      console.log('resetEvent', id, props.resetEvent)
      dispatch({type: INPUT_CHANGE, value: '', isValid: false})
    }
  }, [props.resetEvent, dispatch])

  const textChangeHandler = (text) => {
    const emailRegex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const alphabetRegex = /^[a-zA-Z]+$/
    const passwordRegex = /^(?=.*\d)(?=.*[a-z!@#$&])[a-z0-9!@#$&]{6,}$/
    let isValid = true
    if (props.required && text.trim().length === 0) {
      isValid = false
    }
    if (props.email && !emailRegex.test(text.toLowerCase())) {
      isValid = false
    }
    if (props.alphabet && !alphabetRegex.test(text)) {
      isValid = false
    }
    if (props.password && !passwordRegex.test(text.toLowerCase())) {
      isValid = false
    }
    if (props.confirmPassword && text !== props.crossCheckPassword) {
      isValid = false
    }
    if (props.min != null && +text < props.min) {
      isValid = false
    }
    if (props.max != null && +text > props.max) {
      isValid = false
    }
    if (props.minLength != null && text.length < props.minLength) {
      isValid = false
    }
    if (props.email && !!emailNotAllowedDomains && isValid && emailNotAllowedDomains.test(text)) {
      isValid = false
    }
    dispatch({type: INPUT_CHANGE, value: text, isValid: isValid})
  }

  const checkIsTouched = () => {
    dispatch({type: INPUT_BLUR})
    if (props.emailAvailability) dispatch({type: CHECK_EMAIL, isEmailAvailable: true})
  }

  const lostFocusHandler = async () => {
    if (props.emailAvailability) {
      const res = await apiCheckEmail(inputState.value)
      dispatch({type: CHECK_EMAIL, isEmailAvailable: res.ok})
      console.log(res)
      if (!res.ok && inputState.isValid) {
        dispatch({type: INPUT_CHANGE, value: inputState.value, isValid: false})
      }
    }
  }

  return (
    <View style={styles.formControl}>
      <Text style={[styles.label, {color: Colors().defaultTextInputLabelColor}]}>{props.label}</Text>
      <TextInput
        {...props}
        style={[
          styles.input,
          {
            backgroundColor: Colors().defaultTextInputBackgound,
            color: Colors().defaultTextInputColor,
          },
          props.style,
        ]}
        value={props.resetEvent ? '' : inputState.value}
        onChangeText={textChangeHandler}
        onBlur={lostFocusHandler}
        onFocus={checkIsTouched}
        placeholder={props.placeholder}
        placeholderTextColor={props.placeholderTextColor}
      />
      {!inputState.isValid && inputState.touched && !props.emailAvailability && (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{props.errorText}</Text>
        </View>
      )}
      {!inputState.isValid && inputState.touched && props.emailAvailability && (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>
            {inputState.isEmailAvailable ? props.errorText : 'Email already in use'}
          </Text>
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  formControl: {
    width: '100%',
  },
  label: {
    fontFamily: Fonts.openSansBold,
    marginVertical: responsiveHeight(6),
  },
  input: {
    paddingHorizontal: responsiveWidth(20),
    paddingVertical: responsiveHeight(5),
    height: responsiveHeight(48),
    borderRadius: 15,
  },
  errorContainer: {
    marginVertical: responsiveHeight(5),
  },
  errorText: {
    fontFamily: Fonts.openSans,
    color: 'red',
    fontSize: 13,
  },
})

export default Input
