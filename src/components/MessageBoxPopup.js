import React from 'react'
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native'
import {responsiveHeight, responsiveWidth, deviceWidth, deviceHeight, isIOS} from '../Themes/Metrics'

import Modal from 'react-native-modal'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import Ionicons from 'react-native-vector-icons/Ionicons'
import ButtonView from './ButtonView'

const MessageBoxPopup = (props) => {
  return (
    <Modal
      isVisible={props.isVisible}
      animationIn="swing"
      animationOut="zoomOut"
      onBackdropPress={() => Keyboard.dismiss()}>
      <KeyboardAvoidingView
        style={{flex: 1, justifyContent: 'center'}}
        behavior={isIOS() ? 'padding' : 'height'}>
        <View style={styles.popupContainer}>
          <TouchableOpacity onPress={() => Keyboard.dismiss()} activeOpacity={1}>
            <View
              style={{
                paddingHorizontal: responsiveWidth(10),
                paddingVertical: responsiveHeight(15),
                width: '100%',
                borderBottomWidth: responsiveHeight(1),
                borderBottomColor: '#eee',
              }}>
              <Text
                style={{
                  ...Styles.largeCapBoldText,
                  ...{color: '#444', textTransform: 'uppercase', alignItems: 'flex-start'},
                }}>
                {props.header}
              </Text>
              <Text
                style={{
                  ...Styles.xSmallNormalText,
                  ...{color: '#444', alignItems: 'flex-start', marginTop: 5},
                }}>
                Got a question? Contact us below!
              </Text>
            </View>
            <View style={{padding: responsiveWidth(10)}}>
              <Text style={{...Styles.largeNormalText, ...{color: '#444', alignSelf: 'flex-start'}}}>
                Message
              </Text>
              {props.value.length === 0 && (
                <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: responsiveHeight(2)}}>
                  <Ionicons
                    name="md-information-circle"
                    size={18}
                    color="red"
                    style={{marginRight: responsiveWidth(5)}}
                  />
                  <Text style={{...Styles.smallNormalText, ...{color: '#444', alignSelf: 'flex-start'}}}>
                    message is required
                  </Text>
                </View>
              )}

              <View>
                <TextInput
                  style={styles.comment}
                  value={props.value}
                  onChangeText={(text) => props.onChangeText(text)}
                  multiline={true}
                  underlineColorAndroid="transparent"
                />
              </View>
            </View>

            <View style={styles.popupButtonsBar}>
              <ButtonView
                title={'Cancel'}
                disabled={false}
                style={[styles.buttonBorder, {borderColor: Colors().second}]}
                titleStyle={{color: Colors().second}}
                onPress={props.onCancelPress}
              />
              <ButtonView
                title={'Ok'}
                disabled={props.value.length === 0}
                style={{flex: 1, marginRight: responsiveWidth(2), marginLeft: responsiveWidth(1)}}
                onPress={props.onOkPress}
              />
            </View>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </Modal>
  )
}

const styles = StyleSheet.create({
  popupContainer: {
    borderRadius: 12,
    alignSelf: 'center',
    width: responsiveWidth(300),
    maxWidth: deviceWidth() * 0.7,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    overflow: 'hidden',
  },
  comment: {
    paddingRight: responsiveWidth(3),
    paddingVertical: 5,
    backgroundColor: '#fff',
    color: '#444',
    height: responsiveHeight(200),
    width: '100%',
    maxHeight: deviceHeight() * 0.3,
    alignSelf: 'center',
    textAlignVertical: 'top',
    lineHeight: 23,
  },
  popupButtonsBar: {
    width: '100%',
    flexDirection: 'row',
    height: responsiveHeight(50),
    marginBottom: responsiveHeight(10),
    justifyContent: 'space-between',
  },
  buttonBorder: {
    flex: 1,
    marginLeft: responsiveWidth(2),
    marginRight: responsiveWidth(1),
    borderWidth: 2,
    backgroundColor: 'transparent',
  },
})

export default MessageBoxPopup
