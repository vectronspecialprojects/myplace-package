import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {FontAwesome5Touch} from '../components/UtilityFunctions'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import {responsiveFont} from '../Themes/Metrics'

const MemberNameBar = (props) => {
  return (
    <View style={{...styles.nameBar, ...props.style}}>
      <Text style={{...Styles.largeCapText, ...{paddingRight: 5}}}>
        {props.name?.member?.first_name} {props.name?.member?.last_name}
      </Text>
      <FontAwesome5Touch
        name="edit"
        size={responsiveFont(15)}
        color={Colors().defaultTextColor}
        onPress={props.onIconPress}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  nameBar: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
})

export default MemberNameBar
