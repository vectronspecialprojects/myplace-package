import React from 'react'
import {Text, StyleSheet, View, Modal, TouchableOpacity, Dimensions} from 'react-native'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'

import ButtonView from './ButtonView'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

export default function BottomSheet({
  visible,
  onClose,
  headerTitle,
  children,
  onConfirm,
  confirmButtonTitle,
  confirmButtonStyle,
  showConfirmButton = true,
  showCloseButton = true,
  titleStyle,
  containerStyle,
  titleContainerStyle,
  disabled,
}) {
  return (
    <Modal animationType={'slide'} visible={visible} transparent onRequestClose={onClose}>
      <TouchableOpacity
        onPress={() => {
          onClose?.()
        }}
        activeOpacity={1}
        style={{flex: 1, backgroundColor: 'transparent', justifyContent: 'flex-end'}}>
        <View style={[styles.contentContainer, {backgroundColor: Colors().white}, containerStyle]}>
          {headerTitle && (
            <View style={[styles.titleContainer, {backgroundColor: Colors().white}, titleContainerStyle]}>
              {showCloseButton && (
                <TouchableOpacity
                  onPress={() => {
                    onClose?.()
                  }}>
                  <MaterialIcons name={'close'} size={30} color={Colors().gray} />
                </TouchableOpacity>
              )}
              <Text style={[styles.title, titleStyle]}>{headerTitle}</Text>
            </View>
          )}
          {children}
          {showConfirmButton && (
            <ButtonView
              disabled={disabled}
              onPress={onConfirm}
              title={confirmButtonTitle}
              style={[{marginTop: responsiveHeight(20)}, confirmButtonStyle]}
            />
          )}
        </View>
      </TouchableOpacity>
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingBottom: 44,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: responsiveHeight(13.5),
    paddingHorizontal: responsiveWidth(25),
  },
  title: {
    fontSize: responsiveFont(18),
    textAlign: 'center',
    fontFamily: Fonts.openSans,
    flex: 1,
  },
  titleContainer: {
    alignItems: 'center',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginBottom: responsiveHeight(20),
    flexDirection: 'row',
  },
})
