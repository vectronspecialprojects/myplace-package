import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import MapPreview from './MapPreview'
import {navigate} from '../navigation/NavigationService'
import RouteKey from '../navigation/RouteKey'
import FontAwesome5Pro from 'react-native-vector-icons/FontAwesome5Pro'
import {getIconsName, isTrue} from '../utilities/utils'
import {useSelector} from 'react-redux'

const MapDetail = (props) => {
  const {address, location, telp, email} = props
  const appFlags = useSelector((state) => state.app.appFlags)
  return (
    <View style={styles.container}>
      {isTrue(appFlags?.app_is_show_map) && (
        <MapPreview onPress={props.onMapPress} style={styles.MapPreview} location={location}>
          <Text style={Styles.smallNormalText}>location not found</Text>
        </MapPreview>
      )}
      <View style={{padding: responsiveWidth(5), flexDirection: 'row'}}>
        <View style={{flex: 2, paddingLeft: responsiveWidth(10)}}>
          {!!address && (
            <Text style={[styles.address, {color: Colors().location.description}]}>{address}</Text>
          )}
          {!!telp && (
            <Text
              style={{
                ...Styles.xSmallNormalText,
                ...{textAlign: 'left', color: Colors().location.description},
              }}
              onPress={props.onTelpPress}>
              T: {telp}
            </Text>
          )}
          {!!email && (
            <Text
              style={[Styles.xSmallNormalText, {textAlign: 'left', color: Colors().location.description}]}
              onPress={props.onEmailPress}>
              E: {email}
            </Text>
          )}
        </View>
        <View style={{flex: 1, justifyContent: 'flex-end'}}>
          <Text
            onPress={props.onDirectionsPress}
            style={[styles.directions, {color: Colors().location.description}]}>
            directions >
          </Text>
        </View>
      </View>
      <View style={styles.contentContainer}>
        {!props?.isHideOpenHours && (
          <View style={styles.timeContainer}>
            <Text style={[styles.openingHours, {color: Colors().location.openHours}]}>opening hours</Text>
            {props.openHours?.map((h) => {
              return (
                <Text key={h.day} style={[styles.openingTime, {color: Colors().location.description}]}>
                  {h.day} {h.time}
                </Text>
              )
            })}
          </View>
        )}
        {!props?.isHideDeliveryHours && (
          <View style={styles.timeContainer}>
            <Text style={[styles.openingHours, {color: Colors().location.openHours}]}>delivery hours</Text>
            {props.DeliveryHours?.map((h) => {
              return (
                <Text key={h.day} style={[styles.openingTime, {color: Colors().location.description}]}>
                  {h.day} {h.time}
                </Text>
              )
            })}
          </View>
        )}
      </View>
      <View style={styles.socialContainer}>
        {[...(props.socialLinks || []), ...(props.menuLinks || [])]?.map((item, index) => {
          if (!item.url) return null
          return (
            <TouchableOpacity
              style={[styles.socialItem, {borderColor: Colors().second}]}
              key={index.toString()}
              onPress={() => {
                navigate(RouteKey.WebviewScreen, {
                  params: {
                    appUri: item.url,
                  },
                })
              }}>
              <FontAwesome5Pro name={getIconsName(item.platform)} size={20} color={Colors().second} />
            </TouchableOpacity>
          )
        })}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    paddingBottom: responsiveHeight(20),
  },
  MapPreview: {
    width: '100%',
    height: 200,
  },
  directions: {
    marginVertical: 10,
    paddingHorizontal: 8,
    ...Styles.smallUpBoldText,
  },
  socialItem: {
    width: responsiveWidth(30),
    height: responsiveWidth(30),
    borderRadius: responsiveWidth(20),
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: responsiveWidth(10),
  },
  socialContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  address: {
    ...Styles.xSmallNormalText,
    lineHeight: responsiveHeight(20),
    marginBottom: responsiveHeight(10),
    textAlign: 'left',
  },
  timeContainer: {
    maxWidth: '50%',
    padding: responsiveWidth(5),
    marginHorizontal: responsiveHeight(20),
    marginBottom: responsiveHeight(10),
  },
  contentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  openingHours: {
    ...Styles.smallUpBoldText,
    alignSelf: 'flex-start',
    marginBottom: responsiveHeight(4),
  },
  openingTime: {
    ...Styles.xSmallNormalText,
    alignSelf: 'flex-start',
  },
})

export default MapDetail
