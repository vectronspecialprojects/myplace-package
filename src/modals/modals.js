export class Voucher {
  constructor(
    name,
    description,
    barcode,
    issueDate,
    price,
    expireDate,
    voucherType,
    amountLeft,
    image,
    memberBarcode,
    memberNumber,
  ) {
    this.name = name
    this.description = description
    this.barcode = barcode
    this.issueDate = issueDate
    this.price = price
    this.expireDate = expireDate
    this.voucherType = voucherType
    this.amountLeft = amountLeft
    this.image = image
    this.memberBarcode = memberBarcode
    this.memberNumber = memberNumber
  }
}

export class Card {
  constructor(title, description, eventDays, emptyStars, filledStarts, image) {
    this.title = title
    this.description = description
    this.eventDays = eventDays
    this.emptyStars = emptyStars
    this.filledStarts = filledStarts
    this.image = image
  }
}

export class signupData {
  constructor(
    venue_id,
    first_name,
    last_name,
    email,
    dob,
    password,
    password_confirmation,
    mobile,
    share_info,
    tier_id,
    opt_out_marketing,
  ) {
    this.venue_id = venue_id
    this.first_name = first_name
    this.last_name = last_name
    this.email = email
    this.dob = dob
    this.password = password
    this.password_confirmation = password_confirmation
    this.mobile = mobile
    this.share_info = share_info
    this.tier_id = tier_id
    this.opt_out_marketing = opt_out_marketing
  }
}
