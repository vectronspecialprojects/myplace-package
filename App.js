import React, {useLayoutEffect} from 'react'
import {connect, Provider} from 'react-redux'
import store, {persistor} from './src/store/store'
import MainLayout from './src/MainLayout'
import {SafeAreaProvider} from 'react-native-safe-area-context'
import CodePush from 'react-native-code-push'
import {PersistGate} from 'redux-persist/integration/react'
import {Text, TextInput, LogBox} from 'react-native'
import vars, {codePushKey} from './src/constants/env'

LogBox.ignoreAllLogs(true)
Text.defaultProps = Text.defaultProps || {}
Text.defaultProps.allowFontScaling = false
TextInput.defaultProps = {
  ...TextInput.defaultProps,
  allowFontScaling: false,
  underlineColorAndroid: 'transparent',
}

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaProvider>
          <MainLayout />
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
  )
}

let codePushOptions = {
  checkFrequency: CodePush.CheckFrequency.MANUAL,
  installMode: CodePush.InstallMode.IMMEDIATE, //App will update immediate
  deploymentKey: codePushKey[vars.buildEvn],
}
export default CodePush(codePushOptions)(App)
