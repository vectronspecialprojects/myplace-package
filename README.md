# Guide To Create New Application

### Step 1: Clone Source Code
* Copy `DemoApp` to new folder
* Use library [react-native-rename](https://github.com/junedomingo/react-native-rename) to change the Project's name and the bundle id

### Step 2: Update package.json
* Open `package.json`
* Replace line: `"@bepoz/my-place": "file:../"`
* By: `"@bepoz/my-place": "git+https://username:password@bitbucket.org/vectronspecialprojects/myplace-package.git"`
* Change `username` and `password` to your credential.

### Step 3: Update App Icon
* Access this link [Make App Icon](https://makeappicon.com/) to create app icons from the image
* Download unzip file
* **IOS**: copy folder `ios/Appicon.appiconset` replace folder `ios/ProjectName/Images.xcassets/Appicon.appiconset`
* **Android**: copy all child in folder `android/` replace all child in folder `android/app/src/main/res`
 
### Step 4: Config Code Push
* Follow `Step 7` in this [file](https://docs.google.com/document/d/13hxZReunW9B8ztW7IwtwkC4NUTmPgclu)

### Step 5: Config Universal Link
* Follow `Step 8` in this [file](https://docs.google.com/document/d/13hxZReunW9B8ztW7IwtwkC4NUTmPgclu)

### Step 6: Update SplashScreen
* Follow `Step 9` in this [file](https://docs.google.com/document/d/13hxZReunW9B8ztW7IwtwkC4NUTmPgclu)

### Step 7: Config environment
* Update file `.env` to new environment.

# Update library to the latest version

* Run the script `yarn install --force`

## Read this file for more information
[Full Step Clone React Native App](https://docs.google.com/document/d/13hxZReunW9B8ztW7IwtwkC4NUTmPgclu)

