module.exports = {
  project: {
    ios: {},
    android: {}, // grouped into "project"
  },
  assets: [
    './node_modules/@bepoz/my-place/src/assets/fonts/',
    './assets/fonts/',
  ], // stays the same
};
