/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from '@bepoz/my-place';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
